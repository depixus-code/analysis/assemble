module RandomSequence
using  Random
using  Distributions: Poisson, quantile
using  StatsBase: mean
export RandomExperiment, RandomParameters, randsequence, randexperiment

using ..Accessor 
using ..Oligo: Experiment, peakposition, peakposition!, oligoid, rc, oligosize

const ARR_T         = Union{Vector{Number}, Number}
const PROPS_T       = Vector{Real}
const RIGID_T       = Union{Tuple{Number, Number}, Number}
const SEED_T        = Union{Int, AbstractRNG}
const _DEFAULT_SEED = Random.GLOBAL_RNG

struct RandomParameters
    original :: Experiment
    missing  :: Vector{Vector{Int32}}
    spurious :: Vector{Vector{Int32}}
    rigid    :: Array{Float32, 2}
end

@defaults struct RandomExperiment
    osize       :: Int     = 3
    spurious    :: ARR_T   = 0.f0
    missing     :: ARR_T   = 0.f0
    sigma       :: ARR_T   = 0.f0
    stretch     :: RIGID_T = (0.f0, 0.f0)
    bias        :: RIGID_T = 0.f0
    proportions :: PROPS_T = zeros(Real, 4)
    batched     :: Bool    = false
end

function _toarray(data::Experiment, missing::ARR_T)
    (typeof(missing) <: Number                                 ?
     [1.f0 for _ = 1:length(data)] * convert(Float32, missing) :
     convert(Array{Float32,1}, missing))
end

function deletepeakpositions!(data::Experiment, oligo::Int, dels::AbstractVector{Bool})
    if !isempty(data[oligo])
        deleteat!(data[oligo], dels)
    end
end

function addpeakpositions!(data::Experiment, oligo::Int, peaks)
    sort!(append!(data[oligo], peaks))
end


"""
Create a random sequence

# Keywords

* sigma: the brownian motion rate. Can be per oligo.
* proportions: the amount of a, t, g and c, in that order
"""
function randsequence(rng         :: AbstractRNG,
                      size        :: Int,
                      proportions :: PROPS_T = proportions(RandomExperiment))
    if sum(proportions) <= 0.
        return string(Base.rand(rng, ["a", "t", "c", "g"], size)...)
    end

    vpro = convert(Array{Float32,1}, proportions)
    prop = cumsum(vpro) / sum(vpro)
    vals = [searchsortedlast(prop, i)+1 for i = rand(rng, Float32, size)]
    return string((["a", "t", "c", "g"][vals])...)
end

function randsequence(ssize::Int, proportions :: PROPS_T = proportions(RandomExperiment))
    randsequence(_DEFAULT_SEED, ssize, proportions)
end

"""
Create the peaks related to a sequence
"""
function experiment(seq::AbstractString, osize::Int = 3)
    data   = [Float32[] for _ = 1:4^osize]
    for i = 1:length(seq)-(osize-1)
        push!(data[oligoid(seq[i:i+osize-1])], i-1)
    end
    data
end

"""
Add spurious peaks
"""
function addspurious(rng::AbstractRNG, data ::Experiment, spurious::ARR_T)
    out   = [Int32[] for i = 1:length(data)]
    vspur = _toarray(data, spurious)
    if any(i > 0.f0 for i in vspur)
        cnts   = ((x, y) -> quantile(Poisson(x), y)).(Float64.(_toarray(data, spurious)),
                                                      Base.rand(rng, length(data)))
        ends   = cumsum(cnts)
        starts = insert!(copy(ends), 1, 0).+1

        maxv   = maximum(isempty(peakposition(data, i)) ? 0. : maximum(peakposition(data, i))
                         for i = 1:length(data))
        pos    = Base.rand(rng, Float32, ends[end])*maxv
        for j in 1:length(cnts)
            if starts[j] != ends[j]
                arr = @view pos[starts[j]:ends[j]]
                addpeakpositions!(data, j, arr)
                out[j] = indexin(arr, peakposition(data, j))
            end
        end
    end
    data, out
end

"""
Add missing data
"""
function addmissing(rng::AbstractRNG, data ::Experiment, missing::ARR_T)
    out   = [zeros(Int32, length(i)) for i = data]
    vmiss = _toarray(data, missing)
    if any(i > 0.f0 for i in vmiss)
        cnt  = [0; cumsum([length(i) for i = data])]
        runi = Base.rand(rng, Float32, cnt[end])

        for j = 1:length(data)
            arr    = (@view runi[cnt[j]+1:cnt[j+1]]) .<= vmiss[j]
            out[j] = [k for (k, l) = enumerate(arr) if l]
            deletepeakpositions!(data, j, arr)
        end
    end
    data, out
end

"""
Add gaussian noise to the data
"""
function addbrownianmotion(rng::AbstractRNG, data ::Experiment, sigma::ARR_T)
    vsig = _toarray(data, sigma)
    if all(i <= 0.f0 for i in vsig)
        return data
    end

    cnt   = [0; cumsum([length(i) for i = data])]
    rnorm = randn(rng, Float32, cnt[end])
    for j = 1:length(data)
        peakposition!(data, j, peakposition(data, j)+vsig[j]*rnorm[cnt[j]+1:cnt[j+1]])
    end
    data
end

"""
Add a stretch and bias to each experiment using normally distributed stretches and biases
"""
function addrigiddeformation(rng    ::AbstractRNG,
                             data   ::Experiment,
                             stretch::Union{Number, Tuple{Number, Number}},
                             bias   ::Union{Number, Tuple{Number, Number}})
    nols   = length(data)
    rcis   = Int[rc(oligosize(data), i) for i = 1:nols]
    if !any(peakposition(data, i) ≡ peakposition(data, rcis[i]) for i = 1:nols)
        rcis .= typemax(Int)
    end

    vstr   = stretch isa Number ? Float32[0f0, Float32(stretch)] : Float32[i for i in stretch]
    vbias  = bias    isa Number ? Float32[0f0, Float32(bias)]    : Float32[i for i in bias]

    if any(i > 0.f0 for i = vstr) || vbias[2]  > 0.f0
        good       = [length(peakposition(data, i)) > 0 for i = 1:length(data)]
        glob       = vstr[1] > 0.f0 ? randn(rng, Float32)*vstr[1]+1.f0 : 1.f0
        rnorm      = randn(rng, Float32, (length(data), 2))

        if vstr[2] > 0.f0
            rnorm[:,1] .= rnorm[:,1] .* (glob/(1f0 + vstr[2]*mean(rnorm[good,1]))) .+ 1f0
            while any(rnorm[good,1] .<= 0f0)
                rnorm[:,1]  .= randn(rng, Float32, length(data)) .*vstr[2] .+1f0
                rnorm[:,1] .*= glob/mean(rnorm[good,1])
            end
            rnorm[.~good,1] .= 1f0
        else
            rnorm[1:end,1]  .= 1f0
        end

        if vbias[2] > 0.f0
            rnorm[:,2]     .*= vbias[2]
            rnorm[.~good,2] .= 0f0
        else
            rnorm[1:end,2]  .= 0f0
        end

        for i = 1:nols
            if good[i]
                if rcis[i] < i
                    rnorm[i,:] .= rnorm[rcis[i],:]
                    continue
                end
                peakposition!(data, i, peakposition(data, i).*rnorm[i,1] .+ rnorm[i,2])
            end
        end

        rnorm[:,2] .= -rnorm[:,2] ./ rnorm[:,1]
        rnorm[:,1] .= 1f0         ./ rnorm[:,1]
    else
        rnorm       = zeros(Float32, (length(data), 2))
        rnorm[:,1] .= 1.f0
    end

    if vbias[1] > 0f0
        delta        = (1. - 2*rand(rng))*vbias[2]
        rnorm[:,2] .-= delta
        for i = 1:nols
            if i < rcis[i]
                peakposition(data, i, peakposition(data, i) .+ delta/rnorm[i,1])
            end
        end
    end
    data, rnorm
end

function batchexperiment(xp::Experiment)
    osize = oligosize(xp)
    xp    = [i for i = xp]
    for i = 1:length(xp)
        rci = rc(osize, i)
        if rci >= i
            xp[rci] = xp[i] = sort!([xp[i]; xp[rci]])
        end
    end
    xp
end

const _E_T  = Union{Type{RandomExperiment}, RandomExperiment}
const _IS_T = Union{Integer, AbstractString}

"""
Create random data for a given string and oligo size.

# Keywords

* missing: the missing rate. Can be per oligo.
* sigma: the brownian motion rate. Can be per oligo.
* stretch: the distribution width for stretches.
* bias: the distribution width for biases.
"""
function randexperiment(ψ ::AbstractRNG, λ ::AbstractString, ξ ::_E_T; kwa...)
    cnf                = clone(ξ; kwa...)
    xp                 = experiment(λ, cnf.osize)
    orig               = copy(xp)
    xp, vmiss          = addmissing(ψ,  xp, cnf.missing)
    xp, vspur          = addspurious(ψ, xp, cnf.spurious)
    xp                 = addbrownianmotion(ψ, xp, cnf.sigma)
    cnf.batched && (xp = batchexperiment(xp))
    xp, params         = addrigiddeformation(ψ, xp, cnf.stretch, cnf.bias)

    RandomParameters(orig, vmiss, vspur, params), xp
end

function randexperiment(ψ::AbstractRNG, σ::Integer, ξ::_E_T; kwa...)
    cnf = clone(ξ; kwa...)
    seq = randsequence(ψ, σ, cnf.proportions)
    (seq, randexperiment(ψ, seq, cnf)...)
end

randexperiment(ψ::AbstractRNG, σ::_IS_T; o...)  = randexperiment(ψ, σ, RandomExperiment; o...)
randexperiment(σ::_IS_T, ξ::_E_T; o...)         = randexperiment(_DEFAULT_SEED, σ, ξ; o...)
randexperiment(σ::_IS_T; o...)                  = randexperiment(σ, RandomExperiment; o...)
rand(ψ::AbstractRNG, ξ::_E_T, λ::_IS_T; kwa...) = randexperiment(ψ, λ, ξ; kwa...)
rand(ξ::_E_T, λ::_IS_T; kwa...)                 = randexperiment(λ, ξ; kwa...)
end
