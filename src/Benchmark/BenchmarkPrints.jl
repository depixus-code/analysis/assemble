module BenchmarkPrints
using  Statistics: median!
using  BioAlignments
using  BioSymbols
import DataFrames: DataFrame
import BioAlignments: pairalign

using  ..Accessor
using  ..Oligo:          OligoConfigs, baseposition, pathpeaks, Experiment,
                       AVECTPEAK, oligoname, peakposition, baseposition,
                       Direction, goright, oligoid, residualreduction!,
                       MInt32, MFloat32, Peak, SequenceStats
import ..Oligo:          oligosize, direction, sequence
using  ..PeakPaths:       residual, normedresidual, Paths, experiment
using  ..PeakTree:       Trees, andreasinsi

import Base: print

const Algs     = Union{Paths, Trees}

@defaults struct PeakAlignment
    oligosize ::Int         = 3
    direction ::Direction   = goright
    precision ::Float32     = 5f0
    maxmiss   ::Int         = 4
    maxreach  ::Float32     = 10f0
end

PeakAlignment(ξ::Algs, a...) = PeakAlignment(oligosize(ξ), direction(ξ), a...)
PeakAlignment(τ::Experiment, a...)          = PeakAlignment(oligosize(τ), a...)

function pairalign(ξ::AbstractString, osize::Int, dir::Symbol, ♠::AVECTPEAK)
    pairalign(LevenshteinDistance(), sequence(osize, dir, ♠), ξ)
end

function pairalign(ξ   ::AbstractString,
                   olcf::Union{OligoConfigs, PeakAlignment},
                   ♠   ::AVECTPEAK)
    pairalign(LevenshteinDistance(), sequence(olcf, ♠), ξ)
end

function Base.print(io   ::IO,
                    res  ::Tuple{AbstractString, OligoConfigs, AVECTPEAK},
                    width::Integer = 150)
    ass = sequence(res[2], res[3])
    pw = pairalign(LevenshteinDistance(), ass, res[1])

    println(io, summary(pw), ':')
    if pw.isscore
        print(io, "  score: ", pw.value)
    else
        print(io, "  distance: ", pw.value)
    end
    println(io)

    aln = alignment(pw)

    gap     = BioSymbols.gap(eltype(aln.b))
    anchors = aln.a.aln.anchors
    # width of position numbers
    posw = ndigits(max(anchors[end].seqpos, anchors[end].refpos)) + 1

    i      = 0
    j      = 0
    k      = 0
    cnt    = convert(Array{Int,1}, baseposition(res[2], res[3])) .+ 1
    seqpos = anchors[1].seqpos
    refpos = anchors[1].refpos
    seqbuf = IOBuffer()
    refbuf = IOBuffer()
    matbuf = IOBuffer()
    next_xy = iterate(aln)
    while next_xy !== nothing
        (x, y), s = next_xy
        next_xy   = iterate(aln ,s)
        i        += 1
        if x != gap
            seqpos += 1
            k      += 1
            if j == 0 || (j <= length(cnt) && cnt[j] < k)
                j += 1
            else
                x  = uppercase(x)
            end
        end
        if y != gap
            refpos += 1
        end

        if i % width == 1
            print(seqbuf, "  seq:", lpad(seqpos, posw), ' ')
            print(refbuf, "  ref:", lpad(refpos, posw), ' ')
            print(matbuf, " "^(posw + 7))
        end

        print(seqbuf, x)
        print(refbuf, y)
        print(matbuf, lowercase(x) == y ? '|' : ' ')

        if i % width == 0
            print(seqbuf, lpad(seqpos, posw))
            print(refbuf, lpad(refpos, posw))
            print(matbuf)

            println(io, String(take!(seqbuf)))
            println(io, String(take!(matbuf)))
            println(io, String(take!(refbuf)))

            if next_xy !== nothing
                println(io)
                seek(seqbuf, 0)
                seek(matbuf, 0)
                seek(refbuf, 0)
            end
        end
    end

    if i % width != 0
        print(seqbuf, lpad(seqpos, posw))
        print(refbuf, lpad(refpos, posw))
        print(matbuf)

        println(io, String(take!(seqbuf)))
        println(io, String(take!(matbuf)))
        println(io, String(take!(refbuf)))
    end
end

STATES = Union{(i.sig.parameters[end] for i = methods(pathpeaks).ms if i.nargs == 2)...}
function Base.print(io   ::IO,
                    res  ::Tuple{AbstractString, OligoConfigs, STATES},
                    width::Integer = 150)
    print(io, (res[1], res[2], pathpeaks(res[3])), width)
end

for fcn = (:overlapcount, :missingcount, :basecount, :baseposition, :sequence)
    @eval begin
        import ..Oligo: $fcn
        $fcn(ξ::PeakAlignment, ♠::AVECTPEAK) = $fcn(oligosize(ξ), direction(ξ), ♠)
    end
end

function spikealign(ξ::PeakAlignment, left::AbstractVector, right::AbstractVector
                   ) :: Tuple{Float32, Tuple{Int,Int}}
    if length(left) == 0 || length(right) == 0
        return 0f0, 0
    end

    prec   = precision(ξ)
    nmiss  = maxmiss(ξ)
    ileft  = convert(Vector{Int32}, round.((left  .- (left[1]-prec*.5))  ./ prec))
    iright = convert(Vector{Int32}, round.((right .- (right[1]-prec*.5)) ./ prec))
    
    best   = (0, -typemax(Float32), -typemax(Float32))
    imin   = (0, 0)

    for     i = (k for k = 1:length(ileft)  if k -1 < nmiss || length(ileft) -k < nmiss),
            j = (k for k = 1:length(iright) if k -1 < nmiss || length(iright)-k < nmiss)
        val, ix, iy = (0, 0f0, 0f0), length(ileft), length(iright)
        while ix > 0 && iy > 0
            delta = abs(ileft[ix]-ileft[i]-iright[iy]+iright[j]) 
            if delta < 1
                val = (val[1]+1, val[2]-delta, val[3] + right[iy]-left[ix])
            end

            if ileft[ix]-ileft[i]  < iright[iy]+iright[j]
                iy -= 1
            else
                ix -= 1
            end
        end

        if val > best
            best, imin = val, (i, j)
        end
    end

    return best[3]/best[1], imin
end

function grouppeaks(ξ    ::PeakAlignment,
                    left ::AbstractVector,
                    right::AbstractVector) :: Vector{Tuple{Int, Int}}
    maxr = maxreach(ξ)
    inds = Tuple{Int, Int}[]
    if length(left) == 0 || length(right) == 0
        return inds
    end
    mat  = abs.(left' .- right)
    cur  = findmin(mat)[2]
    while mat[cur] < maxr
        push!(inds, (cur[2], cur[1]))
        mat[:,inds[end][1]] .= maxr
        mat[inds[end][2],:] .= maxr
        cur = findmin(mat)[2]
    end
    sort(inds)
end

function sequencedata(ξ::Union{Paths, Trees, PeakAlignment}, λ::AbstractString)
    osize = oligosize(ξ)
    data  = [Float32[] for i = 1:4^osize]
    for i = 1:length(λ)-osize+1
        oid = oligoid(λ[i:i+osize-1])
        push!(data[oid], i)
    end
    data
end

function sequencepeakselection(ξ::PeakAlignment,
                               τ::Experiment,
                               λ::AbstractString) :: Tuple{Vector{Vector{Tuple{Int, Int}}},
                                                           Vector{Peak}}
    osiz = oligosize(ξ)
    data = [Float32[] for i = 1:length(τ)]
    tmp  = Peak[]
    for i = 1:length(λ)-osiz+1
        oid = oligoid(λ[i:i+osiz-1])
        push!(data[oid], i)
        push!(tmp, (oid, length(data[oid])))
    end

    vals = [spikealign(ξ, i, j)[1]
            for (i,j) = zip(τ, data) if length(i) > 0 && length(j) > 0]
    zero = median!(vals)
    [grouppeaks(ξ, i .- zero, j) for (i, j) = zip(data, τ)], tmp
end

function sequencepeaks(ξ::Vector{Vector{Tuple{Int, Int}}}, ♠::AVECTPEAK, τ::Experiment)
    swap  = [Dict{Int,Int}(i) for i = ξ]
    peaks = Peak[]
    for (oid, pkid) = ♠
        newid = get(swap[oid], pkid, 0)
        if newid > 0
            newid > length(τ[oid]) && error("Unknown peak ($oid, $pkid)")
            push!(peaks, (oid, newid))
        end
    end

    peaks
end

function sequencepeakswithmissing(ξ::Vector{Vector{Tuple{Int, Int}}},
                                  ♠::AVECTPEAK,
                                  τ::Experiment)
    swap  = [Dict{Int,Int}(i) for i = ξ]
    peaks = Tuple{Int, Union{Int, Missing}}[]
    for (oid, pkid) = ♠
        newid = get(swap[oid], pkid, 0)
        newid > length(τ[oid]) && error("Unknown peak ($oid, $pkid)")
        push!(peaks, (oid, newid > 0 ? newid : missing))
    end

    peaks
end

for fcn = (:sequencepeaks, :sequencepeakswithmissing)
    @eval function $fcn(ξ::PeakAlignment, τ::Experiment, λ::AbstractString)
        $fcn(sequencepeakselection(ξ, τ, λ)..., τ)
    end
end

for fcn = (:sequencepeakselection, :sequencepeaks, :sequencepeakswithmissing)
    @eval begin
        function $fcn(ξ::Algs, λ::AbstractString, a...)
            $fcn(PeakAlignment(ξ, a...), experiment(ξ), λ)
        end

        function $fcn(τ::Experiment, λ::AbstractString, a...)
            $fcn(PeakAlignment(τ, a...), τ, λ)
        end
    end
end

function sequencebases(ξ::PeakAlignment, λ::AbstractString, ♠::AVECTPEAK)
    bpos  = Int32[]
    j     = 1
    osize = oligosize(ξ)
    dir   = direction(ξ) == goright
    for i = 1:length(λ)-osize+1
        if length(♠) < j
            break
        end
        if oligoid(λ[i:i+osize-1]) == ♠[j][1]
            ibase = dir ? i-1 : length(λ)-i+1
            push!(bpos, ibase)
            j += 1
        end
    end
    bpos
end

function sequencebases(ξ::Algs, λ::AbstractString,
                       ♠::AVECTPEAK  = sequencepeaks(ξ, λ))
    sequencebases(PeakAlignment(ξ), λ, ♠)
end

function DataFrame(τ::Experiment, ♠::AVECTPEAK)
    sz = oligosize(τ)
    DataFrame(oligo        = String[oligoname(sz, i) for (i,_) = ♠],
              oligoid      = Int32[i                 for (i,_) = ♠],
              peakid       = Int32[i                 for (_,i) = ♠],
              position     = peakposition(τ, ♠))
end

function DataFrame(ξ::PeakAlignment, τ::Experiment, ♠::AVECTPEAK)
    df        = DataFrame(τ, ♠)
    df[:base] = convert(Vector{Int32}, baseposition(ξ, ♠)) .+ 1
    df
end

function DataFrame(ξ::Algs, ♠::AVECTPEAK = andreasinsi(ξ))
    DataFrame(PeakAlignment(ξ), experiment(ξ), ♠)
end

function DataFrame(λ::AbstractString, ξ::PeakAlignment,
                   τ::Experiment; bead::Union{Missing,Integer} = missing)
    sz   = oligosize(ξ)
    ♠    = sequencepeakswithmissing(ξ, τ, λ)
    cols = [String[oligoname(sz, i) for (i,_) = ♠],
            Int32[i                 for (i,_) = ♠],
            MInt32[i                for (_,i) = ♠],
            peakposition(τ, ♠),
            Int32[i for i = 1:length(♠)]]
    DataFrame(cols, Symbol[:oligo, :oligoid,
                           bead ≡ missing ? :peakid   : Symbol("peakid_$bead"),
                           bead ≡ missing ? :position : Symbol("position_$bead"),
                           :base])
end

DataFrame(λ::AbstractString, τ::Experiment, a...) = DataFrame(λ, PeakAlignment(τ, a...), τ)
function DataFrame(λ::AbstractString, ξ::Algs, a...)
    DataFrame(λ, PeakAlignment(ξ, a...), experiment(ξ))
end

function DataFrame(ξ::SequenceStats)
    nols = length(ξ.falsepositives)
    osz  = oligosize(ξ.falsepositives)
    DataFrame(oligo          = oligoname.(osz, 1:nols),
              oligoid        = collect(1:nols),
              falsepositives = ξ.falsepositives,
              detected       = ξ.used,
              undetected     = ξ.undetected)
end

function DataFrame(::Type{SequenceStats}, λ::AbstractString, τ::Experiment, a...)
    cnf = PeakAlignment(τ, a...)
    pks = sequencepeakswithmissing(cnf, τ, λ)
    DataFrame(SequenceStats(oligosize(cnf), direction(cnf), τ, pks))
end

function DataFrame(::Type{SequenceStats}, λ::AbstractString, ξ::Algs, a...)
    DataFrame(SequenceStats, λ, experiment(ξ), a...)
end
end
