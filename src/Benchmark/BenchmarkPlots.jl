module BenchmarkPlots

using  BioSymbols
using  BioAlignments: alignment
using  Statistics: mean, std
using  StatsPlots:   @layout, text, default
using  RecipesBase, Query

using ..Oligo:       peakposition, baseposition, estimatedposition, AVECTPEAK,
                   Experiment, oligosize, direction, SequenceStats, oligoname
using ..PeakPaths:    sequence, andreasinsi, startpeaks, experiment,
                   positionloss, Paths
using ..PeakTree:    Trees
using ..BenchmarkPrints: PeakAlignment, sequencebases, sequencepeaks, pairalign
using ..BenchmarkPrints


"""
Create a plot showing the alignment of the experimental sequence to the hairpin
and the residuals of either sequences per position.
"""
function alignmentplot end

"""
Create 4 plots showing each the number of:

* orange: extra bindings
* blue:   experimental bindings accounted for
* red:    bindings missing from the experiment

using independently for each plot:

* top   -left:  the *experimental* sequence, per oligo
* top   -right: the *experimental* sequence, for all oligos at once
* bottom-left:  the *theoretical* sequence, per oligo
* bottom-right: the *theoretical* sequence, for all oligos at once

options are:

* oligos  = [...]: the order in which to display oligos.
* squeeze = `true`: only keep oligos in the experiment or either sequences
"""
function oligostats end

"""
Show residuals per oligo.

Options are

* norm == `:avg`:  an average of residuals
* norm == `:l1`:   an average of the absolute value of residuals
* norm == `:l2`:   an average of the square value of residuals
* norm == `:std`:  the square root of the average of the square value of residuals
* oligos  = [...]: the order in which to display oligos.
* squeeze = `true`: only keep oligos in the experiment or either sequences
"""
function oligoresiduals end

@recipe function f(ξ::PeakAlignment, τ::Experiment, ♠::AVECTPEAK; myticks  = true)
    if myticks
        xticks --> __xticks_peakalignment(sequence(ξ, ♠))
    end

    bpos  = baseposition(ξ, ♠) .+ 1
    yvals = peakposition(τ, ♠) .- bpos
    sig   = round(std(yvals);  digits = 1)

    seriestype :=  :path
    label      --> "experiment:  σ = $sig"
    color      --> :blue

    (bpos, yvals .- mean(yvals))
end

@recipe function f(ξ::PeakAlignment, τ::Experiment, truth::AbstractString; myticks = false)
    if myticks
        xticks --> __xticks_peakalignment(truth)
    end

    peaks = sequencepeaks(ξ, τ, truth)
    bpos  = sequencebases(ξ, truth, peaks) .+ 1
    out   = peakposition(τ, peaks) .- bpos
    sig   = round(std(out);  digits = 1)

    seriestype :=  :path
    label      --> "hairpin: σ = $sig"
    color      --> :green
    (bpos, out .- mean(out))
end

@recipe function f(ξ::Union{Trees, Paths}, ♠ ::Union{AVECTPEAK, AbstractString})
    (PeakAlignment(oligosize(ξ), direction(ξ)), experiment(ξ), ♠)
end

@userplot AlignmentPlot
@recipe function f(h::AlignmentPlot;
                   labels = ["experiment" => :blue, "hairpin" => :green])
    ξ, τ, λ, ♠       = __four_args(h)
    hppks            = sequencepeaks(ξ, τ, λ)
    xtks, xexp, xref = BenchmarkPlots.__xticks_alignmentplot(ξ, λ, ♠, hppks)

    seriestype := :path
    xticks     := collect(1:length(xtks)), xtks
    xlabel     --> ""
    ylabel     --> "Residuals (base)"
    xlims      --> (.5, length(xtks)+.5)

    @series begin
        label  :=  "Score: $(pairalign(λ, ξ, ♠).value)"
        color  --> __default(:white, :background_color_legend,
                             :background_color_subplot, :background_color)
        []
    end

    for (xv, lab, spaces, pks, bp) = zip((xexp, xref), labels, (0, 7), (♠, hppks),
                                         (baseposition(ξ, ♠), sequencebases(ξ, λ, hppks)))
        @series begin
            out    =   peakposition(τ, pks) .- bp
            sig    =   round(std(out);  digits = 1)
            label  :=  "$(lab.first):$(' '^spaces) σ = $sig"
            color  --> lab.second
            xv, out .- mean(out)
        end
    end
end

@userplot OligoResiduals
@recipe function f(h::OligoResiduals; 
                   squeeze      = true,
                   oligos       = nothing,
                   residualnorm = :average,
                   labels       = ["experiment" => :blue,
                                   "harpin"     => :green])
    ξ, τ, λ, ♠  = __four_args(h)

    res         = [zeros(Float32, length(τ)), zeros(Float32, length(τ))]
    available   = zeros(Bool, length(τ))
    for (i, pks)  = enumerate((♠, sequencepeaks(ξ, τ, λ)))
        cnt = zeros(Float32, length(τ))
        tmp   = peakposition(τ, pks) .- baseposition(ξ, pks)
        tmp .-= mean(tmp)
        for (x, y) = zip(pks, tmp)
            res[i][x[1]] += (residualnorm == :l1        ? abs(y) :
                             residualnorm ∈ (:l2, :std) ? y^2    : y)
            cnt[x[1]]    += 1
        end

        res[i] ./= max.(cnt, 1)
        if residualnorm == :std
            res[i] = sqrt.(res[i])
        end

        available .|= cnt .> 0
    end

    xlabs       = [oligoname(oligosize(ξ), i) for i = 1:length(τ)]
    if squeeze
        xlabs = xlabs[available]
        res   = [i[available] for i = res]
        meanv = [mean(i) for i = res]
    else
        meanv = [mean(i[available]) for i = res]
    end

    if oligos != nothing
        inds  = [i for k = oligos for (i, j) = enumerate(xlabs) if j == k]
        append!(inds, [i for (i, j) = enumerate(xlabs) if j ∉ oligos])

        xlabs = xlabs[inds]
        res   = [i[inds] for i = res]
    end

    seriestype := :path
    xticks     := collect(1:length(xlabs)), xlabs
    xlabel     --> ""
    ylabel     --> "$(string(residualnorm)) (base)"
    xlims      --> (.5, length(xlabs)+.5)


    @series begin
        label  :=  "Score: $(pairalign(λ, ξ, ♠).value)"
        color  --> __default(:white, :background_color_legend,
                             :background_color_subplot, :background_color)
        []
    end

    for (i, j, μ) = zip(labels, res, meanv)
        @series begin
            label  :=  "$(i.first): μ = $(round(μ; digits = 1))"
            color  --> i.second
            j
        end
    end
end

@userplot OligoStats
@recipe function f(h::OligoStats;
                   squeeze = true,
                   labels  = ["unused"  => :darkorange,
                              "used"    => :blue,
                              "missing" => :red],
                   oligos  = nothing)
    ξ, τ, λ, ♠ = __four_args(h)
    data       = [begin
                    dt = SequenceStats(oligosize(ξ), direction(ξ), τ, pks)
                    ((dt.falsepositives .+ dt.used,            sum(dt.falsepositives),
                      sum(dt.falsepositives .* .5 .+ dt.used), labels[1]),
                     (dt.used,                                 sum(dt.used),
                      sum(dt.used)*.5,                         labels[2]),
                     (-dt.undetected,                          sum(dt.undetected),
                      -sum(dt.undetected)*.5,                  labels[3]))
                  end for pks = (♠, sequencepeaks(ξ, τ, λ))]
    xlabs      = [oligoname(oligosize(ξ), i) for i = 1:length(τ)]
    if squeeze
        good  = zeros(Bool, length(τ))
        foreach(data) do i good .|= (i[1][1] .- i[3][1]) .> 0 end

        data  = [[(j[1][good], j[2], j[3], j[4]) for j = i] for i  = data]
        xlabs = xlabs[good]
    end

    if oligos !== nothing
        inds  = [i for k = oligos for (i, j) = enumerate(xlabs) if j == k]
        append!(inds, [i for (i, j) = enumerate(xlabs) if j ∉ oligos])
        data  = [[(j[1][inds], j[2], j[3], j[4]) for j = i] for i  = data]
        xlabs = xlabs[inds]
    end

    xtks       = ((collect(1:length(xlabs)), xlabs),
                  (collect(1:length(xlabs)), ["" for _ = 1:length(xlabs)]))

    ylms       = [(min((minimum(i[end][1])          for i = data)...)-1,
                   max((maximum(i[1][1])            for i = data)...)+1),
                  (min((sum(i[end][1])              for i = data)...)-10,
                   max((sum(i[1][1])+sum(i[end][1]) for i = data)...)+10)]

    fgcolor    = __default(:black, :foreground_colour_title, :foreground_colour_subplot,
                           :foreground_colour)
    fontsize   = __default(10, :legendfontsize, :tickfontsize)

    layout          --> @layout [exdtl{0.95w, 0.5h} extot
                                 hpdtl{0.95w, 0.5h} extot]
    alpha           --> .5
    legend          --> false
    title           --> ["Experiment  (score: $(BenchmarkPrints.pairalign(λ, ξ, ♠).value))" "" "sequence" ""]
    title_location  --> :left
    legend          --> false

    for (i, j) = enumerate(data), (dt1, dt2, dt3, info) = j
        @series begin
            seriestype         := :bar
            subplot            :=  (i-1)*2+1
            linealpha          --> 0.0
            label              --> info.first
            color              --> info.second
            xlims              --> (.5, length(dt1)+.5)
            ylims              --> ylms[1]
            xticks             --> xtks[1]
            ylabel             --> "bindings"
            dt1
        end

        @series begin
            seriestype         := :bar
            subplot            :=  (i-1)*2+2
            linealpha          --> 0.0
            label              --> info.first
            color              --> info.second
            [0, sum(dt1)]
        end

        @series begin
            seriestype         := :scatter
            subplot            :=  (i-1)*2+2
            markersize         := 0
            label              --> info.first
            color              --> info.second
            xlims              --> (.5, 2.5)
            xticks             --> nothing#([2], [i == 2 ? "" : "all"])
            ylims              --> ylms[2]
            ymirror            --> true
            showaxis           :=  false
            series_annotations :=  [text(string(dt2), fgcolor, :center, fontsize)]
            [1], [dt3]
        end
    end
end

@userplot BenchmarkDistribution
@recipe function f(h::BenchmarkDistribution; maxscore = 61, density = false)
    msg            = "benchmarkdistribution args are (data, noiseconfig, iterconfig)"
    length(h.args) == 3     || error(msg)
    h.args[1] isa DataFrame || error(msg)
    h.args[2] isa Integer   || error(msg)
    h.args[3] isa Integer   || error(msg)
    data, noiseconfig, iterconfig = h.args

    xx = __dist_data(data, noiseconfig, iterconfig, maxscore)
    xlabel --> "errors"
    ylabel --> "sequences (%)"

    if density
        @series begin
            seriestype := :bar
            xx[:score], xx[:density]
        end
    end

    @series begin
        seriestype  := :path
        xx[:score], xx[:csum]
    end

    meanv = sum(xx[:score] .* xx[:density])
    score = 0.5
    for (i, x) = enumerate(xx[:score])
        (x < meanv) && continue
        if x == meanv || i == 1
            score = x
        else
            rho   = (meanv-xx[:score][i-1]) / (x - xx[:score][i-1])
            score = (1. - rho) * xx[:csum][i-1] + rho * xx[:csum][i]
        end
        break
    end

    @series begin
        seriestype        :=  :scatter
        label             :=  ""
        markerstrokewidth --> 0
        [meanv], [score]
    end
end

@userplot BenchmarkDistributions
@recipe function f(h::BenchmarkDistributions; twincolor = :red, maxscore = 61)
    msg            = "benchmarkdistributions args are (data)"
    length(h.args) == 1     || error(string(msg, " not ", h.args))
    h.args[1] isa DataFrame || error(string(msg, " not ", h.args))
    data           = h.args[1]

    ids    = sort!(unique(data[[:noiseconfig, :iterconfig]]))
    shape  = (div(size(ids)[1], length(unique(ids[:iterconfig]))),
              length(unique(ids[:iterconfig])))
    layout --> shape
    normalize :=  false
    legend    :=  false
    grid      --> false
    xlims     --> (0, maxscore+1)
    #ylims     --> (0, .7)
    for i = zip(columns(ids)...)
        xx = __dist_data(data, i[1], i[2], maxscore)

        subplot := (i[1]-1)*shape[2]+i[2]
        @series begin
            seriestype := :bar
            xx[:score], xx[:density]
        end

        @series begin
            if i[1] == maximum(ids[:noiseconfig])
                xlabel --> "errors"
            else
                xlabel := ""
            end
            if i[2] == minimum(ids[:iterconfig])
                ylabel --> "sequences (%)"
            else
                ylabel := ""
            end
            seriestype  := :path
            xx[:score], xx[:csum]
        end
    end
end

function meanerrorrate(data, noiseconfig, iterconfig)
    __dist_data(data, noiseconfig, iterconfig)
    cur   =(((data
              |> @filter(_.noiseconfig == noiseconfig && _.iterconfig == iterconfig))
              |> DataFrame))

    xx = @from g in data begin
            @group g by g.score into k
            @select {score = key(k), count = length(k)}
            @collect DataFrame
    end

    xx[:density] = xx[:count] ./ nrow(cur)
    xx[:csum]    = cumsum(xx[:density])
    mean(xx[:score] .* xx[:csum])
end

"""
Compute statistics over each configuration.

Output is:

1. iterconfig
2. noiseconfig
3. meanscore:    average score over all tests
3. percentile80: 80th percentile score value
3. scoreinf20:   percentage of tests with a score of less than 20
"""
function errorstatistics(data)
    @from i in data begin
        @group i by (i.iterconfig, i.noiseconfig) into k
        @select {iterconfig   = key(k)[1], noiseconfig = key(k)[2],
                 meanscore    = sum(k.score)/length(k),
                 percentile80 = percentile(k.score, 80),
                 errorinf20   = sum(k.score .< 20) / length(k)*100.}
        @collect DataFrame
    end
end

function __four_args(h)
    ξ   = τ = λ = ♠ = nothing
    msg = string("$(lowercase(string(typeof(h)))) args are (path/tree, sequence [,peaks]) or ",
                 "(peak alignment, experiment, sequence, peaks))")
    if length(h.args) < 2 || length(h.args) > 4
        error(msg)

    elseif length(h.args) <= 3
        types = Union{Paths, Trees}, AbstractString, AVECTPEAK
        if !all(i isa j for (i,j) = zip(h.args, types[1:length(h.args)]))
            error(msg)
        end

        ξ = PeakAlignment(oligosize(h.args[1]), direction(h.args[1]))
        τ = experiment(h.args[1])
        if length(h.args) == 3
            λ, ♠ = h.args[end-1:end]
        else
            λ = h.args[end]
            ♠ = andreasinsi(h.args[1])
        end

    elseif length(h.args) == 4
        if !all(i isa j for (i,j) = zip(h.args, types))
            error(msg)
        end
        ξ, τ, λ, ♠ = h.args
    end

    ξ, τ, λ, ♠
end

function __dist_data(data, noiseconfig, iterconfig, maxscore)
    cur   =(((data
              |> @filter(_.noiseconfig == noiseconfig && _.iterconfig == iterconfig))
              |> DataFrame))

    xx = @from g in cur begin
            @orderby g.score
            @where g.score < maxscore
            @group g by g.score into k
            @select {score = key(k), count = length(k)}
            @collect DataFrame
    end

    xx[:density] = xx[:count] ./ nrow(cur)
    xx[:csum]    = cumsum(xx[:density])
    xx
end

function __xticks_peakalignment(seq::AbstractString)
    xvals  = [string(j, (i % 20 == 0 ? "\n$i" : "")) for (i,j) = enumerate(seq)]
    collect(1:length(seq)), xvals
end

function __default(dlft, args...)
    found = :match
    foreach(args) do i found == :match && (found = default(i)) end
    found == :match ? dflt : found
end

function __xticks_alignmentplot(ξ::PeakAlignment, λ::AbstractString,
                                ♠exp::AVECTPEAK,
                                ♠ref::AVECTPEAK)
    xticks = String[]

    aln  = alignment(BenchmarkPrints.pairalign(λ, ξ, ♠exp))
    gap  = BioSymbols.gap(eltype(λ))

    info = [(Int[1, 1], convert(Vector{Int}, baseposition(ξ, i)) .+ 1, Int32[])
            for i = (♠exp, ♠ref)]

    for (i, symbs) = enumerate(aln)
        tick = ""
        for ((pos, bp, vals), x) = zip(info, symbs)
            if x == gap
                tick *= x
            else
                if (pos[1] <= length(bp) && bp[pos[1]] < pos[2])
                    push!(vals, i)
                    pos[1] += 1
                    tick   *= x
                else
                    tick   *= uppercase(x)
                end
                pos[2] += 1
            end
        end

        tick = string(tick[1], symbs[1] == symbs[2] ? "\n|\n" : "\n \n", tick[2],
                      (i % 10 == 0 ? ( '\n', i) : ())...)
        push!(xticks, tick)
    end
    xticks, info[1][end], info[2][end]
end
end
