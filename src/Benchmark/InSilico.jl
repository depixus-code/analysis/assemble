module InSilico
export                createdataframe, clone, andreasinsi, oligoname, pathpeaks
using DataFrames
using BioAlignments
using ..Oligo:          clone, oligoname, pathpeaks, goleft, goright
using ..PeakPaths:       DeltaScorePath, sequence, andreasinsi, experiment
using ..RandomSequence: randsequence, randexperiment, batchexperiment

const Opts = Dict{Symbol, Any}
DEFAULT_ITR = Opts(:searchwindow   => 10.f0,
                   :oligosize      => 3,
                   :minoverlap     => 1,
                   :direction      => goright,
                   :positionloss   => .33f0,
                   :searchdepth    => 4,
                   :falsepositives => 1f0,
                   :iterator       => DeltaScorePath,
                   :spoonerism     => false,
                   :remaining      => false,
                   :depth          => 3,
                   :bothsides      => 10)
DEFAULT_RND = Opts(:sigma    => 2.f0,
                   :stretch  => 0.f0,
                   :bias     => 0.f0,
                   :spurious => 0.f0,
                   :missing  => 0.f0)

using ProgressMeter

checkoptions(opts::Dict{Symbol, Any}) = checkoptions(opts[:iterator], opts)

function createdataframe(nseq::Int, nruns::Int = 1;
                         sequencelength :: Int                                = 100,
                         rnd            :: Union{Opts, AbstractArray{Opts,1}} = Opts(),
                         itr            :: Union{Opts, AbstractArray{Opts,1}} = Opts(),
                         defaultitr     :: Opts                               = DEFAULT_ITR,
                         defaultrnd     :: Opts                               = DEFAULT_RND,
                         progress       :: Bool                               = true)
    "returns an array of dicts where containing default values unlesss specified"
    function setopts(name, dflt, opts)
        if typeof(opts) <: Opts
            # change into an array
            opts = Opts[opts]
        end

        # use default values unless specifics are given
        opts = Opts[merge(dflt, x) for x = opts]

        # add an id number
        for i = 1:length(opts)
            get!(opts[i], name, i)
        end

        opts
    end

    itr   = setopts(:iterconfig,  defaultitr, itr)
    for (j, i) = Iterators.enumerate(itr)
        i[:itertype]   = replace(replace(replace(string(i[:iterator]), "PeakPath." => ""),
                                         "Path" => ""), "PeakTree." => "")
        i[:iterconfig] = j

    end

    isbatch = all(startswith("Batch", i[:itertype]) for i = itr)
    rnd   = setopts(:noiseconfig, defaultrnd, rnd)
    runs  = nothing
    meter = nothing
    if progress
        meter = Progress(nseq*length(rnd)*nruns*length(itr); dt = 0.1)
        next!(meter)
    end
    for i = 1:nseq
        seq = randsequence(sequencelength)
        for noise = rnd
            for k = 1:nruns
                xp = randexperiment(seq;
                                    batched = isbatch,
                                    (j for j = noise if j.first != :noiseconfig)...)[end]
                for (ic, cnf) = enumerate(itr)
                    tmp = if startswith(cnf[:itertype], "Batch") && !isbatch
                        batchexperiment(xp)
                    else
                        xp
                    end
                    path  = clone(cnf[:iterator](); experiment = tmp, cnf...)
                    found = sequence(path,
                                     andreasinsi(path;
                                                 falsepositives = cnf[:falsepositives],
                                                 spoonerism     = cnf[:spoonerism],
                                                 remaining      = cnf[:remaining]))
                    dist  = pairalign(LevenshteinDistance(), found, seq)
                    tmp   = DataFrame(;seqid = [hash(seq)],
                                      runid  = [k],
                                      Dict(u=> [j] for (u, j) = noise)...,
                                      Dict(u=> [j] for (u, j) = cnf if u != :iterator)...,
                                      score       = [BioAlignments.score(dist)],
                                      deletion    = [count_deletions(dist.aln)],
                                      insertion   = [count_insertions(dist.aln)],
                                      mismatch    = [count_mismatches(dist.aln)])

                    if runs == nothing
                        runs = tmp
                    else
                        foreach(setdiff(names(tmp), names(runs))) do i runs[i] = NaN32 end
                        foreach(setdiff(names(runs), names(tmp))) do i tmp[i] = NaN32 end
                        runs = [tmp; runs]
                    end
                    if progress
                        next!(meter)
                    end
                end
            end
        end
    end
    runs
end

function oneframe(rnd            :: Opts = Opts(),
                  itr            :: Opts = Opts();
                  sequencelength :: Int  = 100,
                  defaultitr     :: Opts = DEFAULT_ITR,
                  defaultrnd     :: Opts = DEFAULT_RND)
    seq, _, data = randexperiment(sequencelength; merge(defaultrnd, rnd)...)
    cnf          = merge(defaultitr, itr)
    path         = clone(cnf[:iterator](); cnf...)
    found        = sequence(path, andreasinsi(path))
    dist         = pairalign(LevenshteinDistance(), found, seq)

    path, found, dist
end
end
