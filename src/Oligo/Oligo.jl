module Oligo

using Printf
using ..Accessor

function searchdepth    end
function positionloss   end
function estimatedposition   end
pathpeaks(::Nothing) = Peak[] 
for i = (:pathpositions, :pathscores, :pathzeros)
    @eval $i(::Nothing) = Float32[]
end

for i = (:pathbases, :pathoverlaps)
    @eval $i(::Nothing) = Int[]
end

include("_Oligo.jl")
include("_Node.jl")
include("_Neighbours.jl")
include("_Indexes.jl")
end
