@enum Direction::Bool goleft=false goright=true
"""
Id for oligo batched with its reverse complement
"""
primitive type BatchedOligo <: Signed 64 end

for fcn = (:+, :*, :-, :(<<), :(>>), :%)
    @eval function Base.$fcn(x::BatchedOligo, y::BatchedOligo) :: BatchedOligo
        reinterpret(BatchedOligo, $fcn(reinterpret(Int, x), reinterpret(Int, y)))
    end
    for tpe = (Int, UInt)
        @eval function Base.$fcn(x::BatchedOligo, y::$tpe) :: BatchedOligo
            reinterpret(BatchedOligo, $fcn(reinterpret(Int, x), y))
        end
    end
end

for fcn = (:<, :>, :(<=), :(>=), :(==))
    @eval function Base.$fcn(x::BatchedOligo, y::BatchedOligo)
        $fcn(reinterpret(Int, x), reinterpret(Int, y))
    end
end

ь(x::Int)                           = reinterpret(BatchedOligo, x)
BatchedOligo(x::Int)                = reinterpret(BatchedOligo, x)
Base.Int(x::BatchedOligo)           = reinterpret(Int,   x)
Base.parse(::Type{BatchedOligo}, x) = reinterpret(BatchedOligo, parse(Int, x))
Base.show(x::IO, y::BatchedOligo)   = print(x, "ь$(reinterpret(Int, y))")

macro o_str(seq)
    oligoid(Int, seq)
end

macro or_str(seq)
    oligoid(BatchedOligo, seq)
end

macro ob_str(seq)
    out = oligoid(BatchedOligo, seq)
    min(out, rc(length(seq), out))
end

export @or_str, @ob_str, @o_str

function Base.convert(::Type{Direction}, τ::Symbol)
    if τ ∈ (:left, :goleft)
        goleft
    elseif τ ∈ (:right, :goright)
        goright
    else
        throw(KeyError("Symbol ($τ) should be either :left or :right"))
    end
end

Base.:~(τ::Direction) = τ ≡ goleft ? goright : goleft

function oligoid(::Type{T}, name::String) :: T where {T <: Integer}
    if name[1] ∉ "atcgATGC"
        return parse(T, seq)
    end

    res::T = T(1) # adding 1 as julia indexes start at 1
    for i = 1:length(name)
        if name[i] == 't' || name[i] == 'T'
            res +=  T(1) << T(2*(i-1))
        elseif name[i] == 'c' || name[i] == 'C'
            res +=  T(2) << T(2*(i-1))
        elseif name[i] == 'g' || name[i] == 'G'
            res +=  T(3) << T(2*(i-1))
        elseif name[i] != 'a' && name[i] != 'A'
            error("unknown alphabet")
        end
    end

    res
end
oligoid(::Type{Tuple{T, Int}}, ν::String) where T = oligoid(T, ν)
oligoid(name::String) :: Int                      = oligoid(Int, name)

function oligoname(size::Int, val::Integer)
    string(("atcg"[i+1] for i in digits(Int(val)-1; base = 4, pad = size))...)
end

function overlaps(side    ::Direction,
                  noverlap::Integer,
                  size    ::Integer,
                  oligo   ::Integer)::StepRange{Int, Int}
    olig = Int(oligo)-1
    if side == goright
        1+olig>>2(size-noverlap) : 1<<2noverlap : 1<<2size
    else
        val = (olig % (1<<2noverlap)) << 2(size-noverlap)
        1+val:1:val + 1<<2(size-noverlap)
    end
end

function hasoverlap(size::Integer, noverlap::Integer, ol1::T, ol2::T) :: Bool where {T <:Integer}
    ((ol2-1) % (1<<2noverlap)) == ((ol1-1)>>2(size-noverlap))
end

const PEAK_TYPES     = Union{Integer, Tuple{T, Int} where {T<: Integer}}
const PEAKVECT_TYPES = AbstractVector{<:Union{Integer, Tuple{T, Int} where {T<: Integer}}}

function overlapcount(σ::Integer, ol1::T, ol2::T) :: Int where {T <: Integer}
    ol1 -= T(1)
    ol2 -= T(1)
    for i = σ-1:-1:0
        if (ol2 % (1<<2i)) == (ol1>>2(σ-i))
            return i
        end
    end
    σ
end

overlapcount(σ::Integer, ol1::Tuple, ol2::Tuple) = overlapcount(σ, ol1[1], ol2[1])

function overlapcount(σ::Integer, θ::Direction, ol1::PEAK_TYPES, ol2::PEAK_TYPES)
    θ ≡ goright ? overlapcount(σ, ol1, ol2) : overlapcount(σ, ol2, ol1)
end

function hasoverlap(σ::Integer, ol1::PEAK_TYPES, ol2::PEAK_TYPES) :: Bool
    overlapcount(σ, ol1, ol2) > 0
end

"""
Compute theoretical position of peaks depending on their overlap with previous ones
"""
function baseposition(osize::Integer, odir::Direction, peaks::PEAKVECT_TYPES) :: Vector{Float32}
    arr = if odir ≡ goright
        Float32[osize-overlapcount(osize, peaks[i], peaks[i+1]) for i = 1:length(peaks)-1]
    else
        Float32[osize-overlapcount(osize, peaks[i+1], peaks[i]) for i = 1:length(peaks)-1]
    end
    arr = append!(Float32[0f0], cumsum!(arr, arr))
    odir ≡ goright ? arr : arr[end]-arr
end

"""
Compute the sum of overlaps given a list of peaks
"""
function overlapcount(osize::Integer, odir::Direction, peaks::PEAKVECT_TYPES)
    if length(peaks) == 0
        return 0
    end
    if odir ≡ goright
        return sum(overlapcount(osize, peaks[i][1], peaks[i+1][1]) for i = 1:length(peaks)-1)
    end
    sum(overlapcount(osize, peaks[i+1][1], peaks[i][1]) for i = 1:length(peaks)-1)
end

"""
Compute the number of peaks missing from the sequence
"""
function missingcount(osize::Integer, odir::Direction, peaks::PEAKVECT_TYPES)
    (osize-1) * (length(peaks)-1) - overlapcount(osize, odir, peaks)
end

"""
Compute the size of the sequence found
"""
function basecount(osize::Integer, odir::Direction, peaks::PEAKVECT_TYPES)
    length(peaks)-1+osize+missingcount(osize, odir, peaks)
end

"""
Compute the sequence from a list of peaks
"""
function sequence(λ::Integer, θ::Direction, peaks::AbstractVector{T}) where {T <: Integer}
    if length(peaks) == 0
        return ""
    end

    seq = oligoname(λ, peaks[1])
    if θ ≡ goright
        for i in @view peaks[2:end]
            ind  = min(λ, overlapcount(λ, oligoid(T, seq[end-λ+1:end]), i)+1)
            seq *= oligoname(λ, i[1])[ind:λ]
        end
    else
        for i in @view peaks[2:end]
            ind = max(1, λ-overlapcount(λ, i[1], oligoid(T, seq[1:λ])))
            seq = oligoname(λ, i)[1:ind]*seq
        end
    end
    seq
end

sequence(λ::Integer, peaks::AbstractVector{<:Integer}) = sequence(λ, goright, peaks)

function sequence(λ::Integer, θ::Direction, peaks::AbstractVector{Tuple{T, Int}}) where {T <: Integer}
    sequence(λ, θ, [i[1] for i in peaks])
end

for op in (:overlaps, :overlapcount)
    @eval begin
        function $op(ol1::AbstractString, ol2::AbstractString)
            if length(ol1) != length(ol2)
                error("Not implemented")
            end
            ($op)(length(ol1), oligoid(ol1), oligoid(ol2))
        end
    end
end

"Returns the reverse complement"
function rc(λ::K, υ::T) :: T where {K <: Integer, T <: Integer}
    inv = Int(υ) - 1
    out = T(0)
    for i = 0:λ-1
        out  += T(((inv % 2) == 0 ? (inv%4)+1 : (inv%4)-1) << 2(λ-i-1))
        inv >>= 2
    end
    out+T(1)
end

rc(λ::AbstractString) :: String = oligoname(length(λ), rc(length(λ), oligoid(λ)))
