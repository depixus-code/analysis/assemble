"""
Iterates over overlapping oligos returning the range of adjacent peaks
"""
@oligoconfigs struct Neighbour{T} <: OligoConfigs
    experiment  ::Experiment
    noverlap    ::Int
    currentpeak ::Tuple{T, Int}
    currentpos  ::Float32
end

# function Neighbour(ξ::OligoConfigs, τ::Experiment, δ::Integer, ♠::Tuple{T, Int}) where T
#     Neighbour{T}(ξ, τ, δ, ♠, NaN32)
# end
Neighbour{T}(ξ::OligoConfigs, τ::Experiment, δ::Integer, ♠::Tuple{T, Int}) where {T}= Neighbour{T}(ξ, τ, δ, ♠, NaN32)

function currentpos(itr::Neighbour)
    psh = positionshift(itr, noverlap(itr))
    if isfinite(itr.currentpos)
        itr.currentpos+psh
    else
        peakposition(experiment(itr), currentpeak(itr))+psh
    end
end

struct NeighbourState{T}
    itr :: StepRange{T,T}
    rng :: T
    pos :: Tuple{Float32, Float32}
end

function Base.iterate(ξ::Neighbour{T}, τ::Union{NeighbourState{T}, Nothing} = nothing) where T
    if τ ≡ nothing
        pos        = currentpos(ξ)
        itr        = overlaps(ξ, noverlap(ξ), currentpeak(ξ)[1])
        oligo, rng = @ifsomething iterate(T(itr.start):T(itr.step):T(itr.stop))
        τ          = NeighbourState{T}(itr, rng, (pos-searchwindow(ξ), pos+searchwindow(ξ)))
    else
        oligo, rng = @ifsomething iterate(τ.itr, τ.rng)
    end

    pos        = peakposition(experiment(ξ), oligo)
    out        = searchsortedfirst(pos, τ.pos[1]):searchsortedfirst(pos, τ.pos[2])-1
    ((oligo, out), NeighbourState(τ.itr, rng, τ.pos))
end

Base.length(itr::Neighbour)               = 1<<2(oligosize(itr)-noverlap(itr))
Base.eltype(::Type{Neighbour{T}}) where T = Tuple{T, UnitRange{Int64}}

"""
Find overlapping peaks adjacent to the provided peak
"""
function neighbours(cnf      :: OligoConfigs,
                    data     :: Experiment,
                    noverlap :: Integer,
                    peak     :: APEAK,
                    position :: Float32 = NaN32)
    itr = Neighbour(cnf, data, noverlap, peak, position)
    Iterators.flatten(((oligo, ipk) for ipk = peaks) for (oligo, peaks) = itr)
end

"""
Count the number of adjacent peaks for overlapping oligos
"""
function neighbourcount(cnf     :: OligoConfigs,
                       data     :: Experiment,
                       noverlap :: Integer,
                       peak     :: APEAK,
                       position :: Float32 = NaN32) :: Int
    sum(length(i[2]) for i = Neighbour(cnf, data, noverlap, peak, position))
end

function neighbourcount(cnf::OligoConfigs, data::Experiment, noverlap::Integer,
                        peaks::AVECTPEAK):: Vector{Int}
    Int[neighbourcount(cnf, data, noverlap, i) for i = peaks]
end

function neighbourcount(cnf::OligoConfigs, data::Experiment, noverlap::Integer
                       ) :: Vector{Int}
    Int[neighbourcount(cnf, data, noverlap, i) for i = data]
end

"""
Return a list of peaks adjacent to the selected position
"""
function adjacentpeaks(data::Experiment, width::Real, pos::Real
                      ) :: UnitRange{Int}
    inds(oligo, op) = searchsortedfirst(peakposition(data, oligo), op(pos, width))
    UnitRange{Int}[inds(i,-):inds(i,+) for i = 1:length(data)]
end
