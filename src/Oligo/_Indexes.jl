struct PeakIndexes{T <: APEAK}
    isr         ::Bool
    starts      ::Vector{Int}
    stops       ::Vector{Int}
    peak        ::T
    pos         ::Float32
    experiment  ::Experiment
    oligosize   ::Int

    function PeakIndexes{T}(ξ::OligoConfigs, τ::Experiment, ♠start,
                            ♠stop::AbstractVector{T}, ♠cur::T, ρ::Float32) where T
        isr     = direction(ξ) ≡ goright
        vect(x) = x ? ones(Int, length(τ)) : lengths(τ)
        self    = new(isr, vect(isr), vect(!isr), ♠cur, ρ, τ, oligosize(ξ))
        _pk_update!(self, isr,  ♠start, self.starts)
        _pk_update!(self, !isr, ♠stop,  self.stops)
        self
    end
end

update!(ι::PeakIndexes{T}, ♠::AbstractVector{T}) where T = _pk_update!(ι, ι.isr, ♠, ι.starts)

function haspathpeaks(ι::PeakIndexes, ξ::OligoConfigs)
    for novr = overlaps(ξ), i = _pk_neighbour(ι, ξ, novr)
        (length(_pk_range(ι, i)) > 0) && return true
    end
    false
end

macro pathpeaks(T, ξ, inds, filtering!, sorting!)
    quote
        cnf  = $(esc(ξ))
        inds = $(esc(inds))
        used = $(esc(:(Vector{$T})))[]
        for novr = overlaps(cnf)
            good = $(esc(T))[(i[1], j)
                             for i = _pk_neighbour(inds, cnf, novr)
                             for j = _pk_range(inds, i)]
            $((filtering! == :nothing ? () : (:($(esc(filtering!))(cnf, good)),))...)
            if !$(esc(:isempty))(good)
                $((sorting! ≡ :nothing ? () : (:($(esc(sorting!))(cnf, good)),))...)
                $(esc(:push!))(used, good)
                update!(inds, good)
            end
        end

        used
    end
end

macro peakindexes(T, ξ, starts, cur, pos)
    quote
        cnf = $(esc(ξ))
        PeakIndexes{$(esc(T))}(cnf, $(esc(:experiment))(cnf),
                               $(esc(starts)), $(esc(:stoppeaks))(cnf),
                               $(esc(cur)), $(esc(pos)))
    end
end

function _pk_neighbour(inds::PeakIndexes, ξ::OligoConfigs, novr::Int)
    Neighbour(ξ, inds.experiment, novr, inds.peak, inds.pos)
end

@generated function _pk_update!(ι   ::PeakIndexes{T},
                                isr ::Bool, ♠,
                                inds::AbstractVector{Int}) where T
    check(x) = x ? :((j >= inds[i]) && (inds[i] = j + 1)) :
                   :((j <= inds[i]) && (inds[i] = j - 1)) 
    batched  = T.parameters[1] ≡ BatchedOligo
    function code(x)
        quote 
            for (i, j) = ♠
                $(check(x))
                $((batched ? (:(i = rc(ι.oligosize, i)), check(x)) : ())...)
            end
        end
    end

    quote
        if isr
            $(code(true))
        else
            $(code(false))
        end
    end
end


function _pk_range(inds::PeakIndexes, rng::Tuple{<:Integer,UnitRange{Int}})
    @inbounds if inds.isr
        out = max(inds.starts[rng[1]], rng[2].start):min(inds.stops[rng[1]], rng[2].stop)
    else
        out = max(inds.stops[rng[1]], rng[2].start):min(inds.starts[rng[1]], rng[2].stop)
    end
    out
end
