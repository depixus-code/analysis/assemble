using Base.Iterators: zip
using Statistics: mean, cov

const MFloat32   = Union{Missing, Float32}
const MInt32     = Union{Missing, Int32}
const MInt       = Union{Missing, Int}

"""
pair of oligoid, peak index within a track
"""
const Peak       = Tuple{Int, Int}

"""
pair of oligoid, peak index within a track
"""
const APEAK      = Tuple{T, Int} where {T<:Integer}

const AVECTPEAK  = AbstractVector{Tuple{T, Int}} where {T <: Integer}
const MAVECTPEAK = AbstractVector{Tuple{T, Union{Missing,Int}}} where {T <: Integer}

hasmissing(::Type{AVECTPEAK})  = true
hasmissing(::Type{MAVECTPEAK}) = true

abstract type OligoConfigs end
oligoname(ξ::OligoConfigs, ♠::APEAK)               = oligoname(oligosize(ξ), ♠[1])
oligoid(♠::APEAK)                                  = ♠[1]
oligopeakid(♠::APEAK)                              = ♠[2]
oligotype(::Type)                                            = Int
oligotype(::Type{Tuple{T,Int}})           where T <: Integer = T
oligotype(::Type{Array{Tuple{T,Int}, 1}}) where T <: Integer = T
peaktype(ξ::Type)                                            = Peak

const OLIGOCONFIG_FIELDS = quote
    searchwindow::Float32
    minoverlap  ::Int
    oligosize   ::Int
    direction   ::Direction
end

function _oligoconfigs(mdl, τ)
    args              = (τ.head == :escape ? τ.args[1] : τ).args
    args[3].args      = vcat(OLIGOCONFIG_FIELDS.args, args[3].args)
    name, atype, opts = Accessor.extractinfo(τ, :(::))

    existing = Symbol[i.args[1] for i = OLIGOCONFIG_FIELDS.args
                      if i isa Expr && i.head == :(::)]
    if name == :OligoNode
        existing = Symbol[]
    end

    sigs    = filter(opts) do x x.args[1] ∉ existing end
    getters = Accessor.creategetters(mdl, atype, sigs, true)

    if name == :OligoNode
        existing = Symbol[i.args[1] for i = sigs]
        sigs     = Expr[]
    end

    if name isa Expr && name.head == :curly
        temp = name.args[2]
        name = name.args[1]
        cstr = quote
            function $(esc(name))(ξ::OligoConfigs, $(sigs...)) where $temp
                $(esc(name)){$temp}($((:($i(ξ)) for i = existing)...),
                                    $((i.args[1] for i = sigs)...))
            end
        end
    else
        cstr = quote
            function $(esc(name))(ξ::OligoConfigs, $(sigs...))
                $(esc(name))($((:($i(ξ)) for i = existing)...), 
                             $((i.args[1] for i = sigs)...))
            end
        end
    end

    quote
        Base.@__doc__ $τ
        $(esc(cstr))
        export $name
        $(esc(getters))
    end
end

macro oligoconfigs(mdl, τ)
    _oligoconfigs(mdl, τ)
end

macro oligoconfigs(τ)
    :(@oligoconfigs $__module__ $(esc(τ)))
end

function peaktypecode(τ)
    name = gettypename(τ; raw = false)
    pk   = startswith(string(name), "Batch") ? BatchedOligo : Int
    if pk !== Int
        pkt  = Tuple{pk, Int}
        if name isa Expr && name.head == :curly
            peaktype = (quote
                            $(esc(:peaktype))(::Type{<:$(name.args[1])}) = $pkt
                            $(esc(:oligotype))(::Type{<:$(name.args[1])}) = $pk
                        end,)
        else
            peaktype = (quote
                            $(esc(:peaktype))(::Type{$name}) = $pkt
                            $(esc(:oligotype))(::Type{$name}) = $pk
                        end,)
        end
    else
        peaktype = ()
    end
    pk, peaktype
end

@oligoconfigs struct OligoNode <: OligoConfigs
end

const ProbeTest  = Array{Float32,   1}
const Experiment = Array{ProbeTest, 1}
emptyexperiment(κ::Experiment) = all(length(peakposition(κ, i)) == 0 for i = 1:length(κ))
lengths(τ::Experiment)         = Int[length(i) for i = τ]

"The list of oligos having at least 1 binding"
isused(τ::Experiment)               = Int[i for (i,j) = enumerate(τ) if length(j) > 0]
isused(::Type{Bool}, τ::Experiment) = Bool[length(i) > 0 for i = τ]
nbindings(::Type, τ::Experiment)    = sum(length(i) for i = τ)
function nbindings(::Type{BatchedOligo}, τ::Experiment)
    sz = oligosize(τ)
    sum(length(j) for (i, j) = enumerate(τ) if rc(sz, i) >= i)
end

"The list of oligos with no bindings"
isunused(τ::Experiment)               = Int[i for (i,j) = enumerate(τ) if isempty(j)]
isunused(::Type{Bool}, τ::Experiment) = Bool[length(i) == 0 for i = τ]

overlaps(ξ::OligoConfigs) :: StepRange{Int, Int} = oligosize(ξ)-1:-1:minoverlap(ξ)

function start(odir::Direction, τ::Experiment)
    if odir ≡ goright
        ind = findmin([length(i) > 0 ? i[1] : Inf32 for i = τ])[2]
        (ind, 1)
    else
        ind = findmax([length(i) > 0 ? i[1] : -Inf32 for i = τ])[2]
        (ind, length(peakposition(τ, ind)))
    end
end

initialpeaks(::Type{Peak}, odir::Direction, τ::Experiment) = Peak[start(odir, τ)]
function initialpeaks(::Type{Tuple{BatchedOligo, Int}}, odir::Direction, τ::Experiment)
    i, j = start(odir, τ)
    Tuple{BatchedOligo, Int}[(BatchedOligo(k), j) for k = (i, rc(oligosize(τ), i))]
end

"The sequence size of the oligos"
function oligosize(nols::Integer) :: Int
    osize = 0
    while nols > 1
        nols  >>= 2
        osize  += 1
    end
    osize
end
oligosize(data::Union{Array, Experiment}) = oligosize(length(data))

function overlaps(ξ::OligoConfigs, noverlap::Integer, oligo::Integer)::StepRange{Int, Int}
    overlaps(direction(ξ), noverlap, oligosize(ξ), oligo)
end

for fcn = (:overlapcount, :missingcount, :basecount, :baseposition, :sequence)
    @eval begin
        function $fcn(ξ::OligoConfigs, ♠::Oligo.PEAKVECT_TYPES)
            $fcn(oligosize(ξ), direction(ξ), ♠)
        end

    end
end

function pathoverlaps(ξ::OligoConfigs, ♠::AVECTPEAK)
    osize = oligosize(ξ)
    @inbounds out = Float32[osize; 
         if direction(ξ) ≡ goright
             map(1:length(♠)-1) do i overlapcount(osize, ♠[i], ♠[i+1]) end
         else
             map(1:length(♠)-1) do i overlapcount(osize, ♠[i+1], ♠[i]) end
         end]
    out
end

pathbases(ξ::OligoConfigs, ♠::AVECTPEAK) = cumsum(oligosize(ξ) .- pathoverlaps(ξ, ♠))

overlapcount(ξ::OligoConfigs, left, right) = overlapcount(oligosize(ξ), direction(ξ), left, right)

for i = (Int,), tpe = [i, AbstractVector{i}]
    @eval function peakposition(τ::Experiment, oligo::$i, ♠::$tpe)
        @inbounds x = τ[oligo][♠]
        x
    end
end
function peakposition(τ::Experiment, oligo::Integer) :: Vector{Float32}
    @inbounds  x::Vector{Float32} = τ[oligo]
    x
end
function peakposition(τ::Experiment, ♠::APEAK) :: Float32
    @inbounds x::Float32 = τ[♠[1]][♠[2]]
    x
end
function peakposition(τ::Experiment, ♠::AVECTPEAK) :: Vector{Float32}
    @inbounds x::Vector{Float32} = [τ[i][j] for (i, j) = ♠]
    x
end

function peakposition(τ::Experiment, ♠::MAVECTPEAK) :: Vector{MFloat32}
    @inbounds x::Vector{MFloat32} = [j ≡ missing ? missing : τ[i][j] for (i, j) = ♠]
    x
end

function peakposition(τ::Experiment, oligo::Integer, ♠::AVECTPEAK) :: Vector{Float32}
    @inbounds vals               = τ[oligo]
    @inbounds x::Vector{Float32} = [vals[j] for (_, j) = ♠]
    x
end

peakposition!(τ::Experiment, oligo::Integer, ♠) = setindex!(τ, sort!(♠), oligo)
function peakposition!(τ::Experiment, oligo::Integer, stretch::Float32, bias::Float32)
    τ[oligo] = τ[oligo] * stretch + bias
    τ[oligo]
end
function peakposition(τ::Experiment, ρ::AbstractArray{Float32, 2})
    [ρ[i,1] == 1f0 && ρ[i,2] == 0f0 ? τ[i] : (τ[i] .* ρ[i,1] .+ ρ[i,2])
     for i = 1:length(τ)]
end

function peakposition(ξ::OligoConfigs, τ::Experiment, noverlap::Integer, ♠::APEAK)
    peakposition(τ, ♠) + positionshift(ξ, noverlap)
end

positionshift(dir::Direction)  = dir ≡ goright          ? 1f0 : -1f0
positionshift(ξ::OligoConfigs) = direction(ξ) ≡ goright ? 1f0 : -1f0
function positionshift(ξ::OligoConfigs, novr::T)::T where {T <: Integer}
    if direction(ξ) ≡ goright
        T(oligosize(ξ)-novr)
     else
        T(novr-oligosize(ξ))
     end
end

function positionshift(dir::Direction, osize::Integer, pk1::T, pk2::T
                      ) where {T <: PEAK_TYPES}
    (dir ≡ goright ? osize - overlapcount(osize, pk1, pk2) :
                     overlapcount(osize, pk2, pk1) - osize)
end

"""
Estimate the position using a loss factor
"""
estimatedposition(τ::Float32, x1::Float32, x2::Float32) = (1f0-τ)*x1 + τ*x2
function estimatedposition(ξ::OligoConfigs, τ::Float32,
                           data::Experiment, peaks::AVECTPEAK)
    if length(peaks) == 0
        0f0
    elseif length(peaks) == 1 || τ >= 1f0
        peakposition(data, peaks[end])
    else
        dir   = direction(ξ)
        osize = oligosize(ξ)
        val   = peakposition(data, peaks[1])
        for i = 2:length(peaks)
            ovr = positionshift(dir, osize, peaks[i-1], peaks[i])
            val = estimatedposition(τ, val+ovr, peakposition(data, peaks[i]))
        end
        val
    end
end

function pathpositions(ξ::OligoConfigs, τ::Float32,
                       data::Experiment, peaks::AVECTPEAK)
    if length(peaks) == 0
        Float32[]
    elseif length(peaks) == 1 || τ >= 1f0
        Float32[peakposition(data, peaks[end])]
    else
        dir     = direction(ξ)
        osize   = oligosize(ξ)
        vals    = zeros(Float32, length(peaks))
        vals[1] = peakposition(data, peaks[1])
        for i = 2:length(peaks)
            ovr     = positionshift(dir, osize, peaks[i-1], peaks[i])
            vals[i] = estimatedposition(τ, vals[i-1]+ovr, peakposition(data, peaks[i]))
        end
        vals
    end
end

function estimatedposition(ξ    ::OligoConfigs,
                           τ    ::Float32,
                           data ::Experiment,
                           oligo::Integer,
                           pos  ::Float32,
                           ♠    ::APEAK)
    val = peakposition(data, ♠)
    if τ >= 1f0
        val
    else
        ovr = positionshift(direction(ξ), oligosize(ξ), oligo, ♠[1])
        estimatedposition(τ, pos+ovr, val)
    end
end

function normedresidual(ξ::OligoConfigs, data::Experiment, ♠::AVECTPEAK)
    if isempty(♠)
        return typemax(Float32)
    end

    x     = peakposition(data, ♠)
    y     = baseposition(ξ,  ♠)
    coeff = (cov(x, y)/cov(x))
    mx    = coeff * mean(x) -  mean(y)
    mean((coeff .* x  .- y .- mx).^2)
end

function residual(ξ::OligoConfigs, data::Experiment, ♠::AVECTPEAK)
    if isempty(♠)
        return typemax(Float32)
    end
    x = peakposition(data, ♠)
    y = baseposition(ξ,  ♠)
    mean((x .- y .- (mean(x)-mean(y))).^2)
end

function symmetry(ξ::OligoConfigs, data::Experiment, ♠::AVECTPEAK)
    if isempty(♠)
        return typemax(Float32)
    end

    x  = peakposition(data, ♠)
    y  = baseposition(ξ,  ♠)
    1. - cov(x, y)/cov(x)
end

_norm1(a::AbstractArray, b) = @. abs(a-b)

"""
For a given list of peaks, swaps between used and unused peaks in order to reduce
the residuals.

This is needed because the search algorithm always prefers to use peaks further
to the left.
"""
function residualreduction!(ξ::OligoConfigs,
                            ♠::AbstractVector{Tuple{T, Int}},
                            ι::Int = 1) where T <: Integer
    data      = experiment(ξ)
    basepos   = baseposition(ξ, ♠)
    zero      = mean(peakposition(data, ♠[i]) - basepos[i] for i = eachindex(♠))
    sz        = oligosize(ξ)
    basepos .+= zero
    for oligo = T(1):T(length(data))
        inds = if T ≡ BatchedOligo
            rco  = rc(sz, oligo)
            rco < oligo ? Int[] : Int[i[1] for i = enumerate(♠) if i[2][1] ∈ (oligo, rco)]
        else
            Int[i[1] for i = enumerate(♠) if i[2][1] == oligo]
        end

        if (length(inds) == 0
            || length(inds) == length(peakposition(data, oligo))
            || all(i < ι for i = inds))
            continue
        end

        positions = peakposition(data, oligo)

        current   = Int[♠[i][2] for i = inds]
        available = Int[i for i = 1:length(positions) if i ∉ current]
        if length(available) == 0
            continue
        end

        if ι != 1
            good    = inds .> ι
            current = current[good]
            inds    = inds[good]
        end

        bps       = basepos[inds]

        curdist   = _norm1(bps, positions[current])
        potdist   = _norm1(bps, reshape(positions[available], 1, :))

        ipot      = findmin(potdist)[2]
        ncur      = length(current)
        while potdist[ipot] < curdist[ipot[1]]
            tmp                     = ♠[inds[ipot[1]]][2]
            ♠[inds[ipot[1]]]        = (♠[inds[ipot[1]]][1],  available[ipot[2]])
            available[ipot[2]]      = tmp

            curdist[ipot[1]]        = potdist[ipot]
            potdist[1:end, ipot[2]] = _norm1(bps, positions[available[ipot[2]]])

            ipot                    = findmin(potdist)[2]
        end
    end
    ♠
end

"""
Return an array of int arrays, grouping peak ids per oligo id.

The array index is the oligo id.
"""
function groupedpeaks(♠::AVECTPEAK)
    nintra = zeros(Int, maximum((x[1] for x = ♠)))
    for i = ♠
        nintra[i[1]] +=1
    end

    intra = zeros.(Int, nintra)
    fill!(nintra, 1)
    for (i, j) = enumerate(♠)
        intra[j[1]][nintra[j[1]]] = i
        nintra[j[1]]             += 1
    end

    intra
end

"Information on the sequence"
struct SequenceStats
    "per oligo, the number of peaks used"
    used           :: Vector{Int}
    "per oligo, the number of peaks that were not found"
    undetected     :: Vector{Int}
    "per oligo, the number of peaks that are not in the sequence"
    falsepositives :: Vector{Int}
    "the residual on positions in general"
    residuals²     :: Vector{Float32}
end

function SequenceStats(Δ   ::Integer,
                       odir::Direction,
                       τ   ::Experiment,
                       ♠   ::AbstractVector{Tuple{T, Int}}) where T <: Integer
    @assert odir ≡ goright
    und  = zeros(Int,     length(τ))
    ρ    = [1 .<< (0:2:2Δ-1) ...]
    δ    = zeros(Float32, length(τ))
    Σ    = zeros(Int, length(τ))
    bp   = convert(Array{Float32,1}, baseposition(Δ, odir, ♠))
    for i = 1:length(♠)-1
        ovr = overlapcount(Δ, odir, ♠[i][1], ♠[i+1][1])
        if ovr < Δ-1
            seq = vcat(digits(♠[i][1]-1;   base = 4,  pad = Δ)[2:end],
                       digits(♠[i+1][1]-1; base = 4,  pad = Δ)[Δ-ovr:end])
            for j = 1:length(seq)-Δ
                und[sum(ρ .* @view seq[j:j+Δ-1])+1] += 1
            end
        end
        δ[♠[i][1]] += (peakposition(τ, ♠[i]) - bp[i])^2
        Σ[♠[i][1]] += 1
    end
    δ[♠[end][1]] += (peakposition(τ, ♠[end]) - bp[end])^2
    Σ[♠[end][1]] += 1

    SequenceStats(Σ, und, _falsepositives(T, Δ, τ, Σ), δ./length(♠))
end

function SequenceStats(Δ   ::Integer,
                       odir::Direction,
                       τ   ::Experiment,
                       ♠   ::AbstractVector{Tuple{T, MInt}}) where T <: Integer
    @assert odir ≡ goright
    δ   = zeros(Float32, length(τ))
    Σ   = zeros(Int,     length(τ))
    und = zeros(Int,     length(τ))
    for (i, j) = enumerate(♠)
        if j[2] ≡ missing
            und[j[1]] += 1
        else
            δ[j[1]]   += (peakposition(τ, j) - i+1)^2
            Σ[j[1]]   += 1
        end
    end
        
    SequenceStats(Σ, und, _falsepositives(T, Δ, τ, Σ), δ./length(♠))
end

function SequenceStats(ξ::OligoConfigs, τ::Experiment, ♠::AbstractVector)
    SequenceStats(oligosize(ξ), direction(ξ), τ, ♠)
end
function SequenceStats(τ::Experiment, ♠::AbstractVector)
    SequenceStats(oligosize(τ), goright, τ, ♠)
end

function _falsepositives(::Type{Int}, Δ::Integer, τ::Experiment, Σ::AbstractVector{<:Integer})
    fpos = lengths(τ)
    for (i, j) = enumerate(Σ)
        fpos[i] -= j
    end
    fpos
end

function _falsepositives(::Type{BatchedOligo}, Δ::Integer, τ::Experiment, Σ::AbstractVector{<:Integer})
    fpos = lengths(τ)
    for (i, j) = enumerate(Σ)
        fpos[i]        -= j
        fpos[rc(Δ, i)] -= j
    end

    if Δ % 2 == 0
        for i = 1:length(fpos)
            if rc(Δ, i) != i
                fpos[i] *= .5
            end
        end
    end
end

function printsummary(io::IO, σ::SequenceStats)
    size   = oligosize(σ.used)
    cnt    = length(σ.used)
    exists = (σ.used .+ σ.undetected .+ σ.falsepositives)
    @printf(io, "Oligos / Bindings:                    %4d %6d",
            sum(exists .> 0), sum(exists))
    println(io, "")
    @printf(io, "Used / Undetected / False Positives:  %4d %6d %d",
            sum(σ.used), sum(σ.undetected), sum(σ.falsepositives))
    println(io, "")
end
printsummary(io::IO, vals...)  = printsummary(io, SequenceStats(vals...))
printsummary(σ::SequenceStats) = printsummary(STDOUT, σ)
printsummary(vals...)          = printsummary(STDOUT, SequenceStats(vals...))

function printtable(io::IO, σ::SequenceStats, width = 150)
    size = oligosize(σ.used)
    cnt  = length(σ.used)
    len  = div(width, 13)
    miss = (σ.used .+ σ.undetected .+ σ.falsepositives) > 0

    println(io, "Cell content:  Used / Undetected / False Positives")
    println(io, "               Residual²")

    oname(i) = begin
        name  = oligoname(size, i)
        if miss[i]
            @printf io "%12s|" name
        elseif σ.used[i] == 0
            printstyled(io, @sprintf("%12s|", name); color = :light_red)
        else
            name  = σ.used[i] == 0 ? name : uppercase(name)
            blank = " "^div(12-length(name),2)
            @printf io "%12s|" blank*name*blank
        end
    end

    stats(i)  = begin
        if miss[i]
            print(io, " "^12*"|")
        else
            txt = @sprintf "%3d %3d %3d |" σ.used[i] σ.undetected[i] σ.falsepositives[i]
            if σ.used[i] == 0
                printstyled(io, txt; color = :light_red)
            else
                print(io, txt)
            end
        end
    end
    params(i)  = begin
        if miss[i]
            print(io, " "^12*"|")
        else
            txt = @sprintf "%12.2f|"  σ.residuals²[i]
            if σ.used[i] == 0
                printstyled(io, txt; color = :light_red)
            else
                print(io, txt)
            end
        end
    end

    for s = 1:len:cnt
        rng = s:min(s+len-1, cnt)
        foreach(oname,  rng)
        println("")
        foreach(stats,  rng)
        println("")
        foreach(params, rng)
        println("")
        println(io, ("-"^12*"|")^(rng.stop-rng.start+1))
    end
end

import Base: print
function Base.print(io::IO, σ::SequenceStats, width = 150)
    printsummary(io, σ)
    println(io, "")
    printtable(io, σ, width)
end
