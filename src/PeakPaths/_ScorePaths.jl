abstract type ScorePaths <: DepthPaths    end

"""$(_DEPTHDOC("with the lowest sum residual"))
"""
@paths struct DeltaScorePath <: ScorePaths end

"""$(_DEPTHDOC("with the lowest L1 residual"))
"""
@paths struct L1ScorePath <: ScorePaths end

"""$(_DEPTHDOC("with the lowest L2 residual"))
"""
@paths struct L2ScorePath <: ScorePaths end

"""$(_DEPTHDOC("with the lowest L2 residual + undetected count"))
"""
@paths struct L²uScorePath <: ScorePaths
    undetected::Float32 = Inf32
end


"""$(_DEPTHDOC("with the lowest L2 residual")) $__BATCHDOC"""
@paths struct BatchL2ScorePath <: ScorePaths end

"""$(_DEPTHDOC("with the lowest L2 residual + undetected count")) $__BATCHDOC"""
@paths struct BatchL²uScorePath <: ScorePaths
    undetected::Float32 = Inf32
end

_peakcost(::DeltaScorePath, x::Float32)        = x
_peakcost(::L1ScorePath,    x::Float32)        = abs(x)
_peakcost(::L2ScorePath,    x::Float32)        = x^2
_undetectedcost(::Paths, ::Float32, ::Float32) = 0.f0
_pathcost(::ScorePaths)                        = (0, 0, Inf32, Inf32)
function _pathcost(ξ::ScorePaths, cur::AbstractVector{<:APEAK}, best::Tuple{Int, Float32})
    (-best[1],
     length(cur) > 1 ? -overlapcount(ξ, cur[end-1], cur[end]) : 0,
     best[2],
     peakposition(experiment(ξ), cur[end]))
end

for cls = (:L²uScorePath, :BatchL²uScorePath)
    @eval begin
        _pathcost(::$cls)             = (0, Inf32, Inf32)
        _peakcost(::$cls, x::Float32) = x^2
        function _pathcost(ξ::$cls, cur::AbstractVector{<:APEAK}, best::Tuple{Int, Float32})
            (-best[1], best[2], peakposition(experiment(ξ), cur[end]))
        end
        _undetectedcost(ξ::$cls, ∑o::Float32, ∂o::Float32) = undetected(ξ)*(∑o-∂o)
        _peekminoverlap(ξ::$cls)                           = minoverlap(ξ)
    end
end

function _pathcost(ξ::ScorePaths, path::PeekPaths, cur::AVECTPEAK)
    best:: Tuple{Int, Float32} = (0, Inf32)
    depth   = max(length(cur)-searchdepth(path)+1, 1)
    osize   = oligosize(ξ)
    data    = experiment(path)
    isright = direction(ξ) ≡ goright
    ini     = currentbaseposition(ξ, @view cur[1:depth])
    @inbounds for k = path
        npeaks = length(pathedges(k))
        if isendpoint(k) && npeaks >= best[1]
            peaks           = pathpeaks(k)
            delta ::Float32 = peakposition(data, peaks[depth]) - ini
            sumovr::Float32 = 0f0

            if isright
                for i = depth+1:length(peaks)
                    sumovr += osize - overlapcount(osize, peaks[i-1], peaks[i])
                    delta  += _peakcost(ξ, peakposition(data, peaks[i]) - ini -sumovr)
                end
            else
                for i = depth+1:length(peaks)
                    sumovr += osize - overlapcount(osize, peaks[i], peaks[i-1])
                    delta  += _peakcost(ξ, peakposition(data, peaks[i]) - ini + sumovr)
                end
            end

            dovr::Float32  = length(peaks)-depth+1
            delta          = delta/dovr + _undetectedcost(ξ, sumovr, dovr)
            if npeaks > best[1] || delta < best[2]
                best = npeaks, delta
            end
        end
    end

    _pathcost(ξ, cur, best)
end
