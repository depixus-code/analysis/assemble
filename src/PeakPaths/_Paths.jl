for i = fieldnames(OligoNode)
    @eval import ..Oligo: $i
end

abstract type Paths       <: OligoConfigs  end
abstract type FullPaths   <: Paths         end
abstract type DepthPaths  <: Paths         end
abstract type PeekPaths   <: DepthPaths    end
abstract type PathsStates                  end

function __pathopts(tpe::Type{<:Integer}, strict :: Bool = false)
    pkt = Tuple{tpe, Int}
    vpk = strict ? Vector{pkt} : AbstractVector{<:APEAK}
    :(seachwindow    :: $(strict ? Float32 : Real)    = 10f0,
      minoverlap     :: $(strict ? Int     : Integer) = 1,
      oligosize      :: $(strict ? Int     : Integer) = 3,
      direction      :: Direction                     = goright,
      positionloss   :: $(strict ? Float32 : Real)    = .33f0,
      searchdepth    :: $(strict ? Int     : Integer) = 4,
      experiment     :: Experiment                    = Vector{Float32}[],
      startpeaks     :: $vpk                          = $pkt[],
      stoppeaks      :: $vpk                          = $pkt[],
     ).args
end

for expr = __pathopts(Int)
    @eval function $(expr.args[1].args[1]) end
end

macro paths(τ)
    name, parent = Accessor.extractinfo(τ, :(=))[1:2]
    pk           = startswith(string(name), "Batch") ? BatchedOligo : Int

    function _popts(strict)
        isd  = eval(parent) <: DepthPaths
        tmp  = __pathopts(pk, strict)
        deepcopy([tmp[1:(isd ? 6 : 5)]; τ.args[3].args[2:2:end]; tmp[end-2:end]])
    end

    pkt   = Tuple{pk, Int}
    vpk   = Vector{pkt}

    args  = map(_popts(true)) do i deepcopy(i.args[1]) end
    fcns  = [i.args[1] for i = args]
    opts  = [begin i.head = :kw; i end for i = _popts(false)[1:end-3]]

    code  = quote
        $name($(opts...)) = $name($(fcns[1:end-3]...), $vpk[], $pkt[], $pkt[])
        Base.eltype(ξ::Type{$name}) = PathsState{$pkt}
        $((pk ≡ Int ? () :
           (quote
               oligotype(::Type{$name}) = $pk
               peaktype(::Type{$name})  = $pkt
           end,))...)
    end

    quote
        @oligoconfigs $__module__ struct $name <: $(esc(parent))
            $(args[length(fieldnames(OligoNode))+1:end]...)
        end

        $(esc(code))
    end
end

@eval searchdepth(::Paths) = $(__pathopts(Int, true)[6].args[2])
searchdepth(ξ::DepthPaths) = ξ.searchdepth

const __DOC = """
Iterate over all paths and every step of the path starting at a given position.

The iteration goes to the end of a path then explores paths bifurcating at the
last node, the one before that and so on.

Nodes are sorted by the number of overlaping bases and the algebraic distance
to the current position.
"""

const __BATCHDOC = """

Oligos are batched with their reverse complement.
"""

"""$__DOC"""
@paths struct Path <: Paths end

"""$__DOC

Iterate over all *complete* paths starting at a given position.
"""
@paths struct FullPath <: FullPaths end


"""$__DOC

Iterate over all *complete* paths starting at a given position. $__BATCHDOC"""
@paths struct BatchFullPath <: FullPaths end

"At a given node position, the κ of the iterator"
struct NodeState{T <: APEAK}
    "List of peaks that can be accessed from the last peak"
    peaks::Vector{T}

    "Peak currently being iterated over"
    index::Int

    "Experimental position of last peak (not the current list of peaks)"
    pos  ::Float32
end
NodeState(peaks::AbstractVector{T}, index, pos) where T = NodeState{T}(peaks, index, pos)

"The iterator κ for basic paths"
@accessors struct PathsState{T <: APEAK} <: PathsStates
    """
    The list of peaks up to and including the current node

    The list does not include the very last peak during an iteration.
    The latter can be accessed through `pathtip`.
    """
    pathpeaks ::Vector{T}

    """
    Potential edges to the current list of peaks.

    The edges are for nodes `pathpeaks[end-length(pathedges)+1:end]`, in that order.
    """
    pathedges ::Vector{NodeState{T}}

    "whether the current path can be extended beyond the current point"
    isendpoint::Bool
end
PathsState(peaks::AbstractVector{T}, edges, ise) where T = PathsState{T}(peaks, edges, ise)
PathsState(peaks::AbstractVector{T}) where T = PathsState{T}(peaks, NodeState{T}[], false)

for fcn = (:overlapcount, :missingcount, :basecount, :baseposition, :sequence)
    @eval begin
        import ..Oligo: $fcn

        function $fcn(ξ::OligoConfigs, κ::PathsStates)
            $fcn(oligosize(ξ), direction(ξ), pathpeaks(κ))
        end
    end
end

function pathpositions(ξ::Paths, κ::AVECTPEAK)
    pathpositions(ξ, positionloss(ξ), experiment(ξ), κ)
end

for fcn = (:pathbases, :pathoverlaps, :pathpositions)
    @eval begin
        function $fcn(ξ::Paths, κ::PathsStates)
            $fcn(ξ, pathpeaks(κ))
        end
    end
end

pathpeaks(::Paths, κ::PathsStates) = pathpeaks(κ)

function currentbaseposition(ξ::Paths, κ::AVECTPEAK) :: Float32
    osize           = oligosize(ξ)
    data            = experiment(ξ)
    bpos  ::Float32 = 0f0
    delta ::Float32 = peakposition(data, κ[1])
    if direction(ξ) ≡ goright
        @inbounds for i = 2:length(κ)
            bpos += osize-overlapcount(osize, κ[i-1], κ[i])
            delta+= peakposition(data, κ[i])-bpos
        end
    else
        @inbounds for i = 2:length(κ)
            bpos -= osize - overlapcount(osize, κ[i], κ[i-1])
            delta+= peakposition(data, κ[i])-bpos
        end
    end
    delta/length(κ)+bpos
end

function pathzeros(ξ::Paths, κ::AVECTPEAK) :: Vector{Float32}
    osize           = oligosize(ξ)
    data            = experiment(ξ)
    bpos  ::Float32 = 0f0
    rho   ::Float32 = 0f0
    out             = zeros(Float32, length(κ))
    out[1]          = peakposition(data, κ[1])
    if direction(ξ) ≡ goright
        @inbounds for i = 2:length(κ)
            rho    = i/(i+1)
            bpos  += osize-overlapcount(osize, κ[i-1], κ[i])
            out[i] = rho*out[i-1] + (1. - rho) *(peakposition(data, κ[i])-bpos)
        end
    else
        @inbounds for i = 2:length(κ)
            rho    = i/(i+1)
            bpos  += osize - overlapcount(osize, κ[i], κ[i-1])
            out[i] = rho*out[i-1] + (1. - rho) *(peakposition(data, κ[i])+bpos)
        end
    end
    out
end
pathzeros(ξ::Paths, κ::PathsStates) :: Vector{Float32} = pathzeros(ξ, pathpeaks(κ))

pathtipindex(τ::NodeState)   = τ.index
pathtip(τ::NodeState)        = τ.peaks[τ.index]
pathtip(τ::PathsStates)      = pathtip(pathedges(τ)[end])
pathtipedges(τ::NodeState)   = τ.peaks
pathtipedges(τ::PathsStates) = pathtipedges(pathedges(τ)[end])
next(τ::NodeState)           = NodeState(τ.peaks, τ.index+1, τ.pos)
done(τ::NodeState)           = length(τ.peaks) <= τ.index

estimatedposition(s::NodeState) = s.pos

"""
Find the peak position for the currrent node.
"""
function estimatedposition(ξ::Paths, κ::PathsStates) :: Float32
    data  = experiment(ξ)
    peaks = pathpeaks(κ)
    τ     = positionloss(ξ)
    if length(peaks) == 1 || τ >= 1f0
        return peakposition(data, peaks[end])
    end

    if length(pathedges(κ)) == 0
        estimatedposition(ξ, τ, data, peaks)
    else
        val = estimatedposition(pathedges(κ)[end])
        estimatedposition(ξ, τ, data, peaks[end-1][1], val, peaks[end])
    end
end

function estimatedposition(ξ::Paths, peaks::AVECTPEAK)
    estimatedposition(ξ, positionloss(ξ), experiment(ξ), peaks)
end

function _pk_indexes(ξ::Paths, κ::PathsState{T}) where T
    peaks = pathpeaks(κ)
    @peakindexes T ξ peaks peaks[end] estimatedposition(ξ, κ)
end

"""
Check whether there are any more possible paths out of the current node
"""
hasedges(ξ::Paths, κ::PathsStates) = haspathpeaks(_pk_indexes(ξ, κ), ξ)

growedges_filterneighbours!(::Paths, ::AbstractVector{<:APEAK}) = nothing
growedges_sortall!(::Paths, ::PathsStates)                      = nothing
function growedges_sort!(ξ::Paths, good::AbstractVector{<:APEAK})
    sort!(good, by = x->peakposition(experiment(ξ), x))
end
    
"""
Move forward in the graph.

New edges are sorted by maximum overlap & closest position to the current.
"""
function growedges!(ξ::T, κ::PathsState{K}) where {T <: Paths, K <: APEAK}
    inds  = _pk_indexes(ξ, κ)
    itms  = @pathpeaks K ξ inds growedges_filterneighbours! growedges_sort!
    added = vcat(itms...)
    push!(pathpeaks(κ), added[1])
    push!(pathedges(κ), NodeState(added, 1, inds.pos))
    growedges_sortall!(ξ, κ)
end

"""
Move backward in the graph
"""
function trimedges!(ξ::Paths, κ::PathsStates)
    edges   = pathedges(κ)
    current = pathpeaks(κ)
    for ind = length(edges):-1:1
        if !done(edges[ind])
            sz         = length(current)-length(edges)+ind-1
            resize!(edges, ind)
            edges[end] = next(edges[end])
            push!(resize!(current, sz), pathtip(edges[end]))
            return
        end
    end
    @assert false
end

function _findnext!(ξ::Paths, κ::PathsState{T}) where T
    peaks = pathpeaks(κ)
    edges = pathedges(κ)
    if isendpoint(κ)
        if isempty(edges)
            return PathsState{T}(peaks, NodeState{T}[NodeState{T}(T[], 0, 0f0)], true)
        end
        trimedges!(ξ, κ)
    else
        growedges!(ξ, κ)
    end

    PathsState{T}(peaks, edges, length(edges) > 0 && !hasedges(ξ, κ))
end

function _start(ξ::T) where {T <: Paths}
    if length(startpeaks(ξ)) == 0
        pks = initialpeaks(peaktype(T), direction(ξ), experiment(ξ))[1:1]
    else
        pks = copy(startpeaks(ξ))
    end

    s = PathsState(pks)
    PathsState(pks, pathedges(s), !hasedges(ξ, s))
end

function done(s::PathsStates)
    edges = pathedges(s)
    (isendpoint(s) && length(edges) > 0 && all(done(i) for i = @view edges[end:-1:1]))
end

Base.IteratorSize(::Paths)                 = Base.SizeUnknown()

function Base.iterate(ξ::Paths, κ::PathsStates = _start(ξ))
    if done(κ)
        return nothing
    end

    s = _findnext!(ξ, κ)
    s, s
end

function Base.iterate(ξ::FullPaths, κ::PathsStates = _start(ξ))
    if done(κ)
        return nothing
    end

    s = _findnext!(ξ, κ)
    while !(isendpoint(s) || done(s))
        s = _findnext!(ξ, s)
    end
    (s, s)
end
