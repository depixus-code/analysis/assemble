using  ..Oligo:    emptyexperiment, nbindings
mutable struct AndreasInSiScore{T <: Paths, K <:APEAK}
    config          ::T
    remaining       ::Bool
    falsepositives  ::Float32
    nbindings       ::Int
    peaks           ::Vector{K}
    score           ::Float32

end

function AndreasInSiScore(ξ::T, rem::Bool, fp::Float32) where T <: Paths
    K = peaktype(T)
    AndreasInSiScore{T, K}(ξ, rem, fp, nbindings(oligotype(T), experiment(ξ)),
                           K[], Inf32)
end

score(ξ::AndreasInSiScore)  = ξ.score
peaks(ξ::AndreasInSiScore)  = ξ.peaks

function better!(ξ::AndreasInSiScore, ♠::AVECTPEAK)
    orig = ♠
    if ξ.remaining
        ind = 1
        for i = 1:min(length(ξ.peaks), length(♠))
            if ξ.peaks[i] != [i]
                ind = min(1, i-1)
                break
            end
        end

        if direction(ξ.config) == goleft 
            ξright = clone(ξ.config; direction = goright)
            ♠      = ♠[end:-1:1]
        else
            ξright = ξ.config
            ♠      = copy(♠)
        end

        residualreduction!(ξright, ♠, ind)
        ♠ = insertremaining(ξright, ♠, ind)
        if direction(ξ.config) == goleft 
            ♠ = ♠[end:-1:1]
        end
    end

    if ξ.falsepositives <= 0f0
        out  = -length(♠)
    else
        ovr  = length(♠) < 2 ?
                0f0 :
                sum(overlapcount(ξ.config, ♠[i], ♠[i+1]) for i = 1:length(♠)-1)

        out  = ((length(♠)-1)*(oligosize(ξ.config)-1)-ovr 
                + (ξ.nbindings-length(♠))*ξ.falsepositives)
    end

    if out < ξ.score
        ξ.score = out
        ξ.peaks = ♠ ≡ orig ? copy(♠) : ♠
        true
    else
        false
    end
end

function Base.max(ξ             ::T,
                  depth         ::Int     = 4,
                  falsepositives::Float32 = 0f0,
                  remaining     ::Bool    = true;
                  opts ...) :: Union{Nothing, PathsState} where {T <: Paths}
    ξ = clone(ξ; opts...)
    emptyexperiment(experiment(ξ)) && return nothing

    best    = nothing
    scoring = AndreasInSiScore(ξ, remaining, falsepositives)
    for state = ξ
        if isendpoint(state)
            better!(scoring, pathpeaks(state)) && (best = deepcopy(state))
            if length(pathedges(state)) > depth
                arr = pathedges(state)
                arr[1:depth] .= arr[end-depth+1:end]
                resize!(arr, depth)
            end
        end

    end
    best
end


"""
Find the first big sequence, exploring the graph up to a given depth.

The algorithm finds full paths, iterating only on paths at depth greater than
N-depth where N is the size of the last 

The sequence of peaks returned is the longest one.
"""
function andreasinsi(ξ             ::T,
                     depth         ::Int,
                     falsepositives::Float32,
                     remaining     ::Bool,
                     spoonerism    ::Bool) where {T <: Paths}
    state = max(ξ, depth, falsepositives, remaining)
    if state ≡ nothing
        return peaktype(T)[]
    end
    best = pathpeaks(state)
    if direction(ξ) ≡ goleft 
        ξright = clone(ξ; direction = goright)
        best   = best[end:-1:1]
    else
        ξright = ξ
    end

    residualreduction!(ξright, best)
    if remaining
        best = insertremaining(ξright, best)
    end
    if spoonerism
        best = bestspoonerism(ξright, best)
    end
    direction(ξ) ≡ goleft ? best[end:-1:1] : best
end

function andreasinsi(ξ::T; 
                     depth          ::Int                                     = 4,
                     falsepositives ::Float32                                 = 1f0,
                     nodes          ::Union{Array{<:APEAK,1}, APEAK, Nothing} = nothing,
                     bothsides      ::Integer                                 = 1,
                     remaining      ::Bool                                    = false,
                     spoonerism     ::Bool                                    = false,
                     opts...) where {T <: Paths}
    ξ = clone(ξ; opts...)
    if all(length(i) == 0 for i = experiment(ξ))
        return peaktype(T)[]
    end

    if nodes isa APEAK
        nodes = peaktype(T)[nodes]
    elseif nodes ≡ nothing
        nodes = initialpeaks(peaktype(T), direction(ξ), experiment(ξ))
    end
    
    best = peaktype(T)[]
    for node in nodes
        ξ = clone(ξ; startpeaks = peaktype(T)[node])
        out = andreasinsi(ξ, depth, falsepositives, remaining, false)
        if bothsides > 0
            tmp = clone(ξ; direction = ~direction(ξ), startpeaks = out[end:-1:bothsides])
            out = andreasinsi(tmp, depth, falsepositives, false, false)[end:-1:1]
        end

        if spoonerism
            out = bestspoonerism(ξ, out)
        end

        if length(best) < length(out)
            best = out
        end
    end
    best
end
