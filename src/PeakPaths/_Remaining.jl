using ..Oligo: OligoConfigs, Neighbour, residualreduction!, rc

"""
Insert missing peaks if it doesn't change the overlap
"""
function insertremaining(ξ::Paths, ♠::AbstractVector{T}, gradefcn, startpos::Int = 1
                        ) where {T <: Tuple{<:Integer, Int}}
    data    = experiment(ξ)
    sz      = oligosize(ξ)
    ovr     = oligosize(ξ)-1
    cnfanti = OligoNode(searchwindow(ξ), ovr, oligosize(ξ), ~direction(ξ))
    K       = oligotype(T)

    for oligo = K(1):K(length(data)), peak = 1:length(peakposition(data, oligo))
        if (oligo, peak) ∈ ♠ || (K ≡ BatchedOligo && (rc(sz, oligo), peak) ∈ ♠)
            continue
        end

        best = (gradefcn(ξ, T[]), T[])
        for itr = Neighbour(cnfanti, data, ovr, (oligo, peak), NaN32), prev = itr[2]
            if (prev == peak
                && (itr[1] == oligo || (K ≡ BatchedOligo && rc(sz, itr[1]) == oligo)))
                continue
            end

            for (i, j) = enumerate(@view ♠[startpos:end])
                i += startpos-1
                if j[1] == itr[1] && j[2] == prev
                    tmp = T[]
                    for state = _peekpath(ξ, T[view(♠, 1:i); (oligo, peak)],
                                          ♠[i+2:end], ovr)
                        cur = pathpeaks(state)
                        if (length(best) < length(cur) 
                            && (i == length(♠) || cur[end] == ♠[i+1]))
                            tmp = copy(cur)
                        end
                    end

                    if !isempty(tmp)
                        grade = gradefcn(ξ, tmp)
                        if grade < best[1]
                            best = grade, [tmp; @view ♠[i+2:end]]
                        end
                    end
                    break
                end
            end

        end

        if !isempty(best[2])
            ♠ = best[2]
        end
    end
    ♠
end

function insertremaining(ξ::Paths, ♠::AVECTPEAK, startpos ::Int = 1)
    insertremaining(ξ, ♠, (x, y) -> (-length(y), residual(x, y)))
end
