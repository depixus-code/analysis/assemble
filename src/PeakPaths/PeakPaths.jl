module PeakPaths
using ..Accessor

using ..Oligo:  Experiment, OligoNode, OligoConfigs, Direction, goleft, goright,
    peakposition, estimatedposition, overlaps, @oligoconfigs,
    APEAK, AVECTPEAK, initialpeaks, BatchedOligo, update!, haspathpeaks,
    @peakindexes, @pathpeaks, peaktypecode

import ..Oligo: estimatedposition, experiment, searchdepth, pathpeaks, peaktype,
    pathpositions, pathoverlaps, pathscores, pathzeros, pathbases,
    positionloss, estimatedposition, oligotype

include("_Paths.jl")
include("_DepthPaths.jl")
include("_ScorePaths.jl")
include("_Old.jl")
include("_Spoonerisms.jl")
include("_Remaining.jl")
include("_AndreasInSilico.jl")
end
