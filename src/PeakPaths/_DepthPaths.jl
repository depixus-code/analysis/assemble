const __PEEKDOC = """$__DOC

Paths extending for further than `depth` nodes are truncated.
"""

"""$__PEEKDOC"""
@paths struct PeekPath <: PeekPaths end

"""$__PEEKDOC $__BATCHDOC"""
@paths struct BatchPeekPath <: PeekPaths end


_DEPTHDOC(info) = """$__DOC

Nodes are sorted peeking at nodes at most `depth` nodes away from the current
one. The longest truncated path $info is explored first.
"""

"""$(_DEPTHDOC("ending at the shortest distance of the current position"))
"""
@paths struct InDepthPath <: DepthPaths end

_pathcost(itr::InDepthPath) = (0, 0, Inf32)
function _pathcost(itr::InDepthPath, path::PeekPaths, cur::AVECTPEAK)
    maxi = 0
    for k = path
        maxi = max(length(pathedges(k)), maxi)
        if maxi == searchdepth(itr)
            break
        end
    end
    (-maxi,
     -overlapcount(oligosize(itr), cur[end-1], cur[end]),
     peakposition(experiment(itr), cur[end]))
end

_peekminoverlap(ξ::DepthPaths) = oligosize(ξ)-1

@generated function _peekpath(ξ ::Paths,
                              ♠1::AVECTPEAK,
                              ♠2::AVECTPEAK = stoppeaks(ξ),
                              Δ ::Int       = _peekminoverlap(ξ))
    cls  = oligotype(ξ) ≡ BatchedOligo ? BatchPeekPath : PeekPath
    args = ((ξ <: DepthPaths ? (:(searchdepth(ξ)),) : ())...,
            :(experiment(ξ)), :♠1, :♠2)
    quote
        $cls(searchwindow(ξ), Δ, oligosize(ξ), direction(ξ), positionloss(ξ), $(args...))
    end
end

growedges_sort!(::DepthPaths, ::AbstractVector{<:APEAK}) = nothing

"""
Permutes edges

Sorted looking `depth` peaks further than the current sorting versus
the distance to the theoretical position.
"""
function growedges_sortall!(itr::DepthPaths, state::PathsStates)
    if searchdepth(itr) > 0
        peaks = pathtipedges(state)
        cur   = pathpeaks(state)
        path  = _peekpath(itr, cur)
        order = [(cur[end] = i; _pathcost(itr, path, cur)) for i = peaks]
        permute!(peaks, sortperm(order))
        cur[end] = peaks[1]
    end
end

growedges_sortall!(::PeekPaths, ::PathsStates) = nothing

function pathscores(ξ::DepthPaths, κ::AVECTPEAK)
    map(x->(val = κ[1:x]; _pathcost(ξ, _peekpath(ξ, val), val)), 1:length(κ))
end
pathscores(ξ::DepthPaths, κ::PathsStates) = pathscores(ξ, pathpeaks(κ))

function Base.iterate(ξ::PeekPaths, κ::PathsStates = _start(ξ))
    if done(κ)
        return nothing
    end

    state = _findnext!(ξ, κ)
    while !(isendpoint(state)
            || done(state)
            || length(pathedges(state)) >= searchdepth(ξ))
        state = _findnext!(ξ, state)
    end

    if !isendpoint(state)
        state = PathsState(pathpeaks(state), pathedges(state), true)
    end
    (state, state)
end
