using ..Accessor: clone
using ..Oligo:    hasoverlap

"""$__PEEKDOC

Furthermore, the peaks are not inverted.
"""
@paths struct SpoonerPath <: PeekPaths end

"""$__PEEKDOC

Furthermore, the peaks are not inverted. $__BATCHDOC"""
@paths struct BatchSpoonerPath <: PeekPaths end

function growedges_filterneighbours!(::SpoonerPath, good::AbstractVector{<:APEAK})
    if length(good) == 0
        return
    end
    j = 1
    for i = 2:length(good)
        if good[i][1] != good[j][1]
            j += 1
            if i != j
                good[j] = good[i]
            end
        end
    end
    resize!(good, j)
    good
end

"""
Finds positions where a spoonerism is possible
"""
function spoonerable(itr::DepthPaths, peaks::AVECTPEAK)
    @assert direction(itr) ≡ goright
    size   = oligosize(itr)
    depth  = searchdepth(itr)
    npeaks = length(peaks)
    ovrs   = [overlapcount(size, peaks[i][1], peaks[i+1][1]) for i = 1:length(peaks)-1]
    good   = Tuple{Int,Int}[]
    for i = 2:length(peaks)-1, j = min(i+depth, npeaks):-1:i+1
        if (peaks[i][1] == peaks[j][1]
            || !hasoverlap(size, ovrs[i-1], peaks[i-1][1], peaks[j][1]))
            continue
        end

        rng = min(i+depth,npeaks-1): -1: (length(good)==0 ? j : max(good[end][2],j))
        for k = rng, l = i:k-1
            if (peaks[k][1] == peaks[l][1]
                || !hasoverlap(size, ovrs[k], peaks[l][1], peaks[k+1][1]))
                continue
            end

            push!(good, (i-1, k+1))
            break
        end
    end
    good
end

"""
Iterates over paths starting as `first-1` and finishing as `last+1` providing
with all potential spoonerisms
"""
function spooners(itr::DepthPaths, peaks::AbstractVector{T}, ind1::Int, ind2::Int
                 ) where {T <: APEAK}
    @assert direction(itr) ≡ goright
    osize   = oligosize(itr)
    depth   = ind2-ind1
    oligo   = peaks[ind2][1]
    minovr  = overlapcount(osize, peaks[ind2-1][1], oligo)

    good    = Vector{T}[]
    pathtpe = oligotype(T) ≡ BatchedOligo ? BatchSpoonerPath : SpoonerPath
    path    = pathtpe(itr, positionloss(itr), depth, experiment(itr),
                      peaks[1:ind1], peaks[ind2:end])
    seqsize = depth + ind1 -1
    for i = path
        cur = pathpeaks(i)
        if length(cur) == seqsize && overlapcount(osize, cur[end][1], oligo) >= minovr
            push!(good, cur[ind1:end])
        end
    end
    good
end

for fcn = (:residual, :symmetry, :normedresidual)
    @eval begin
        import ..Oligo: $fcn
        function $fcn(itr::Paths, peaks::AVECTPEAK)
            $fcn(itr, experiment(itr), peaks)
        end

        function $fcn(itr     ::Paths,
                      cur     ::AbstractVector{T},
                      inds    ::Tuple{Int,Int},
                      spooners::Vector{Vector{T}}
                     ) :: Tuple{Vector{T}, Float32} where {T <: APEAK}
            if length(spooners) > 1
                delta  = $fcn(itr, view(cur, inds[1]:inds[2]-1))
                grades = [$fcn(itr, i)-delta for i = spooners]
                ind    = findmin(grades)[2]
                if grades[ind] < 0f0
                    return (spooners[ind], grades[ind])
                end
            end

            return (T[], typemax(Float32))
        end
    end
end

import StatsBase: corspearman
function corspearman(itr     ::Paths,
                     cur     ::AbstractVector{T},
                     inds    ::Tuple{Int,Int},
                     spooners::Vector{Vector{T}}
                    ) :: Tuple{Vector{T}, Float32} where {T <: APEAK}
    if length(spooners) > 1
        grades = [corspearman([peakposition(experiment(itr), k) for k = i],
                              1:length(i)) for i = spooners]
        ind    = indmax(grades)
        if any(i != j for (i, j) = Iterators.zip(view(cur, inds[1]:inds[2]-1), spooners[ind]))
            return (spooners[ind], grades[ind])
        end
    end

    return (T[], typemax(Float32))
end

"""
Find the best spoonerisms and change the array of peaks accordingly
"""
function bestspoonerism(itr::DepthPaths, peaks::AVECTPEAK, gradefcn)
    if direction(itr) == goleft
        # Spoonerism search should be symmetric.
        # The code is made simpler by imposing direction ≡ goright
        itr = clone(itr; direction = goright)
    end

    grades = Dict(i => gradefcn(itr, peaks, i,
                                spooners(itr, peaks, i[1], i[2]))
                  for i = spoonerable(itr, peaks))

    for i = keys(grades)
        if grades[i][2] == typemax(Float32)
            pop!(grades, i)
        end
    end

    while length(grades) > 0
        ind = ((0,0), typemax(Float32))

        # find the best spoonerism
        for i = keys(grades)
            if grades[i][end] < ind[end]
                ind = (i, grades[i][end])
            end
        end

        if ind[end] == typemax(Float32)
            break
        end

        # apply the spoonerism
        peaks[ind[1][1]+1:ind[1][2]-1] = @view grades[ind[1]][1][2:end]

        # remove spoonerisms impacting the same region as the one selected
        for i = collect(keys(grades))
            if (any(k >= ind[1][1]  && k <= ind[1][2] for k = i)
                || any(k >= i[1]  && k <= i[2] for k = ind[1]))
                pop!(grades, i)
            end
        end
    end
    peaks
end

bestspoonerism(itr::DepthPaths, peaks::AVECTPEAK) = bestspoonerism(itr, peaks, residual)
