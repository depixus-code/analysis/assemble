abstract type OldScorePaths     <: ScorePaths    end
@paths struct OldDeltaScorePath <: OldScorePaths end
@paths struct OldL2ScorePath    <: OldScorePaths end

function growedges_sort!(ξ::OldScorePaths, good::AVECTPEAK)
    sort!(good, by = x->peakposition(experiment(ξ), x))
end

_pathcost(::OldScorePaths) = (0, 0, Inf32)
_peakcost(::OldDeltaScorePath, x::Float32) = x
_peakcost(::OldL2ScorePath,    x::Float32) = x^2

function _pathcost(ξ::OldScorePaths, path::PeekPaths, cur::AVECTPEAK)
    best:: Tuple{Int, Float32} = (0, Inf32)
    depth   = max(length(cur)-searchdepth(path)+1, 1)
    osize   = oligosize(ξ)
    data    = experiment(path)
    isright = direction(ξ) ≡ goright
    ini     = currentbaseposition(ξ, @view cur[1:depth])
    @inbounds for k = path
        if isendpoint(k) && length(pathedges(k)) >= best[1]
            peaks           = pathpeaks(k)
            delta ::Float32 = peakposition(data, peaks[depth]) - ini
            sumovr::Float32 = 0f0

            if isright
                for i = depth+1:length(peaks)
                    sumovr += osize - overlapcount(osize, peaks[i-1], peaks[i])
                    delta  += _peakcost(ξ, peakposition(data, peaks[i]) - ini -sumovr)
                end
            else
                for i = depth+1:length(peaks)
                    sumovr += osize - overlapcount(osize, peaks[i], peaks[i-1])
                    delta  += _peakcost(ξ, peakposition(data, peaks[i]) - ini + sumovr)
                end
            end

            dovr::Float32  = length(peaks)-depth+1
            delta          = delta/dovr
            best           = length(pathedges(k)), min(delta, best[2])
        end
    end

    (-best[1], -overlapcount(ξ, cur[end-1], cur[end]), best[2])
end
