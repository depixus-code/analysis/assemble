module Accessor
using InteractiveUtils: methodswith

function creategetters(mdl::Module, parent, args, doexport = false)
    if parent isa Expr && parent.head == :curly
        parent = parent.args[1]
    end

    names   = Symbol[i.args[1] for i = args]
    evl     = mdl.eval

    root    = parent
    while root isa Expr && root.head == :escape
        root = root.args[1]
    end

    cls     = nothing
    try
        cls   = evl(root)
        names = filter(names) do i
            try
                !any(j.nargs == 2 for j = methodswith(cls, evl(i), supertypes = true))
            catch err
                err != UndefVarError(i) && rethrow(err)
                true
            end
        end
    catch err
        err != UndefVarError(root) && rethrow(err)
    end

    if cls ≡ nothing
        cls = parent
    end
    quote
        $((esc(:($i(ξ::$cls) = ξ.$i)) for i = names)...)
        $(doexport && length(names) > 0 ? :(export $(names...)) : nothing)
    end
end

function createclassgetters(cls, args)
    names = [(i.args[1].args[1], i.args[1].args[2], i.args[2]) for i = args]
    if cls isa Expr && cls.head == :curly
        cls = cls.args[1]
        quote
            $((esc(:($i(ξ::Type{<:$cls}) ::$j = $k)) for (i, j, k) = names)...)
        end
    else
        quote
            $((esc(:($i(ξ::Type{$cls}) ::$j = $k)) for (i, j, k) = names)...)
        end
    end
end

function addconstructor(τ::Expr, opts)
    while τ.head == :escape
        τ = τ.args[1]
    end

    name = if isa(τ.args[2], Symbol) || isa(τ.args[2], Expr) && τ.args[2].head == :curly
        τ.args[2]
    elseif isa(τ.args[2], Expr) && τ.args[2].head == :<:
        τ.args[2].args[1]
    else
        @assert false τ
    end
    
    cstr = createconstructor(name, opts; discardcalls = false, renameargs = false)
    push!(τ.args[3].args, cstr)
end

const LAX = Dict{Symbol, Symbol}((Symbol(j, "Int", i) => :Integer
                                  for i = (8, 16, 32, 64) for j = ("U", ""))...,
                                 (Symbol("Float", i)  => :Real
                                  for i = (16, 32, 64))...,
                                 :String => :AbstractString, 
                                 :SubString => :AbstractString)
function createconstructor(name::Union{Symbol,Expr}, opts;
                           discardcalls = true, renameargs = true)
    j    = 0
    sig1 = []
    sig2 = []
    args = []
    for i = opts
        if discardcalls
            if i isa Expr && i.head ∈ (:function, :call)
                continue
            end
            if i isa Expr && i.head == :(=) && i.args[1] isa Expr && i.args[1].head == :call
                continue
            end
        end
        if      (i isa Expr && i.args[1] isa Expr
                 && i.head ∈ (:(=), :kw)
                 && i.args[1].head == :(::))
            if i.args[1].args[1] isa Expr && i.args[1].args[1] == :call
                continue
            end

            j        += 1
            i         = deepcopy(i)
            if renameargs
                i.args[1] = Symbol(string("_", j, i.args[1].args[1]))
            end
            i.head    = :kw
            push!(sig2, i)
            push!(args, i.args[1])
        elseif i isa Expr && i.head == :(::) && i.args[1] isa Symbol
            j        += 1
            i         = deepcopy(i)
            if renameargs
                i.args[1] = Symbol(string("_", j, i.args[1]))
            end
            push!(sig1, i)
            push!(args, i.args[1])
        else
            push!(args, deepcopy(i))
        end
    end

    sig  = [sig1..., sig2...]

    if name isa Symbol
        :($name($(sig...)) = new($(args...)))
    else
        @assert name.head == :curly
        cpy      = deepcopy(name)
        cpy.args = [i isa Symbol ? i : i.args[1] for i = cpy.args]
        :($cpy($(sig...)) where {$(name.args[2:end]...)} = new($(args...)))
    end
end

function extractinfo(τ, λ)
    while τ.head == :escape
        τ = τ.args[1]
    end
    if isa(τ.args[2], Symbol) || isa(τ.args[2], Expr) && τ.args[2].head == :curly
        name = parent = τ.args[2]
    elseif isa(τ.args[2], Expr) && τ.args[2].head == :<:
        name   = τ.args[2].args[1]
        parent = τ.args[2].args[2]
        name   = τ.args[2].args[1]
    else
        @assert false τ
    end

    if λ == :(::)
        opts = [i for i = τ.args[3].args if isa(i, Expr) && i.head == λ && isa(i.args[1], Symbol)]
    else
        opts = [i for i = τ.args[3].args
                if (isa(i, Expr)
                        && i.head == λ
                        && (isa(i.args[1], Symbol)
                            || isa(i.args[1], Expr) && isa(i.args[1].args[1], Symbol))
                   )]
    end
    name, parent, opts
end

function getfields(τ, λ)
    if λ == :(::)
        [i for i = τ.args[3].args if isa(i, Expr) && i.head == λ && isa(i.args[1], Symbol)]
    else
        [i for i = τ.args[3].args
         if (isa(i, Expr) && i.head == λ
             && (isa(i.args[1], Symbol)
                 || isa(i.args[1], Expr) && isa(i.args[1].args[1], Symbol))
            )]
    end
end

function _defaults(mdl::Module, τ::Expr, cstr::Bool = true, getters::Bool = true)
    name, parent, opts = extractinfo(τ, :(=))

    # remove the default values from the type
    args = τ.head == :esc ? τ.args[1].args[3].args : τ.args[1].args[3].args
    for (i, j) = Iterators.enumerate(args)
        if (isa(j, Expr)
                && j.head == :(=)
                && (isa(j.args[1], Symbol) || j.args[1].head == :(::)))
            args[i] = deepcopy(j.args[1])
        end
    end

    itms = map(opts) do i deepcopy(i.args[1]) end
    if cstr
        push!(args, createconstructor(name, opts))
    end

    if getters
        quote
            Base.@__doc__ $(esc(τ))
            $(esc(creategetters(mdl, parent, itms)))
            $(esc(createclassgetters(name, opts)))
        end
    else
        quote
            Base.@__doc__ $(esc(τ))
        end
    end
end

macro defaults(mdl, cstr, getters, τ)
    _defaults(mdl, τ, cstr, getters)
end

macro defaults(mdl, τ)
    _defaults(mdl, τ)
end

macro defaults(cstr, getters, τ)
    :(@defaults $__module__  $cstr $getters $(esc(τ)))
end

macro defaults(τ)
    :(@defaults $__module__ $(esc(τ)))
end

macro accessors(mdl, τ)
    quote
        Base.@__doc__ $(esc(τ))
        $(esc(creategetters(mdl, extractinfo(τ, :(::))[2:end]...)))
    end
end

macro accessors(τ)
    :(@accessors $__module__ $(esc(τ)))
end

function gettypename(τ; raw ::Bool = true)
    out = τ.args[2]
    rem = raw ? (:curly, :<:, :escape) : (:<:, :escape)
    while isa(out, Expr) && out.head in rem
        out = out.args[1]
    end
    out
end

function clone(::Type{T}, itr::K; opts...) where {T, K}
    kwa = Dict{Symbol, Any}(opts)
    T((get(kwa, i) do; getfield(itr, i) end for i = fieldnames(T))...)
end
clone(::Type{T}; opts...) where T = clone(T, T(); opts...)
clone(itr::T; opts...)    where T = clone(T, itr; opts...)

macro ifsomething(ex)
    quote
        result = $(esc(ex))
        result === nothing && return nothing
        result
    end
end

macro dfltarg(cls, name)
    expr      = :($name = $name($cls))
    expr.head = :kw
    esc(expr)
end

function settypebody(τ::Expr, vals::Vector)
    while τ.head == :escape
        τ = τ.args[1]
    end
    τ.args[3].args = vals
end

function gettypebody(τ::Expr)
    while τ.head == :escape
        τ = τ.args[1]
    end
    τ.args[3].args
end

export @accessors, @defaults, gettypename, getfields, clone, @ifsomething,
       addconstructor, settypebody, gettypebody, @dfltarg
end
