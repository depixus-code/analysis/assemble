memorysize(::GenerationTrees{Mem, Part})    where {Mem, Part} = Mem
memorysize(ξ::Type{<:GenerationTrees})                        = ξ.parameters[1] 
searchdepth(::GenerationTrees{Mem, Part})   where {Mem, Part} = Part
searchdepth(ξ::Type{<:GenerationTrees})                       = ξ.parameters[2] 
ancestordepth(::GenerationTrees{Mem, Part}) where {Mem, Part} = Part+2
ancestordepth(ξ::Type{<:GenerationTrees})                     = ξ.parameters[2]+2

const Generation{T<:TreeNodes} = Vector{Leaves{T}}
const Memory{T<:TreeNodes}     = Vector{Generation{T}}
const GenOutput{T<:TreeNodes}  = Tuple{Memory{T}, Memory{T}}

using ..Oligo:oligoname

function growleaves!_impl(ξ::Type, κ::Type)
    @assert memorysize(ξ) > 0 && searchdepth(ξ) > 0

    eltpe    = eltype(κ)
    parent   = Parent{eltype(κ)}
    unsafegp = :(κ[ileaf])
    safegp   = :(node :: $parent = κ[1])
    for i = 1:searchdepth(ξ)-1
        unsafegp = :(nodeparent($unsafegp))
        safegp   = quote
                $safegp
                node ≡ nothing || (node = nodeparent(node))
        end
    end

    quote
        @assert !isempty(κ)
        ifirst = 1
        $safegp
        if node isa $eltpe
            peak :: $(peaktype(ξ)) = nodepeak(node)
            for ileaf = 2:length(κ)
                gp  = $unsafegp
                cur = nodepeak(gp)
                if peak != cur
                    growleaves!(ξ, view(κ, ifirst:ileaf-1), node, τ)
                    ifirst, peak = ileaf, cur
                    node         = gp
                end
            end
        end
        growleaves!(ξ, view(κ, ifirst:length(κ)), node, τ)
        node ≡ nothing ? node : nodeparent(node)
    end
end

@generated growleaves!(ξ::GenerationTrees, κ::Leaves, τ::Generation) = growleaves!_impl(ξ, κ)

function nodescore(ξ ::GenerationTrees, κ ::Leaves, gp::TreeNodes) ::Float32
    minimum(nodescore(ξ, i, gp) for i = κ)
end

function nodescore(ξ ::GenerationTrees, κ ::Generation, gp::TreeNodes)
    [nodescore(ξ, i, gp) for i = κ]
end

function sortleaves!(ξ ::GenerationTrees, κ ::Generation, gp::Parent)
    if gp !== nothing && length(κ) > 1
        scores = nodescore(ξ, κ, gp)
        permute!(κ, sortperm(scores, order = Base.Reverse))
    end
end

sortleaves!(::GenerationTree, κ::Generation, ::Parent) = κ

function endnodes(κ::GenOutput{T}) ::Generation{T} where {T <: TreeNodes}
    if length(κ[2]) == 0
        return vcat(κ[1]...)
    end

    cur = Set{T}([nodeparent(j) for i = κ[2][end] for j = i])
    if nodeparent(κ[2][end][1][1]) ∈ κ[1][end][end]
        [T[i for i = κ[1][end][end] if i ∉ cur]]
    else
        parent = κ[2][end][1][1]
        nodes  = Leaves{T}[]
        for gens = @view κ[1][end:-1:1]
            if nodeparent(parent) ≡ nothing
                break
            end

            parent = nodeparent(parent)
            for peaks = gens[end:-1:1]
                if parent ∈ peaks
                    return push!(nodes, [i for i = peaks if i ∉ cur])
                end
                push!(nodes, peaks)
            end
        end
        nodes
    end
end

function isendpoint(κ::GenOutput)
    length(κ[2]) == 0 || nodeparent(κ[2][end][1][1]) ∉ κ[1][end][end]
end

Base.eltype(ξ::Type{<:GenerationTrees}) = GenOutput{treenodes(ξ)}
function Base.iterate(ξ::GenerationTrees{M, P}, κ::Memory{T}) where {M, P, T <: TreeNodes}
    if isempty(κ)
        return nothing
    end

    ind :: Tuple{Int, Int} = length(κ), length(κ[end])
    out :: Generation{T}   = Generation{T}()
    gp  :: Parent{T}       = nothing
    while isempty(out) && ind[1] >= 1 && !isempty(κ[ind[1]])
        sortleaves!(ξ, out, growleaves!(ξ, κ[ind[1]][ind[2]], out))
        ind = ind[2] == 1 ?
              (ind[1]-1, ind[1] == 1 ? 0 : length(κ[ind[1]-1])) :
              (ind[1],   ind[2]-1)
    end

    nextκ =
        if isempty(out)
            Memory{T}()
        elseif ind[1] == 0
            [out]
        else
            [(κ[i] for i = (length(κ) == M ? 2 : 1):ind[1]-1)...,
             κ[ind[1]][1:ind[2]], out]
        end
    (κ, nextκ), nextκ
end
function Base.iterate(ξ::T) where {T <: GenerationTrees}
    emptyexperiment(ξ) ? nothing : iterate(ξ, [[[treenodes(ξ)]]])
end
