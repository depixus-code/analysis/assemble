module PeakTree
using ..Accessor

include("_Trees.jl")
include("_GenerationTrees.jl")
include("_ScoreTrees.jl")
include("_AndreasInSilico.jl")
end
