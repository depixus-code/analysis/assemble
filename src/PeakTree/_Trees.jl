import Base:  show, print
using  ..Oligo: Experiment, OligoNode, OligoConfigs, Neighbour, BatchedOligo,
    overlaps, AVECTPEAK, APEAK, Direction, goleft, goright, initialpeaks,
    @peakindexes, haspathpeaks, update!, @pathpeaks, peaktypecode

import ..Oligo: experiment, emptyexperiment, estimatedposition, peakposition,
    oligoname, oligopeakid, oligoid, pathpositions, pathpeaks, pathoverlaps,
    peaktype, oligotype, positionloss

for i = fieldnames(OligoNode)
    @eval import ..Oligo: $i
end

abstract type Trees                               <: OligoConfigs               end
abstract type GenerationTrees{Mem, Part}          <: Trees                      end
abstract type ScoreTrees{Mem, Part}               <: GenerationTrees{Mem, Part} end
abstract type L²uScoreTrees{Mem, Part}            <: ScoreTrees{Mem, Part}      end
abstract type ThresholdedL²uScoreTrees{Mem, Part} <: L²uScoreTrees{Mem, Part}   end

experiment(ξ::Trees)      = ξ.experiment
emptyexperiment(ξ::Trees) = emptyexperiment(experiment(ξ))

function __treeopts(pk::Type)
    epk = Tuple{pk, Int}
    vpk = Vector{epk}
    :(searchwindow   :: Float32    = 10f0,
      minoverlap     :: Int        = 1,
      oligosize      :: Int        = 3,
      direction      :: Direction  = goright,
      positionloss   :: Float32    = .33f0,
      experiment     :: Experiment = Vector{Float32}[],
      startpeaks     :: $vpk       = $epk[],
      stoppeaks      :: $vpk       = $epk[]
     ).args
end

macro trees(τ)
    pk, peaktype = peaktypecode(τ)
    opts         = __treeopts(pk)

    settypebody(τ, [opts[1:end-3]..., gettypebody(τ)..., opts[end-2:end]...])

    dflt = [i.args[1].args[1] == :experiment ? :(τ::Experiment) :
            i.args[1].args[1] == :oligosize  ? :(oligosize(τ))  :
            i.args[2]
            for i = τ.args[3].args if i isa Expr && i.head == :(=)]
    addconstructor(τ, dflt)

    quote
        @defaults $__module__ $(esc(τ))
        $(peaktype...)
        export $(gettypename(τ; raw = true))
    end
end

@trees struct Tree                        <: Trees                       end

@trees struct GenerationTree{Mem, Part}   <: GenerationTrees{Mem, Part}  end

@trees struct LazyL²uScoreTree{Mem, Part} <: ScoreTrees{Mem, Part}
    undetected   ::Float32 = 10f0
end

for tpe = ("", "Batch")
    cls = "L²uScoreTree"
    @eval @trees struct $(Symbol(tpe, cls)){Mem, Part} <: $(Symbol(cls, "s")){Mem, Part}
        undetected   ::Float32 = 10f0
    end

    cls = "ThresholdedL²uScoreTree"
    @eval @trees struct $(Symbol(tpe, cls)){Mem, Part} <: $(Symbol(cls, "s")){Mem, Part}
        undetected   ::Float32 = 10f0
        softthreshold::Float32 = 20f0
        hardthreshold::Float32 = 40f0
    end
end

for fcn = (:residual, :symmetry, :normedresidual)
    @eval begin
        import ..Oligo: $fcn
        $fcn(ξ::Trees, ♠::AVECTPEAK) = $fcn(ξ, experiment(ξ), ♠)
    end
end

abstract type TreeNodes{T<:APEAK} end

const Parent{T<:TreeNodes} = Union{Nothing, T}
const Leaves{T<:TreeNodes} = Vector{T}

@accessors struct TreeNode{T} <: TreeNodes{T}
    nodepeak     ::T
    nodeparent   ::Parent{TreeNode{T}}
    nodeposition ::Float32
end

nodepeak(κ::Tuple{T, Int}) where T = κ
oligoname(ξ::Trees, ♠::TreeNodes) = oligoname(oligosize(ξ), nodepeak(♠))
oligoid(♠::TreeNodes)             = nodepeak(♠)[1]
oligopeakid(♠::TreeNodes)         = nodepeak(♠)[2]

function Base.print(io::IO, κ::T) where {T <: TreeNodes}
    out  = "$(typeof(κ))(["
    for i = κ
        out *="$(nodepeak(i)), "
    end
    out  = out[1:end-2]*"]"
    for i = fieldnames(T)
        if i ∉ (:nodepeak, :nodeparent)
            out *= ", $(@eval($i($κ)))"
        end
    end
    print(io, out*")")
end
Base.show(io::IO, res::TreeNodes) = print(io, res)


for (pname, nname) = [(Symbol("path$(i)s"), Symbol("node$i")) for i = ("peak", "position")]
    @eval $pname(κ::TreeNodes)          = map($nname, κ)[end:-1:1]
    @eval $pname(::Trees, κ::TreeNodes) = map($nname, κ)[end:-1:1]
end
pathoverlaps(ξ::Trees, κ::TreeNodes) = pathoverlaps(ξ, pathpeaks(κ))

function TreeNode{T}(ξ::Trees) where T
    if length(startpeaks(ξ)) == 0
        pks = initialpeaks(peaktype(T), direction(ξ), experiment(ξ))[1:1]
    else
        pks = copy(startpeaks(ξ))
    end

    TreeNode{T}(pks[end], nothing, estimatedposition(ξ, pks))
end

treenodes(ξ::Type{T}) where {T <:Trees} = TreeNode{peaktype(ξ)}
treenodes(ξ::T)       where {T <:Trees} = TreeNode{peaktype(T)}(ξ)
function treenodes(ξ::Trees, κ::K, ♠::APEAK, ::Parent) where {K <: TreeNodes}
    K(♠, κ, estimatedposition(ξ, κ, ♠))
end

peakposition(ξ::Trees,      κ::TreeNodes) = peakposition(experiment(ξ), nodepeak(κ))
peakposition(ξ::Trees,      κ::APEAK)     = peakposition(experiment(ξ), κ)
peakposition(ξ::Experiment, κ::TreeNodes) = peakposition(ξ,             nodepeak(κ))

for fcn = (:overlapcount, :missingcount, :basecount, :baseposition, :sequence)
    @eval begin
        import ..Oligo: $fcn
        function $fcn(ξ::Trees, state::TreeNodes)
            vals = map(nodepeak, state)
            $fcn(oligosize(ξ), direction(ξ), view(vals, length(vals):-1:1))
        end
    end
end

function currentbaseposition(ξ::Trees, κ::TreeNodes) :: Float32
    nodeparent(κ) ≡ nothing && return peakposition(experiment(ξ), κ)

    osize      = oligosize(ξ)
    last       = nodepeak(κ)
    cnt  ::Int = 0

    if direction(ξ) ≡ goright
        for i = nodeparent(κ)
            cur  = nodepeak(i)
            cnt += osize - overlapcount(osize, cur, last)
            last = cur
        end
    else
        for i = nodeparent(κ)
            cur  = nodepeak(i)
            cnt -= osize - overlapcount(osize, last, cur)
            last = cur
        end
    end

    data = experiment(ξ)
    (peakposition(data, κ)+peakposition(data, last)+cnt)*.5f0
end

function estimatedposition(ξ::Trees, κ::TreeNodes, ♠::APEAK)
    estimatedposition(ξ, positionloss(ξ), experiment(ξ), nodepeak(κ)[1], nodeposition(κ), ♠)
end

function estimatedposition(ξ::Trees, ♠::AVECTPEAK)
    estimatedposition(ξ, positionloss(ξ), experiment(ξ), ♠)
end

function _pk_indexes(ξ::Trees, κ::TreeNodes{T}) where T
    @peakindexes T ξ (nodepeak(i) for i = κ) nodepeak(κ) nodeposition(κ)
end

"""
Check whether there are any more possible paths out of the current node
"""
hasleaves(ξ::Trees, κ::TreeNodes) = haspathpeaks(_pk_indexes(ξ, κ), ξ)

function overlappingpeaks(ξ::Trees, κ::TreeNodes{T}) ::Vector{Vector{T}} where T
    @pathpeaks T ξ _pk_indexes(ξ, κ) nothing nothing
end

function growleaves(ξ::Trees, κ::T, gp::Parent) :: Leaves{T} where {T <: TreeNodes}
    used = overlappingpeaks(ξ, κ)
    isempty(used) ? Leaves{T}() : vcat(map(x -> map(y->treenodes(ξ, κ, y, gp), x), used)...)
end

function growleaves(ξ::Trees, κ::AbstractVector{T}, gp::Parent
                   ) :: Leaves{T} where {T <: TreeNodes}
    vcat(map(x->growleaves(ξ, x, gp), κ)...)
end

function growleaves!(ξ ::Trees,
                     κ ::AbstractVector{T},
                     gp::Parent,
                     τ ::Vector{Leaves{T}}) where {T <: TreeNodes}
    leaves = growleaves(ξ, κ, gp)
    if !isempty(leaves)
        push!(τ, leaves)
    end
end

function nodeparent(κ::T, gen::Int) :: Parent{T} where {T <: TreeNodes}
    @assert gen >= 1
    parent :: Parent{T} = nodeparent(κ)
    for i = 2:gen
        if parent ≡ nothing
            break
        end
        parent = nodeparent(parent)
    end
    parent
end


@generated function nodeparent(κ::T, ::Type{Val{K}}) where {T <: TreeNodes, K}
    ex = :()
    for i = 2:K
        ex = quote
            if x !== nothing
                x = nodeparent(x)
                $ex
            end
        end
    end
    quote 
        x = nodeparent(κ)
        $ex
        x
    end
end

Base.IteratorSize(::TreeNodes)                                       = Base.SizeUnknown()
Base.eltype(::Type{T})                        where {T <: TreeNodes} = T
function Base.iterate(ξ::T, κ::Parent{T} = ξ) where {T <: TreeNodes}
    κ ≡ nothing ? nothing : (κ, nodeparent(κ))
end

Base.IteratorSize(::Trees)    = Base.SizeUnknown()
Base.eltype(ξ::Type{<:Trees}) = Tuple{Leaves{treenodes(ξ)}, Leaves{treenodes(ξ)}}

function Base.iterate(ξ::Trees, κ::Leaves{T}) where {T <: TreeNodes}
    if isempty(κ)
        return nothing
    end
    x = growleaves(ξ, κ, nothing)
    ((κ, x), x)
end

function Base.iterate(ξ::T) where {T <: Trees}
    emptyexperiment(ξ) ? nothing : iterate(ξ, treenodes(T)[treenodes(ξ)])
end
