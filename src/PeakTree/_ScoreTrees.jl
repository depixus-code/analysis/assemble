import ..Oligo: pathscores, pathbases, pathzeros, start

for cls = (:L²uScoreTree, :ThresholdedL²uScoreTree, :BatchThresholdedL²uScoreTree)
    @eval $cls(a...) = $cls{4, 4}(a...)
end

@accessors struct L²uTreeNode{T} <: TreeNodes{T}
    nodepeak     ::T
    nodeparent   ::Parent{L²uTreeNode{T}}
    nodeposition ::Float32
    nodescore    ::Float32
    nodezero     ::Float32
    nodegen      ::Int
    nodebase     ::Int
end

for (pname, nname) = [(Symbol("path$(i)s"), Symbol("node$i"))
                      for i = ("score", "zero", "count", "base")]
    @eval $pname(κ::L²uTreeNode)                  = map($nname, κ)[end:-1:1]
    @eval $pname(::L²uScoreTrees, κ::L²uTreeNode) = map($nname, κ)[end:-1:1]
end

currentbaseposition(::ScoreTrees, κ::L²uTreeNode) = nodezero(κ)
nodescore(::ScoreTrees, κ::L²uTreeNode)           = nodescore(κ)

treenodes(ξ::Type{T}) where {T<:L²uScoreTrees}    = L²uTreeNode{peaktype(T)}
function treenodes(ξ::T) where {T<:L²uScoreTrees}
    if length(startpeaks(ξ)) == 0
        pks = initialpeaks(peaktype(T), direction(ξ), experiment(ξ))
        if length(pks) > 1
            pks = pks[1:1]
        end
    else
        pks = copy(startpeaks(ξ))
    end

    node = TreeNode{peaktype(T)}(ξ)
    treenodes(T)(nodepeak(node), nothing, nodeposition(node),
                 0f0, nodeposition(node), 0, 0)
end

@generated function treenodes(ξ::L²uScoreTrees,
                              κ::L²uTreeNode,
                              ♠::APEAK,
                              gp::Parent{<:L²uTreeNode})
    ntype = treenodes(ξ)
    function _code(dir::Direction)
        cnt          = searchdepth(ξ)
        getinc(x, y) = (dir ≡ goright ?
                        :(osize-overlapcount(osize, $x, $y)) :
                        :(osize-overlapcount(osize, $y, $x)))
        incop        = dir ≡ goright ? :+ : :-
        decop        = dir ≡ goright ? :- : :+
        cost         = :((peakposition(data, nodepeak(leaf))-$incop(zero, nodebase(leaf)))^2)
        quote
            ovr   = $(getinc(:(nodepeak(κ)), :♠))
            bpos  = nodebase(κ)+ovr
            rho   = (gen-1)/gen
            zero  = nodezero(κ)*rho + (1f0 - rho)*($decop(peakposition(ξ, ♠),bpos))
            if gp !== nothing
                $((quote
                       score += $cost
                       leaf  = nodeparent(leaf)
                   end for i = 1:cnt-1)...)
                score += $cost + (peakposition(data, ♠) - $incop(zero, bpos))^2
                tmp = nodeparent(leaf)
                if tmp !== nothing
                    leaf  = tmp
                    score = ((score+$cost)/$(cnt+1)
                             + undetected(ξ)*($incop(bpos-nodebase(leaf))-$(1+cnt)))
                else
                    score = (score/$(cnt+1)
                             + undetected(ξ)*($incop(bpos-nodebase(leaf))-$cnt))
                end
            end
        end
    end

    quote
        osize = oligosize(ξ)
        data  = experiment(ξ)
        gen   = nodegen(κ)+1
        leaf  = κ
        score = 0f0
        if direction(ξ) ≡ goright
            $(_code(goright))
        else
            $(_code(goleft))
        end

        $ntype(♠, κ, estimatedposition(ξ, κ, ♠), score, zero, gen, bpos)
    end
end

function nodescore(ξ::L²uScoreTrees{M, S},
                   κ ::Generation{<:L²uTreeNode},
                   ::L²uTreeNode) where {M, S}
    Tuple{Float32, Float32}[(mapreduce(nodescore, min, i),
                             peakposition(ξ, nodepeak(nodeparent(i[1], Val{S}))))
                            for i = κ]
end

function growleaves!(ξ ::ThresholdedL²uScoreTrees,
                     κ ::AbstractVector{<:L²uTreeNode},
                     gp::Parent{<:L²uTreeNode},
                     τ ::Generation{<:L²uTreeNode})
    hard   = hardthreshold(ξ)
    leaves = vcat(( begin 
                        x = growleaves(ξ, i, gp)
                        x[map(x->nodescore(x)[1] < hard, x)]
                    end for i = κ)...)
    if length(leaves) > 0
        push!(τ, leaves)
    end
end

function sortleaves!(ξ ::ThresholdedL²uScoreTrees,
                     κ ::Generation{<:L²uTreeNode},
                     gp::Parent{<:L²uTreeNode})
    if length(κ) == 0
        return
    end

    soft = softthreshold(ξ)
    if gp !== nothing && length(κ) > 1
        scores = nodescore(ξ, κ, gp)
        permute!(κ, sortperm(scores, order = Base.Reverse))
        good = any(soft > i[1] for i = scores)
    else
        good = any(soft > nodescore(i) for i = κ[end])
    end

    if length(κ[end]) > 1 && good
        cur = κ[end]
        thr = map(x->nodescore(x) < soft, cur)
        if !all(thr)
            push!(κ, cur[thr])
            j = 1
            for (i, leaf) = Iterators.enumerate(cur)
                if !thr[i]
                    if j != i
                        cur[j] = leaf
                    end
                    j += 1
                end
            end
            resize!(cur, j)
        end
    end
end

function __nodescore_vars(::Type{<:ScoreTrees}, ♠::Type{T}, cnt::Int, fcn
                         ) where {T <: APEAK}
    quote
        peaks   = Vector{$♠}(undef, $cnt)
        osize   = oligosize(ξ)
        data    = experiment(ξ)
        missing = undetected(ξ)
        ini     = currentbaseposition(ξ, ancestor)
        bpos::Float32 = 0f0
        cost::Float32 = 0f0

        if direction(ξ) ≡ goright
            score = $(fcn(goright))
        else
            score = $(fcn(goleft))
        end
    end
end

function __nodescore_impl(dir::Direction, ξ::Type{<:ScoreTrees}) :: Expr
    getinc(x, y) = (dir ≡ goright ?
                    :(osize-overlapcount(osize, $x, $y)) :
                    :(osize-overlapcount(osize, $y, $x)))
    incop        = dir ≡ goright ? :+ : :-
    cnt          = ancestordepth(ξ)
    quote
        begin
            $((quote
                   peaks[$i] = nodepeak(leaf)
                   leaf      = nodeparent(leaf)
               end for i = cnt:-1:2)...)

            bpos = $incop(ini, $(getinc(:(nodepeak(leaf)), :(peaks[2]))))
            cost = (peakposition(data, peaks[2])-bpos)^2

            $((quote
                   bpos  = $incop(bpos, $(getinc(:(peaks[$(i-1)]), :(peaks[$i]))))
                   cost += (peakposition(data, peaks[$i])-bpos)^2
               end for i = 3:cnt)...)
            cost + missing*($incop(bpos-ini)-$(cnt-1))
        end
    end
end

@generated function nodescore(ξ       ::ScoreTrees,
                              κ       ::Generation{<:TreeNodes},
                              ancestor::TreeNodes)
    ♠ = peaktype(ξ)
    function __max(dir::Direction)
        quote
            for (i, leaves) = Iterators.enumerate(κ), leaf = leaves
                score = $(__nodescore_impl(dir, ξ))
                score < scores[i] && (scores[i] = score)
            end
        end
    end

    quote
        scores :: Vector{Float32} = fill(Inf32, length(κ))
        $(__nodescore_vars(ξ, ♠, ancestordepth(ξ), __max))
        scores
    end
end

@generated function nodescore(ξ::ScoreTrees, leaf::TreeNodes)
    cnt = ancestordepth(ξ)
    ♠   = peaktype(ξ)
    quote
        gp = nodeparent(leaf, $(cnt-1))
        if gp ≡ nothing
            0f0
        else
            ancestor = gp
            $(__nodescore_vars(ξ, ♠, cnt, x->__nodescore_impl(x, ξ)))
            score/$cnt
        end
    end
end
