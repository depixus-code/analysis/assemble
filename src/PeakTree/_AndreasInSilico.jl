using  ..Accessor: clone
using  ..Oligo:    emptyexperiment
using  ..PeakPaths: L2ScorePath, L²uScorePath, Paths, BatchL2ScorePath,
    BatchL²uScorePath, insertremaining, AndreasInSiScore, better!,
    peaks
import ..PeakPaths: bestspoonerism, andreasinsi

const SOLVERS    = Union{<:Paths, <:GenerationTrees}
const L²uSOLVERS = Union{L²uScorePath, <:L²uScoreTrees}
function convert(left ::Type{SOLVERS}, right::SOLVERS)
    if (left <: L²uSOLVERS) && !(right isa L²uSOLVERS)
        throw(InexactError())
    end
    left((eval(:($i($right))) for i in fieldnames(left))...)
end

@generated function topaths(ξ::GenerationTrees)
    isbatch = BatchedOligo ≡ oligotype(ξ)
    paths   = ξ <: L²uScoreTrees ?
              isbatch ? BatchL²uScorePath : L²uScorePath :
              isbatch ? BatchL2ScorePath  : L2ScorePath;
    quote
        $paths($((:($i(ξ)) for i in fieldnames(paths))...))
    end
end

function Base.max(ξ::T, falsepositives::Float32 = 0f0, remaining ::Bool = true;
                  opts...) where {T<:Trees}
    ξ = clone(ξ; opts...)
    emptyexperiment(experiment(ξ)) && return nothing

    best    = nothing
    scoring = AndreasInSiScore(topaths(ξ), remaining, falsepositives)
    for gens = ξ
        if !isendpoint(gens) || sum(1 for _ = gens[1][end][1][1]) < length(peaks(scoring))
            continue
        end

        for leaves = @view(gens[1][end][end:-1:1]), leaf = leaves
            better!(scoring, map(nodepeak, leaf)[end:-1:1]) && (best = leaf)
        end
    end
    best
end

bestspoonerism(ξ::Trees, ♠::Vector{<:APEAK})      = bestspoonerism(topaths(ξ), ♠)
bestspoonerism(ξ::Trees, ♠::Vector{<:APEAK}, fcn) = bestspoonerism(topaths(ξ), ♠, fcn)

function andreasinsi(ξ::Trees, falsepositives ::Float32, remaining::Bool, spoonerism::Bool)
    leaf = max(ξ, falsepositives, remaining)
    if leaf ≡ nothing
        return peaktype(typeof(ξ))[]
    end

    best = map(nodepeak, leaf)[end:-1:1]
    if remaining
        best = insertremaining(topaths(ξ), best)
    end
    if spoonerism
        best = bestspoonerism(topaths(ξ), best)
    end
    best
end

function andreasinsi(ξ              ::T; 
                     falsepositives ::Float32                                = 1f0,
                     nodes          ::Union{Vector{<:APEAK}, APEAK, Nothing} = nothing,
                     bothsides      ::Integer                                = 1,
                     remaining      ::Bool                                   = false,
                     spoonerism     ::Bool                                   = false,
                     opts...) where {T <: Trees}
    ξ = clone(ξ; opts...)
    if all(length(i) == 0 for i = experiment(ξ))
        return peaktype(T)[]
    end

    if nodes isa APEAK
        nodes = [nodes]
    elseif nodes == nothing
        nodes = initialpeaks(peaktype(T), direction(ξ), experiment(ξ))
    end
    
    best          = peaktype(T)[]
    paths ::Paths = topaths(ξ)
    for node in nodes
        out = andreasinsi(clone(ξ; startpeaks = [node]), falsepositives, remaining, false)

        if bothsides > 0
            tmp = clone(paths;
                        direction  = ~direction(paths),
                        startpeaks = out[end:-1:bothsides]
                       )
            out = andreasinsi(tmp, 4, falsepositives, false, false)[end:-1:1]
        end

        if spoonerism
            out = bestspoonerism(paths, out)
        end

        if length(best) < length(out)
            best = out
        end
    end
    best
end
