module ZConversion
using Statistics: mean, cov

using ..Oligo: Experiment, baseposition, oligosize, groupedpeaks, peakposition,
               Direction, goleft, goright

struct ZConversionConfig
    stretch :: Tuple{Float32, Float32}
    bias    :: Tuple{Float32, Float32}
end

"""
Return rigid transformations which can convert the experiment to the best base
pair values given a sequence.
"""
function basepairfactors(osize::Int,
                         odir ::Direction,
                         cnf  ::ZConversionConfig,
                         data ::Experiment,
                         peaks::Array{T,1}
                        ) where {T}
    bp  = convert(Array{Float32,1}, baseposition(osize, odir, peaks))
    arr = [(0f0, 1f0) for i = 1:length(data)]

    for (iol, inds) = enumerate(groupedpeaks(peaks))
        if length(inds) <= 0
            continue
        end

        x = bp[inds]
        y = [peakposition(data, peaks[i]) for i = inds]
        if length(inds) > 1
            stretch = max(cnf.stretch[1], min(cnf.stretch[2], cov(y, x)/cov(x)))
            bias    = max(cnf.bias[1],    min(cnf.bias[2],    mean(y)-stretch*mean(x)))
            if stretch*peakposition(data, iol, 1)+bias < 0f0
                bias = max(cnf.bias[1],   min(cnf.bias[2],    -stretch*zero))
            end

        elseif x[1] == 0f0
            stretch = 0f0
            bias    = max(cnf.bias[1], min(cnf.bias[1], y[1]-x[1]))

        elseif cnf.stretch[1] <= y[1]/x[1] <= cnf.stretch[2]
            stretch = y[1]/x[1]
            bias    = 0f0

        else
            bias    = max(cnf.bias[1],    min(cnf.bias[2],     y[1]-x[1]))
            stretch = max(cnf.stretch[1], min(cnf.stretch[2], (y[1]-bias)/x[1]))
        end

        arr[iol] = bias, stretch
    end
    arr
end

function basepairfactors(odir     ::Direction,
                         cnf      ::ZConversionConfig,
                         data     ::Experiment,
                         peaks    ::Array{T,1}
                        ) where {T}
    basepairfactors(odir, oligosize(data), data, peaks)
end

"""
Return rigidly transformed experimental positions to optimal base pair positions.
"""
function tobasepair(oligosize::Int,
                    direction::Direction,
                    data     ::Experiment,
                    peaks    ::Array{T,1}) where {T}
    ((x,y) -> x*y[2]+y[1]).(data, basepairfactors(oligosize, direction, data, peaks))
end

function tobasepair(direction::Direction, data::Experiment, peaks::Array{T,1}) where {T}
    tobasepair(direction, oligosize(data), data, peaks)
end
end
