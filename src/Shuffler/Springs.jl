module Springs

using LinearAlgebra.LAPACK: gels!
using ..Oligo: Experiment, AVECTPEAK, overlapcount, oligosize, groupedpeaks, peakposition

struct SpringConfig
    zero ::Float32
    intra::Float32
    inter::Float32
end

function springsystem(cnf::SpringConfig, data::Experiment, peaks::AVECTPEAK)
    intra = groupedpeaks(peaks)
    inter = filter(x-> peaks[x][1] != peaks[x+1][1], 1:length(peaks)-1)

    nsprings = sum(length(i) for i=intra) + length(inter)
    nnodes   = length(peaks)
    adj      = zeros(Float64, (nsprings, nnodes))
    equil    = zeros(Float64, nsprings)
    ind      = 1

    for same = filter(x->length(x) > 0, intra)
        pos               = peakposition(data, peaks[same[1]][1], [i[2] for i in peaks[same]])

        adj[ind, same[1]] = cnf.zero
        equil[ind]        = cnf.zero * pos[1]
        ind              += 1

        for k = 1:length(same)-1
            adj[ind, same[k]]   = -cnf.intra
            adj[ind, same[k+1]] = cnf.intra
            equil[ind]          = cnf.intra*(pos[k+1]-pos[k])
            ind += 1
        end
    end

    olsize = oligosize(data)
    for j = inter
        adj[ind, j]   = -cnf.inter
        adj[ind, j+1] = cnf.inter
        equil[ind]    = cnf.inter*(olsize - overlapcount(olsize, peaks[j][1], peaks[j+1][1]))
        ind          += 1
    end

    adj, equil
end

function equilibrium(cnf::SpringConfig, data::Experiment, peaks::AVECTPEAK)
    gels!('N', springsystem(cnf, data, peaks)...)
end

function equilibriumorder(cnf::SpringConfig, data::Experiment, peaks::AVECTPEAK)
    sortperm(equilibrium(cnf, data, peaks)[2])
end
end
