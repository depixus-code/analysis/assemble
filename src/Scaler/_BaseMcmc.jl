import ..Accessor: clone
using  ..Oligo:       Experiment, peaktype

const _RND = Random.GLOBAL_RNG

abstract type MCConfigs end
abstract type MCStates end

struct Best{T <: MCConfigs} <: MCConfigs
    config :: T
end
Best{T}(args...) where {T <: MCConfigs} = Best{T}(T(args...))

best(ξ::T)                   where T    = Best{T}(ξ)
best(ξ::Best)                           = ξ
best(τ::MCStates)                       = τ
best(τ::Tuple{<:MCStates, <:MCStates})  = τ[1]

configtype(ξ::Type{Best{T}}) where T = T
configtype(ξ::Type{T})       where T = T
config(ξ::Best)                      = ξ.config
config(ξ::MCConfigs)                 = ξ

current(τ::MCStates)                      = τ
current(τ::Tuple{<:MCStates, <:MCStates}) = τ[2]

MC_OPTS = :(experiment ::Experiment,
            rng        ::AbstractRNG).args

function clone(::Type{T}, itr::K; opts...) where {T <:MCConfigs, K <:MCConfigs}
    kwa  = Dict{Symbol, Any}(opts)
    orig = config(itr)
    tpe  = configtype(T)
    T((get(kwa, i) do; getfield(orig, i) end for i = fieldnames(tpe))...)
end
clone(::Type{T}; opts...) where {T <: MCConfigs} = clone(T, T(); opts...)
clone(itr::T; opts...)    where {T <: MCConfigs} = clone(T, itr; opts...)

""""
Creates a structure which contains fields in MCCONFIG_FIELDS.

It also adds a constructor which uses Base.Random.GLOBAL_RNG as a default AbstractRNG.
Finally, getters are created for all fields.
"""
macro mcconfigs(τ)
    sig  = [:exp, (i.args[1] for i in getfields(τ, :(::)))...]
    τ.args[3].args = [MC_OPTS..., τ.args[3].args...]
    
    name = gettypename(τ)

    quote
        @accessors $__module__ $(esc(τ))
        function Mcmc.$name($(sig...))
             $name($(sig[1]), _RND, $(sig[2:end]...))
        end
        export $name
    end
end

stretches(τ::MCStates) = τ.curparams[:,1]
biases(τ::MCStates)    = τ.curparams[:,2]

experiment(ξ::MCConfigs, τ::AbstractArray{Float32, 2}) = peakposition(experiment(ξ), τ)
experiment(ξ::MCConfigs, τ::MCStates) = peakposition(experiment(ξ), curparams(τ))

loglikelihood(τ::MCStates) :: Float32 = sum(logposterior(τ))+sum(logprior(τ))

temperature(x...) = 1f0
function accept(ξ::MCConfigs, τ0::MCStates, τ1::MCStates) :: Bool
    llτ0 = loglikelihood(τ0)
    llτ1 = loglikelihood(τ1)
    llτ0 < llτ1 || log(rand(rng(ξ))) < (llτ1-llτ0)/temperature(ξ, τ1)
end

Base.length(ξ::MCConfigs)             = niterations(ξ)

function Base.iterate(ξ::T) where {T <: MCConfigs}
    if niterations(ξ) < 1
        return nothing
    end

    cur = initialize(ξ, eltype(T)(ξ))
    (cur, cur)
end

function Base.iterate(ξ::MCConfigs, τ::T) where {T <: MCStates}
    if iteration(τ) >= niterations(ξ)
        return nothing
    end
    cur = initialize(ξ, rand(ξ, τ))
    (cur, accept(ξ, τ, cur) ? cur : T(iteration(cur), τ))
end

Base.eltype(::Type{Best{T}}) where T = Tuple{eltype(T), eltype(T)}
function Base.iterate(ξ::Best, τ::Tuple{<:MCStates, <:MCStates})
    val, cur = @ifsomething iterate(config(ξ), current(τ))
    maxv     = loglikelihood(best(τ)) < loglikelihood(val) ? val : best(τ)
    (maxv, val), (maxv, cur)
end

function Base.iterate(ξ::Best)
    val, cur = @ifsomething iterate(config(ξ))
    (val, val), (val, cur)
end
