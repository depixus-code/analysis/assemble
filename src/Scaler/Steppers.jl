module Steppers
using Oligo: Experiment
abstract type ISteppers end
Base.eltype(::ISteppers) = Experiment
end
