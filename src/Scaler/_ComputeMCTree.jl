using  Statistics:  mean
using  ..Oligo:       oligosize, baseposition, isused, Direction, goleft, goright,
    searchwindow, AVECTPEAK
import ..Oligo:       SequenceStats

SequenceStats(ξ::MCConfigs, τ::MCStates) = SequenceStats(experiment(ξ, τ), peakspath(τ))

function lmmatrix(ξ::MCConfigs, peaks::Vector{T}) where {T<:APEAK}
    oligs   = begin
        tmp = Set{Int32}(x[1] for x = peaks)
        Dict{Int32, Int32}(zip(tmp, 1:length(tmp)))
    end

    npeaks = length(peaks)
    nols   = length(oligs)
    nrow   = ((positionnoise(ξ) > 0f0  ? npeaks : 0)
              + sum(y.spread  > 0f0 ? nols : 0 for y = (stretch(ξ), bias(ξ)))
              + sum(y.average > 0f0 ? 1    : 0 for y = (stretch(ξ), bias(ξ))))

    adj    = zeros(Float64, (nrow, 2*nols))
    equil  = zeros(Float64, nrow)

    irow   = 1
    if positionnoise(ξ) > 0. && npeaks > 0
        ys  = baseposition(oligosize(ξ), direction(ξ), peaks)
        iαs = map(x -> oligs[x[1]],       length(ys) == 0 ? T[] : peaks)
        pos = peakposition(experiment(ξ), length(ys) == 0 ? T[] : peaks)

        # 1/N Σ(α_i x_ij + β_i - y_ij)²/σ_x ²
        inds             = iαs .* 2nrow  .+ (1-2nrow:npeaks-2nrow)
        factor           = 1f0 / (sqrt(npeaks)*positionnoise(ξ))
        adj[inds]       .= factor .* pos
        adj[inds.+nrow] .= factor
        equil[1:npeaks] .= factor .* ys
        irow            += npeaks
    end

    for (off, obj) = enumerate((stretch(ξ), bias(ξ)))
        if obj.spread > 0f0
            # 1/n (α_i - 1/n Σα_j)²/σ_α ²    1/n (β_i - 1/n Σβ_j)²/σ_β ²
            ind                                      = irow+(off-1)*nrow
            factor                                   = 1f0 / (obj.spread*nols*sqrt(nols))
            adj[irow:irow+nols-1, off:2:end]        .= -factor
            adj[ind:2nrow+1:ind+(2nrow+1)*(nols-1)] .=  factor*(nols-1)
            irow                                    += nols
        end

        if obj.average > 0f0
            # (1/n Σα_j - 1)²/σ_Α ²         (i/N Σβ_i)²      /σ_Β ²``
            adj[irow, off:2:end] .= 1f0 / (nols*obj.average)
            if off == 1
                equil[irow]       = factor
            end
            irow                 += 1
        end
    end

    oligs, adj, equil
end

"""
Optimize stretches and biases given the prior constraints.
    
does ``min_{α_i, β_i} || A [α_i, β_i] - B ||_2``, where A and B allow minimizing:

``1/N Σ(α_i x_ij + β_i - y_ij)²/σ_x ²
  + 1/n (α_i - 1/n Σα_j)²/σ_α ²
  + 1/n (β_i - 1/n Σβ_j)²/σ_β ²
  + (1/n Σα_j - 1)²/σ_Α ²
  + (i/N Σβ_i)²    /σ_Β ²
``

where:

* ``x_ij`` values run over selected experimental peaks
* ``y_ij`` values run over theoretical positions of selected experimental peaks

The different lines allow :

1. minimizing the error between the experimental and theoretical positions,
2. minimizing the spread of α_i values,
3. minimizing the spread of β_i values,
4. maximizing the prior probability for the average stretch (≈ 1).
5. maximizing the prior probability for the average bias    (≈ 0)
"""
function lmsolve(ξ::MCConfigs, ρ::Array{Float32,2}, ♠::Vector{<:APEAK})
    oligs, adj, equ = lmmatrix(ξ, ♠)
    best            = adj \ equ

    params          = copy(ρ)
    foreach(oligs) do i params[i[1],:] .= best[2i[2]-1:2i[2]] end

    params
end
lmsolve(ξ::MCConfigs, τ::MCStates) = lmsolve(ξ, curparams(τ), peakspath(τ))

function lmstate(ξ::MCConfigs, ρ::Array{Float32,2}, ♠::Vector{<:APEAK})
    initialize(ξ, MCTreeState(0, lmsolve(ξ, ρ, ♠)))
end

function lmstate(ξ  ::MCConfigs,
                 τ  ::T   = @ifsomething(iterate(ξ))[1],
                 cnt::Int = 1) where {T <: MCStates}
    state = τ
    for _ = 1:cnt
        state = initialize(ξ, T(iteration(τ), lmsolve(ξ, state)))
    end
    state
end

function _logprior(ξ::MCConfigs, τ::Array{Float32, 2}) ::Vector{Float32}
    avg  = stretch(ξ).average
    xp   = stretch(ξ).spread
    dat  = experiment(ξ)
    good = isused(dat)
    res  = zeros(Float32, length(dat))
    if length(good) == 0
        return res
    end

    if avg != 0f0 || xp != 0f0
        tmp      = @view(τ[:,1])[good]
        mstretch = mean(tmp)
        if avg != 0f0
            res[good] .+= ((mstretch-1f0)/avg)^2/length(good)
        end

        if xp != 0f0
            res[good] .+= ((tmp .-mstretch)/(xp*sqrt(length(good)))).^2
        end
    end

    if bias(ξ).spread != 0f0
        tmp         = @view(τ[:,2])[good]
        mbias       = mean(tmp)
        res[good] .+= ((tmp .-mbias)/(bias(ξ).spread*sqrt(length(good)))).^2
    end
    res .* (-.5f0)
end

function _logposterior(ξ::MCConfigs, τ::Experiment, ♠::AVECTPEAK) :: Vector{Float32}
    stats = SequenceStats(oligosize(ξ), direction(ξ), τ, ♠)

    if positionnoise(ξ) > 0f0
        res = stats.residuals² .* (-.5/positionnoise(ξ)^2)
    else
        res = zeros(Float32, length(experiment(ξ)))
    end

    if 0f0 < detectionrate(ξ) < 1f0
        # probability is normalized such that a 0.0 indicates a perfect score.
        # probability:
        #   ((1-detectionrate)^sum(undetected) * detectionrate^sum(used))
        #   /detectionrate^(sum(used)+sum(undetected))
        #   = ((1-detectionrate)/detectionrate)^sum(undetected)
        res .+= stats.undetected .* log((1f0 - detectionrate(ξ))/detectionrate(ξ))
    end

    if 0f0 < falseposrate(ξ) < 1f0
        res .+= stats.falsepositives .*log(falseposrate(ξ))
    end
    res
end

function initialize(ξ::MCConfigs, itr::Integer, cpar::AbstractArray{<:Real, 2})
    xp    = experiment(ξ, cpar)
    peaks = andreasinsi(pathfinder(ξ);
                        searchwindow = searchwindow(pathfinder(ξ))*mean(@view(cpar[:,1])),
                        experiment   = xp,
                        oligosize    = oligosize(xp),
                        remaining    = true)
    prior = _logprior(ξ, cpar)
    post  =  _logposterior(ξ, xp, peaks)
    MCTreeState(itr+1, cpar, prior, post, peaks)
end

initialize(ξ::MCConfigs, τ::MCTreeState) = initialize(ξ, iteration(τ), curparams(τ))
initialize(ξ::MCConfigs, τ::AbstractArray{<:Real, 2}) = initialize(ξ, 0, τ)

function Random.rand(ξ::T) :: MCTreeState{peaktype(T)} where T <: MCConfigs
    data  = experiment(ξ)
    vstr  = stretch(ξ).average, stretch(ξ).spread
    vbias = bias(ξ).average, bias(ξ).spread
    if any(i > 0f0 for i = vstr) || any(i > 0.0 for i = vbias)
        good       = isused(Bool, data)
        
        glob       = randn(rng(ξ), Float32)*vstr[1]+1f0
        rnorm      = randn(rng(ξ), Float32, (length(data), 2))

        rnorm[:,1] = rnorm[:,1] .* (glob/(1f0 + vstr[2]*mean(rnorm[good,1]))) .+ 1f0
        while any(rnorm[good,1] .<= 0f0)
            rnorm[:,1]  = randn(rng(ξ), Float32, length(data))*vstr[2]+1f0
            rnorm[:,1] *= glob/mean(rnorm[good,1])
        end
        rnorm[.~good,1]  = 1f0

        glob             = randn(rng(ξ), Float32)*vbias[1]
        rnorm[:,2]       = vbias[1] .+ vbias[2] .* rnorm[:,2]
        rnorm[.~good,2]  = 0f0
    else
        rnorm      = Array{Float32, 2}(length(data), 2)
        rnorm[:,1] = 1f0
        rnorm[:,2] = 0f0
    end

    MCTreeState(0, rnorm, Float32[], Float32[], peaktype(T)[])
end

function Random.rand(ξ::T, τ::MCStates) :: MCTreeState{peaktype(T)} where T <: MCConfigs
    data = experiment(ξ)
    lpar = copy(curparams(τ))
    good = isused(data)
    if (iteration(τ) == 0  && randlm(ξ) != 0f0) || randlm(ξ) >= 1f0
        lpar = lmsolve(ξ, τ)
    elseif randlm(ξ) > 0f0 && rand(rng(ξ)) < randlm(ξ)
        rho  = rand(rng(ξ))
        lpar = lmsolve(ξ, τ) .* rho .+ (1f0-rho) .* curparams(τ)
    elseif randbead(ξ) >=  1f0 || (randbead(ξ) > 0f0 && rand(rng(ξ)) < randbead(ξ))
        lpar[good,1] /= mean(lpar[good,1])
        lpar[good,1] *= randn(rng(ξ)) .* stretch(ξ).average .+ 1f0
    else
        # permutation deals with potential underflows
        perm  = good[randperm(rng(ξ), length(good))]
        var   = 1f0 .-exp.(logprior(τ)[perm] .+ logposterior(τ)[perm])
        var   = cumsum!(var, var)
        if var[end] > 0f0
            var ./= var[end]
        end
        iol   = perm[max(min(searchsorted(var, rand(rng(ξ))).start,
                             length(good)),1)]

        lpar[iol,:] = [1f0, 0f0]                    .+
                      randn(rng(ξ), Float32, 2)     .*
                      [stretch(ξ).spread, bias(ξ).spread]
    end
    MCTreeState(iteration(τ), lpar, Float32[], Float32[], peaktype(T)[])
end
