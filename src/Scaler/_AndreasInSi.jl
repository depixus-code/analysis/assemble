import ..PeakTree:    andreasinsi

function andreasinsi(ξ::T; opts...) where {T <: MCConfigs}
    ξ = clone(ξ; opts...)
    if all(length(i) == 0 for i = experiment(ξ))
        return peaktype(T)[]
    end

    state = nothing
    for i = ξ; state = i; end
    peakspath(ξ, curparams(best(state)))
end
