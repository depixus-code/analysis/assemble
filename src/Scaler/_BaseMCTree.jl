using ..Oligo:    peakposition, APEAK
import ..Oligo:   peaktype
using ..PeakPaths: DeltaScorePath
using ..Accessor: @defaults

@defaults true false struct StretchConfig
    "bead-related stretch variability        (*prior*)"
    average    :: Float32   = .1f0
    "stretch variability per experiment      (*prior*)"
    spread     :: Float32   = .01f0
    "posterior stretch variability           (*posterior*)"
    post       :: Float32   = .0f0
end

@defaults true false struct BiasConfig
    "offset error between theoretical and experimental zero"
    average    :: Float32 = 3f0
    "bias variability per experiment         (*prior*)"
    spread     :: Float32 = 3f0
    "posterior bias variability              (*posterior*)"
    post       :: Float32 = 0f0
end

for i = (StretchConfig, BiasConfig)
    @eval Base.convert(::Type{$i}, ξ::Union{Vector, Tuple}) = $i(ξ...)
end

@mcconfigs struct MCTreeConfig{T} <: MCConfigs
    niterations    :: Int
    positionnoise  :: Float32
    detectionrate  :: Float32
    falseposrate   :: Float32
    stretch        :: StretchConfig
    bias           :: BiasConfig
    "probability of changing the bead stretch towards lsmsolve's anwser's"
    randlm         :: Float32
    "probability of changing the bead stretch rather than an experiment's"
    randbead       :: Float32
    pathfinder     :: T
end

const MC_KWA = Dict(:niterations   => 100,
                    :positionnoise => 1.5f0,
                    :detectionrate => .99f0,
                    :falseposrate  => .01f0,
                    :randlm        => 0f0,
                    :randbead      => 0f0,
                    :stretch       => StretchConfig(),
                    :bias          => BiasConfig(),
                    :pathfinder    => DeltaScorePath())

function MCTreeConfig{T}(τ::Experiment; opts...) where T
    kwa = Dict{Symbol, Any}(opts)
    get!(T, kwa, :pathfinder)
    rng = pop!(kwa, :rng, _RND)
    MCTreeConfig{T}(τ, rng, 
                    (get(kwa, i, MC_KWA[i])
                     for i = fieldnames(MCTreeConfig)[3:end])...)
end

MCTreeConfig(τ::Experiment, ::Type{T}; opts...) where T = MCTreeConfig{T}(τ; opts...)
MCTreeConfig(::Type{T}, τ::Experiment; opts...) where T = MCTreeConfig{T}(τ; opts...)
MCTreeConfig(τ::Experiment, ξ::T; opts...) where T = MCTreeConfig{T}(τ; pathfinder = ξ, opts...)
MCTreeConfig(ξ::T, τ::Experiment; opts...) where T = MCTreeConfig{T}(τ; pathfinder = ξ, opts...)

function MCTreeConfig(τ::Experiment; opts...)
    tpe = typeof(MC_KWA[:pathfinder])
    for (i,j) = enumerate(opts)
        if :pathfinder == j[1]
            tpe = typeof(opts[ind[1]][1])
            break
        end
    end
    MCTreeConfig{tpe}(τ; opts...)
end

for i = fieldnames(MCTreeConfig)
    @eval $i(ξ::Best{<:MCTreeConfig}) = $i(ξ.config)
end

for fcn = [:direction]
    @eval begin
        import ..Oligo: $fcn
        $fcn(ξ::Best{<:MCTreeConfig}) = $fcn(pathfinder(ξ.config))
        $fcn(ξ::MCTreeConfig)         = $fcn(pathfinder(ξ))
    end
end

import ..Oligo: oligosize
oligosize(ξ::MCTreeConfig) = oligosize(experiment(ξ))
oligosize(ξ::Best)         = oligosize(experiment(ξ.config))

@defaults struct MCTreeState{T <: APEAK} <: MCStates
    iteration    ::Int              = 0
    curparams    ::Array{Float32,2} = Array{Float32, 2}(0, 0)
    logprior     ::Vector{Float32}  = Float32[]
    logposterior ::Vector{Float32}  = Float32[]
    peakspath    ::Vector{T}        = T[]

    function MCTreeState{T}(ξ::MCConfigs) where {T <: APEAK}
        cnt = length(experiment(ξ))
        new(0, [ones(Float32, cnt) zeros(Float32, cnt)], Float32[], Float32[], T[])
    end
end

peaktype(::Type{MCTreeConfig{T}})  where T = peaktype(T)
treestate(::Type{MCTreeConfig{T}}) where T = MCTreeState{peaktype(T)}
treestate(ξ::Type{T}) where T <: APEAK     = MCTreeState{T}
treestate(ξ::Type{MCTreeState{T}}) where T = MCTreeState{T}

function MCTreeState(ι, τ, ρrior, ρost, ♠::AbstractVector{T}) where T <: APEAK
    MCTreeState{T}(ι, τ, ρrior, ρost, ♠)
end
    
MCTreeState(ξ::MCTreeConfig{T}) where T = MCTreeState{peaktype(T)}(ξ)

function MCTreeState(ν::Int, τ::MCTreeState{T}) where T
    treestate(T)(ν, curparams(τ), logprior(τ), logposterior(τ), peakspath(τ))
end

Base.eltype(::Type{MCTreeConfig{T}}) where T = treestate(MCTreeConfig{T})
