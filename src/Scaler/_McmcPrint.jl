using Statistics: mean
using ..Oligo:      printsummary, isused

function _priorprint(ξ::MCConfigs, τ::MCStates, stats::SequenceStats)
    avg    = stretch(ξ).average
    xp     = stretch(ξ).spread
    data   = experiment(ξ)
    good   = isused(data)
    priors = []

    if avg != 0f0 || xp != 0f0
        priors = [@sprintf("Prior:       %10.2f %10s", sum(logprior(τ)), " ")]
        mstretch = mean(stretches(τ)[good])
        res1 = avg == 0f0 ? 0f0 : -((mstretch-1f0)/avg)^2
        res2 = xp  == 0f0 ? 0f0 : -mean(((stretches(τ)[good] .- mstretch)/xp).^2)
        push!(priors, @sprintf("    Stretch: %10.2f %10.2f", res1, res2))
    else
        priors = [@sprintf("Prior:       %10.2f", sum(logprior(τ)))]
    end

    spread  = bias(ξ).spread
    if spread > 0f0 0f0
        mbias = mean(biases(τ)[good])
        res   = -mean(((biases(τ)[good] .- mbias) ./ spread).^2)
        if avg != 0f0 || xp != 0f0
            push!(priors, @sprintf("    Bias:    %10.2f %10s", res, " "))
        else
            push!(priors, @sprintf("    Bias:    %10.2f", res))
        end
    end
    priors
end

function _postprint(ξ::MCConfigs, τ::MCStates, stats::SequenceStats, width = 150)
    posts = [@sprintf("Posterior:               %10.2f", sum(logposterior(τ)))]

    post = stretch(ξ).post
    if post != 0f0
        push!(posts,
              @sprintf("    Stretch:             %10.2f",
                       -.5f0*sum(((stats.stretches .- stretches(τ)) ./ stretch(ξ).post).^2)))
    end

    post  = bias(ξ).post
    if post != 0f0
        push!(posts,
              @sprintf("    Bias:                %10.2f",
                       -.5f0*sum(((stats.biases .- biases(τ)) ./ bias(ξ).post).^2)))
    end

    if 0f0 < positionnoise(ξ)
        push!(posts,
              @sprintf("    Noise:               %10.2f",
                       -.5f0*sum(stats.residuals²)/positionnoise(ξ)^2))
    end

    if 0f0 < detectionrate(ξ) < 1f0
        rate = detectionrate(ξ)
        push!(posts,
              @sprintf("    Detected/Undetected: %10.2f",
                       sum(stats.undetected)*log((1f0 - rate)/rate)))
    end

    if 0f0 < falseposrate(ξ) < 1f0
        push!(posts,
              @sprintf("    False Positives:     %10.2f",
                       sum(stats.falsepositives)*log(falseposrate(ξ))))
    end
    posts
end

import Base: print
function Base.print(io::IO, ξτ::Tuple{MCConfigs, MCStates}, width = 150)
    stats  = SequenceStats(ξτ...)
    empty  = @sprintf("             %10s %10s", " ", " ")
    posts  = _postprint(ξτ...,  stats)
    priors = _priorprint(ξτ..., stats)

    printsummary(io, stats)
    if length(priors) == 1
        for i = 1:length(posts)
            println(io, posts[i])
        end
    elseif length(posts) == 1
        for i = 1:length(priors)
            println(io, priors[i])
        end
    else
        append!(priors, [empty for i=length(priors):length(posts)])
        for i = 1:length(posts)
            println(priors[i], " | ", posts[i])
        end
    end
end
