module Assemble
using BioAlignments
using BioSymbols
using DataFrames
using Query

include("Accessor.jl")
include("Oligo/Oligo.jl")
include("PeakPaths/PeakPaths.jl")
include("PeakTree/PeakTree.jl")
include("Scaler/Scaler.jl")
include("Shuffler/Shuffler.jl")
include("Benchmark/Benchmark.jl")

end

