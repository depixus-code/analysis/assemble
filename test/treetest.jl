using Assemble.Oligo:     oligoid, Peak, BatchedOligo, rc
using Assemble.PeakTree:  Tree, GenerationTree, LazyL²uScoreTree,
                          L²uScoreTree, ThresholdedL²uScoreTree, L²uTreeNode,
                          growleaves!, sequence, isendpoint, clone, nodescore,
                          treenodes, pathpeaks, pathpositions, pathscores, pathzeros,
                          pathbases, pathoverlaps, BatchThresholdedL²uScoreTree


function test_tree_paths()
    seq = "atcttctatcgatcg"
    xp  = [Float32[] for _ = 1:64]
    for i = 1:length(seq)-2
        push!(xp[oligoid(seq[i:i+2])], (i-1)*1f0)
    end

    itr  = Tree(.1f0, 2, 3, :right, 1f0, xp, Peak[], Peak[])
    lastv = nothing
    for i = itr
        @test length(i) == 2
        lastv = i[1]
    end
    @test length(lastv) == 1
    @test sequence(itr, lastv[1]) == seq

    seq2 = "atcgggtatcgatcg"
    for i = 2:6
        push!(xp[oligoid(seq2[i:i+2])], (i-1)*1f0)
    end
    foreach(xp) do x sort!(x) end

    lastv = nothing
    for i = itr
        @test length(i) == 2
        lastv = i[1]
    end

    @test length(lastv) == 2
    @test Set([sequence(itr, i) for i = lastv]) == Set([seq, seq2])
end

function test_gentree_paths()
    function _test(fcn)
        seq = "aaaatcttctatcgatcg"
        xp  = [Float32[] for _ = 1:64]
        for i = 1:length(seq)-2
            push!(xp[oligoid(seq[i:i+2])], (i-1)*1f0)
        end

        itr   = fcn(xp)
        lastv = nothing
        for i = itr
            @test length(i) == 2
            @test length(i[1]) <= 2
            @test length(i[1]) >= 1
            lastv = i[1][end][1]
        end
        @test length(lastv) == 1
        @test sequence(itr, lastv[1]) == seq

        seq2 = "aaaatcgggtatcgatcg"
        for i = 5:9
            push!(xp[oligoid(seq2[i:i+2])], (i-1)*1f0)
        end
        foreach(xp) do x sort!(x) end

        outs  = []
        for i = itr
            @test length(i) == 2
            @test length(i[1]) <= 2
            @test length(i[1]) >= 1
            if isendpoint(i)
                push!(outs, sequence(itr, i[1][end][1][1]))
            end
        end

        @test Set(outs) == Set([seq, seq2])
    end

    for tpe = (GenerationTree{2, 2}, GenerationTree{3, 2}, GenerationTree{3, 3})
        _test(xp->tpe(.1f0, 2, 3, :right, 1f0, xp))
    end
    _test(xp->LazyL²uScoreTree{3,3}(.1f0, 2, 3, :right, 1f0, 5f0, xp))
end

function test_l²utree_paths()
    function _test1(itr, seq1, seq2)
        xp  = [Float32[] for _ = 1:64]
        origin = rand()*5
        for i = 1:length(seq1)-2
            push!(xp[oligoid(seq1[i:i+2])], (i-1)*1f0+origin)
            if seq1[i:i+2] !=  seq2[i:i+2]
                push!(xp[oligoid(seq2[i:i+2])], (i-1)*1f0+origin+.05f0)
            end
        end
        foreach(xp) do x sort!(x) end

        itr   = clone(itr; experiment = xp)
        outs  = []
        for i = itr
            if isendpoint(i)
                push!(outs, sequence(itr, i[1][end][1][1]))
            end
        end

        @test outs == [seq1, seq2]

        xp  = [Float32[] for _ = 1:64]
        origin = rand()*5
        for i = 1:length(seq1)-2
            if seq1[i:i+2] !=  seq2[i:i+2]
                push!(xp[oligoid(seq1[i:i+2])], (i-1)*1f0+origin+.05f0)
                push!(xp[oligoid(seq2[i:i+2])], (i-1)*1f0+origin)
            else
                push!(xp[oligoid(seq1[i:i+2])], (i-1)*1f0+origin)
            end
        end

        itr   = clone(itr; experiment = xp)
        outs  = []
        for i = itr
            if isendpoint(i)
                push!(outs, sequence(itr, i[1][end][1][1]))
            end
        end
        @test outs == [seq2, seq1]
    end

    function _test(tpe, vals...)
        itr = tpe(.1f0, 2, 3, :right, 1f0, 5f0, vals...)
       _test1(itr, "atcttctatcgatcg", "atcttgggtcgatcg")
       _test1(itr, "tcttctatcgatcga", "tcttgggtcgatcga")
    end

    _test(L²uScoreTree{6,3})
    _test(LazyL²uScoreTree{6,3})
    _test(ThresholdedL²uScoreTree{6,3}, .1f0, 10f0)
end

function test_rand_tree()
    for i = 1:5
        seq, x, xp = randexperiment(20)
        last    = nothing
        itr     = Tree(.1f0, 2, 3, :right, 1f0, xp)
        for i = itr last = i[1] end
        @test seq ∈ [sequence(itr, i) for i = last]
    end

    seq, x, xp = randexperiment(100)
    itr     = Tree(.1f0, 2, 3, :right, 1f0, xp)
    show(typeof(itr))
    @time for i = itr end
end

function test_rand_gentree()
    seq1, x, xp1 = randexperiment(100)
    seq2, x, xp2 = randexperiment(100; sigma = 1.5f0)
    function _test(fcn)
        for i = 1:5
            seq, x, xp = randexperiment(20)
            last    = nothing
            itr     = fcn(xp)
            found   = false
            for i = itr
                if isendpoint(i)
                    found = any(seq == sequence(itr, k) for j = i[1][end] for k = j)
                    if found
                        break
                    end
                end
            end
            @test found
        end

        itr = clone(fcn(xp1))
        println(typeof(itr))
        maxsz = 0
        @time for i = itr
            if isendpoint(i)
                maxsz = max(length(sequence(itr, i[1][end][end][1])), maxsz)
            end
        end
        println("  sequence size: $maxsz")
        
        if treenodes(typeof(itr)) <: L²uTreeNode
            @show typeof(itr)
            for i = itr
                @test all(nodescore(k) == 0f0 for k = i[1][end][end])
            end
        end

        itr   = clone(itr; experiment = xp2, searchwindow = 8f0)
        maxsz = 0
        @time for i = itr
            if isendpoint(i)
                maxsz = max(length(sequence(itr, i[1][end][end][1])), maxsz)
            end
        end
        println("  sequence size: $maxsz")
    end

    for tpe = (GenerationTree{2, 2}, GenerationTree{3, 2}, GenerationTree{3, 3})
        _test(xp->tpe(.1f0, 2, 3, :right, 1f0, xp))
    end
    _test(xp->LazyL²uScoreTree{3,3}(.1f0, 2, 3, :right, 1f0, 5f0, xp))
    _test(xp->L²uScoreTree{4,4}(.1f0, 2, 3, :right, 1f0, 5f0, xp))
    _test(xp->ThresholdedL²uScoreTree{4,4}(.1f0, 2, 3, :right, 1f0, 5f0, 4f0, 20f0, xp))
end

function test_pathsfunctions()
    seq, x, xp = randexperiment(10; sigma = 1.5f0)
    itr     = clone(ThresholdedL²uScoreTree(); experiment = xp)
    node    = max(itr)
    @test node !== nothing
    size    = sum(1 for i = node)
    for i = (pathpeaks, pathpositions, pathscores, pathzeros, pathbases, pathoverlaps)
        @test length(i(itr, node)) == size
    end
end

function test_rand_batch_tree()
    for i = 1:5
        seq, x, xp = randexperiment(20; batched = true)
        last    = nothing
        oid     = oligoid(BatchedOligo, seq[1:3])
        itr     = BatchThresholdedL²uScoreTree{4,4}(.1f0, 2, 3, :right, 1f0, 5f0,
                                                    4f0, 20f0, xp, [(oid, 1)], [])
        found   = false
        for i = itr
            if isendpoint(i)
                found = any(seq == sequence(itr, k) for j = i[1][end] for k = j)
                if found
                    break
                end
            end
        end
        @test found

        oid     = rc(3, oid)
        itr     = BatchThresholdedL²uScoreTree{4,4}(.1f0, 2, 3, :right, 1f0, 5f0,
                                                    4f0, 20f0, xp, [(oid, 1)], [])
        found   = true
        for i = itr
            if isendpoint(i)
                found = any(seq == sequence(itr, k) for j = i[1][end] for k = j)
                if found
                    break
                end
            end
        end
        @test !found
    end
end


@testset "tree paths"              begin test_tree_paths() end
@testset "random trees"            begin test_rand_tree() end
@testset "generation trees"        begin test_gentree_paths() end
@testset "random generation trees" begin test_rand_gentree() end
@testset "L2 trees"                begin test_l²utree_paths() end
@testset "batch trees"             begin test_rand_batch_tree() end
@testset "paths functions"         begin test_pathsfunctions() end
