using Base.Iterators: product

using Assemble.Oligo:
    Experiment, OligoNode, Peak, Neighbour, peakposition, sequence,
    estimatedposition, SequenceStats, clone, experiment, oligoid,
    oligoname, overlaps, overlapcount, pathpeaks, pathpositions,
    pathscores, pathzeros, pathbases, pathoverlaps, rc, goleft,
    goright, Direction, BatchedOligo
using Assemble.PeakPaths: hasedges, FullPath, Path, PeekPath, DeltaScorePath, L1ScorePath,
    L2ScorePath, L²uScorePath, BatchFullPath, andreasinsi,
    InDepthPath, startpeaks, isendpoint, residualreduction!,
    searchdepth, BatchL²uScorePath

function randseq(dir::Direction = goright, τ = 1f0; batched = false)
    seq, _, xp = randexperiment(100; batched = batched)
    oid        = oligoid(batched ? BatchedOligo : Int,
                         dir == goright ? seq[1:3] : seq[end-2:end])
    startpk    = [(oid, dir == goright ? 1 : length(xp[oid]))]
    if batched
        seq, BatchFullPath(.1f0, 2, 3, dir, τ, xp, startpk, [])
    else
        seq, FullPath(.1f0, 2, 3, dir, τ, xp, startpk, [])
    end
end

function test_path()
    for k = 1:20
        seq, ppath = randseq(goleft, 1f0)
        pots       = [sequence(ppath, i) for i in ppath]
        @test pots == [seq]

        #bppath     = clone(BatchFullPath, ppath)
        #pots       = [sequence(bppath, i) for i in bppath]
        #@test pots == [seq]

        seq2       = string(((seq[i] == 't' ? 'a' : seq[i] == 'a' ? 't' :
                              seq[i] == 'c' ? 'g' : 'c')
                             for i = 1:10)...)*seq[11:end]
        if seq2[10:12] == seq2[11:13]
            continue
        end

        data = experiment(ppath)
        for i = 1:10
            olig = oligoid(seq2[i:i+2])
            push!(data[olig], convert(Float32, i-1))
        end
        for i = data
            sort!(i)
        end
        oid   = startpeaks(ppath)[1][1]
        ppath = FullPath(ppath, 1.f0, experiment(ppath),
                         [(oid, length(experiment(ppath)[oid]))],
                         Peak[])
        pots  = [sequence(ppath, i) for i in ppath]
        @test Set(pots) == Set([seq, seq2])
    end

    seq, ppath = randseq(goleft)
    [sequence(ppath, i) for i in ppath]
    @time [sequence(ppath, i) for i in ppath]

    for k = 1:20
        seq, ppath = randseq(goright)
        pots       = [sequence(ppath, i) for i in ppath]
        @test pots == [seq]

        seq2       = seq[1:10]*string(((seq[i] == 't' ? 'a' : seq[i] == 'a' ? 't' :
                                        seq[i] == 'c' ? 'g' : 'c')
                                      for i = 11:20)...)
        if seq2[8:10] == seq2[9:11]
            continue
        end
        data = experiment(ppath)
        for i = 9:18
            olig = oligoid(seq2[i:i+2])
            push!(data[olig], convert(Float32, i-1))
        end
        for i = data
            sort!(i)
        end

        pots       = [sequence(ppath, i) for i in ppath]
        @test Set(pots) == Set([seq, seq2])
    end
end

function test_andreasinsi()
    seq = "acgtcgtcgctggcttgtgg"
    cnf = Path(2.f0, 2, 3, goright)
    xp  = RandomSequence.experiment(seq, 3)
    out = andreasinsi(cnf; experiment = xp)
    @test sequence(cnf, out) == seq

    seq2  = "acgtcgtcgctggc","ccacaaca"
    seq22 = seq2[1]*seq2[2]
    for i = length(seq2[1]):length(seq22)-2
        push!(xp[oligoid(seq22[i:i+2])], i)
    end
    for i = xp
        sort!(i)
    end
    out = andreasinsi(cnf; experiment = xp, depth = length(seq)-length(seq2[1])-1,
                           falsepositives = -1f0)
    @test sequence(cnf, out) == seq
    cnf = Path(2.f0, 1, 3, goright)
    out = andreasinsi(cnf; experiment = xp, depth = length(seq)-length(seq2[1]),
                           falsepositives = -1f0)
    @test sequence(cnf, out) == seq22
end

function test_peekpaths()
    seq   = "atcgatcgatcg"
    xp    = [Float32[] for i = 1:64]
    for i = 1:length(seq)-2
        push!(xp[oligoid(seq[i:i+2])], i*1f0)
    end
    seq   = "atcttcgatcgatcg"
    for i = 2:length(seq)-2
        push!(xp[oligoid(seq[i:i+2])], i*1f0)
    end
    seq   = "atcc"
    for i = 2:length(seq)-2
        push!(xp[oligoid(seq[i:i+2])], i*1f0)
    end
    ppath = PeekPath(2, 2, 3, goright, 1, 3, xp, [(oligoid("atc"), 1)], Peak[])
    @test searchdepth(ppath) == 3
    vals  = [sequence(ppath, i) for i in ppath]
    @test Set(vals) == Set(["atcttc", "atcgat", "atcc"])
end

function test_indepthpaths()
    seq   = "atcgatcgatcg"
    xp    = [Float32[] for i = 1:64]
    for i = 1:length(seq)-2
        push!(xp[oligoid(seq[i:i+2])], i*1f0)
    end
    seq   = "atcttcgatcgatcg"
    for i = 2:length(seq)-2
        push!(xp[oligoid(seq[i:i+2])], i*1f0)
    end
    seq   = "atcc"
    for i = 2:length(seq)-2
        push!(xp[oligoid(seq[i:i+2])], i*1f0)
    end

    itr = InDepthPath(1.f0, 2, 3, goright, 1.f0, 3, xp, [(oligoid("atc"), 1)], Peak[])
    for i = 1:5
        seq, _, xp = randexperiment(100)
        oid     = oligoid(seq[1:3])
        itr     = InDepthPath(.1f0, 2, 3, goright, 1.f0, 2, xp, [(oid, 1)], Peak[])

        pots    = [sequence(itr, i) for i = itr if isendpoint(i)]
        @test pots == [seq]
    end
end

function test_residualreduction()
    seq  = "ccattcccattccccattcc"
    xp   = [Float32[] for i = 1:64]
    good = Peak[]
    pks  = Peak[]
    for i = 1:length(seq)-2
        oid = oligoid(seq[i:i+2])
        push!(xp[oid], i*1f0-1f0)

        size = length(xp[oid])
        if seq[i:i+2] == "att"
            push!(pks,  (oid, size+1))
        else
            push!(pks,  (oid, size))
        end

        if seq[i:i+2] == "ttc"
            push!(good, (oid, size+1))
        else
            push!(good, (oid, size))
        end
    end
    push!(xp[oligoid("att")], (length(seq)+1)*1f0)
    xp[oligoid("ttc")] = [-1.; xp[oligoid("ttc")]]

    cnf = InDepthPath(1.f0, 2, 3, goright, 1.f0, 3, xp, [(oligoid("atc"), 1)], Peak[])
    @test residualreduction!(cnf, pks) == good
    @time residualreduction!(cnf, pks)
end

function test_random_paths()
    seq, _, xp = randexperiment(100)
    for i = (DeltaScorePath, L1ScorePath, L2ScorePath, L²uScorePath)
        andreasinsi(clone(i(); experiment = xp))
        show(i)
        @time andreasinsi(clone(i(); experiment = xp))
    end
end

function test_pathsfunctions()
    seq, _, xp = randexperiment(10; sigma = 1.5f0)
    itr     = clone(L2ScorePath(); experiment = xp)
    node    = max(itr)
    @test node !== nothing
    size    = length(pathpeaks(node))
    for i = (pathpeaks, pathpositions, pathscores, pathzeros, pathbases, pathoverlaps)
        @show i, i(itr, node)
        @test length(i(itr, node)) == size
    end
end

function test_batched_path()
    for k = 1:20
        seq, ppath = randseq(goleft, 1f0; batched = true)
        pots       = [sequence(ppath, i) for i in ppath]
        @test pots == [seq]
    end

    for k = 1:5
        seq, _, xp = randexperiment(20; batched = true)
        oid        = oligoid(BatchedOligo, seq[1:3])
        itr        = clone(BatchL²uScorePath;
                           searchwindow = .1,
                           experiment   = xp,
                           startpeaks   = [(oid, 1)])
        for i = itr
            if isendpoint(i)
                @test  sequence(itr, i) == seq
                break
            end
        end
    end
end

@testset "peakpath path"               begin test_path() end
@testset "peakpath andreasinsi"        begin test_andreasinsi() end
@testset "peakpath peekpaths"          begin test_peekpaths() end
@testset "peakpath indepthpaths"       begin test_indepthpaths() end
@testset "residualreduction"           begin test_residualreduction() end
@testset "random paths"                begin test_random_paths() end
@testset "paths functions"             begin test_pathsfunctions() end
@testset "peakpath batched path"       begin test_batched_path() end
