using Assemble.Oligo: oligoid, Peak
using Assemble.Springs: SpringConfig, equilibriumorder, springsystem, equilibrium

@testset "springs" begin
    xp = [Float32[] for i = 1:64]
    xp[oligoid("atc")] = Float32[1. ,4. , 7.]
    xp[oligoid("tca")] = Float32[2. ,5. ]
    xp[oligoid("tcg")] = Float32[8. ]

    peaks = Peak[(oligoid("atc"), 1),
                 (oligoid("tca"), 1),
                 (oligoid("atc"), 2),
                 (oligoid("tca"), 2),
                 (oligoid("atc"), 3),
                 (oligoid("tcg"), 1),
                ]

    cnf  = SpringConfig(1., 2., 3.)

    adj, equil =  springsystem(cnf, xp, peaks)

    @test adj == [0.0   1.0   0.0   0.0   0.0  0.0
                  0.0  -2.0   0.0   2.0   0.0  0.0
                  1.0   0.0   0.0   0.0   0.0  0.0
                 -2.0   0.0   2.0   0.0   0.0  0.0
                  0.0   0.0  -2.0   0.0   2.0  0.0
                  0.0   0.0   0.0   0.0   0.0  1.0
                 -3.0   3.0   0.0   0.0   0.0  0.0
                  0.0  -3.0   3.0   0.0   0.0  0.0
                  0.0   0.0  -3.0   3.0   0.0  0.0
                  0.0   0.0   0.0  -3.0   3.0  0.0
                  0.0   0.0   0.0   0.0  -3.0  3.0]
    pos = [1., 2., 4., 5., 7., 8.]
    @test equil == [2., 6., 1., 6., 6., 8., 3., 6., 3., 6., 3.]
    @test adj*pos == equil
    @test equilibrium(cnf, xp, peaks)[2] ≈ pos rtol = 1e-3
    @test equilibriumorder(cnf, xp, peaks) == collect(1:length(peaks))


    xp[oligoid("atc")] = Float32[1., 3. , 5.]
    xp[oligoid("tca")] = Float32[2., 4. ]
    xp[oligoid("tcg")] = Float32[6. ]
    @test equilibriumorder(cnf, xp, peaks) == collect(1:length(peaks))

    xp[oligoid("atc")] = Float32[1., 3. , 5.]
    xp[oligoid("tca")] = Float32[4., 6. ]
    xp[oligoid("tcg")] = Float32[4. ]
    @test equilibriumorder(cnf, xp, peaks) == collect(1:length(peaks))
end
