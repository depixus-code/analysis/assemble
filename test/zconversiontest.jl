using Test
using Base.Iterators
using Assemble.Oligo: oligoid, Peak, goright
using Assemble.ZConversion: ZConversionConfig, basepairfactors, tobasepair

@testset "Z conversions" begin
    xp = [Float32[] for i = 1:64]
    xp[oligoid("atc")] = Float32[1. ,4. , 7.]
    xp[oligoid("tca")] = Float32[2. ,5. ]
    xp[oligoid("tcg")] = Float32[8. ]

    peaks = Peak[(oligoid("atc"), 1),
                 (oligoid("tca"), 1),
                 (oligoid("atc"), 2),
                 (oligoid("tca"), 2),
                 (oligoid("atc"), 3),
                 (oligoid("tcg"), 1),
                ]
    cnf   = ZConversionConfig((.8, 1.2), (-60., 60))
    truth = [(0f0, 1f0) for i = 1:64]
    truth[oligoid("atc")]                       = (1f0, 1f0)
    truth[oligoid("tca")]                       = (1f0, 1f0)
    truth[oligoid("tcg")]                       = (0f0, 8f0/7f0)
    res  = basepairfactors(3, goright, cnf, xp, peaks)
    @test res == truth

    cnf  = ZConversionConfig((.9, 1.1), (-60., 60))
    res  = basepairfactors(3, goright, cnf, xp, peaks)
    truth[oligoid("tcg")]                      = (1f0, 1f0)
    @test res == truth

    xp[oligoid("atc")] = Float32[0. ,3. , 6.] .* 0.9f0
    xp[oligoid("tca")] = Float32[1. ,4. ] .* 1.1f0 .+ 0.5f0
    truth[oligoid("atc")] = (0f0, .9f0)
    truth[oligoid("tca")] = (.5f0, 1.1f0)
    res = basepairfactors(3, goright, cnf, xp, peaks)

    @test collect(flatten(res)) ≈ collect(flatten(truth)) rtol = 1e-3
end
