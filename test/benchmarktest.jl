using Random

using DataFrames: columns

using Assemble.Oligo
using Assemble.InSilico: createdataframe
using Assemble.RandomSequence
using Assemble.RandomSequence: randexperiment
using Assemble.PeakPaths: L²uScorePath
using Assemble.PeakTree: ThresholdedL²uScoreTree, andreasinsi
using Assemble.BenchmarkPrints
using Assemble.BenchmarkPlots
#using Plots

const OPTS = Dict{Symbol, Any}

function test_benchmark()
    runs = createdataframe(1, 1;
                 rnd = [OPTS(:sigma => 1.5f0), OPTS(:sigma => 1.f0, :missing => 0.1)],
                 itr = [OPTS(:iterator => L²uScorePath, :spoonerism => true,
                             :undetected => 10f0),
                        #OPTS(:iterator => BatchThresholdedL²uScoreTree{3,3},
                        #     :undetected => 10f0, :softthreshold => 5f0, :hardthreshold => 25f0),
                        OPTS(:iterator => ThresholdedL²uScoreTree{3,3},
                             :undetected => 10f0, :softthreshold => 5f0, :hardthreshold => 25f0)]
                )
    @test size(runs) == (4, 28)
end

function test_benchmarkplots()
    seq, x, xp = randexperiment(100; sigma = 1.5f0)
    cnf        = ThresholdedL²uScoreTree{4,4}(xp)
    pks        = andreasinsi(cnf)

    println((seq, cnf, pks))
    unicodeplots()
    display(BenchmarkPlots.alignmentplot(cnf, seq, pks))
    display(BenchmarkPlots.oligostats(cnf, seq, pks))
end

#test_benchmarkplots()
test_benchmark()
