push!(LOAD_PATH,  "../deps/XmlTestReport/src/")
using XmlTestReport
@test_toxml begin
    for t = ("benchmarktest.jl",
             "accessorstest.jl",
             "oligotest.jl",
             "spoonertest.jl",
             "springtest.jl",
             "treetest.jl",
             "zconversiontest.jl")
        let
            include(t)
        end
    end
end
