using ..Oligo: oligoid, oligoname
using ..Mcmc:  MCTreeConfig, stretches, biases, logprior, logposterior,
    loglikelihood, iteration, lmsolve

@testset "mcmc" begin
    seq  = "atcgatcgatcgatcg"
    xp   = [Float32[] for i = 1:64]
    for i = 1: length(seq)-2
        oid = oligoid(seq[i:i+2])
        push!(xp[oid], convert(Float32,i-1))
    end

    cnf = MCTreeConfig(xp; niterations = 2)
    out = iterate(cnf)
    print((cnf, out[2]))
    @test out !== nothing
    @test all(stretches(out[1]) .== 1f0)
    @test all(biases(out[1])    .== 0f0)
    @test logprior(out[1])       == zeros(Float32, 64)
    @test logposterior(out[1])   == zeros(Float32, 64)
    @test isfinite(loglikelihood(out[1]))
    @test iteration(out[1]) == 1

    out = iterate(cnf, out[2])
    @test out !== nothing
    @test !all(stretches(out[1]) .== 1f0)
    @test all(isfinite(loglikelihood(out[1])))
    @test iteration(out[2]) == 2

    vals = lmsolve(cnf, out[2])
    @test vals ≈ Float32[ones(64) zeros(64)] atol = 1e-5
end
