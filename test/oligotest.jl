using Base.Iterators: product

using Assemble.Oligo: OligoNode, Peak, Neighbour, estimatedposition, SequenceStats,
                      oligoid, oligoname, overlaps, overlapcount, rc, goleft, goright,
                      BatchedOligo, Experiment, @o_str, @or_str, @ob_str

function test_oligo()
    @test oligoid("a")   == 1
    @test oligoid("t")   == 2
    @test oligoid("c")   == 3
    @test oligoid("g")   == 4
    @test oligoid("aa")  == 1
    @test oligoid("at")  == 1+1<<2
    @test oligoid("ac")  == 1+2<<2
    @test oligoid("ag")  == 1+3<<2
    @test oligoid("aaa") == 1+0
    @test oligoid("aat") == 1+1<<4
    @test oligoid("aac") == 1+2<<4
    @test oligoid("aag") == 1+3<<4
    @test oligoid("ta")  == 1+1
    @test oligoid("tt")  == 1+1+1<<2
    @test oligoid("tc")  == 1+1+2<<2
    @test oligoid("tg")  == 1+1+3<<2
    @test oligoid("cta") == 1+2+1<<2
    @test oligoid("ctt") == 1+2+1<<2+1<<4
    @test oligoid("ctc") == 1+2+1<<2+2<<4
    @test oligoid("ctg") == 1+2+1<<2+3<<4

    @test oligoname(1, 1+0)           == "a"
    @test oligoname(1, 1+1)           == "t"
    @test oligoname(1, 1+2)           == "c"
    @test oligoname(1, 1+3)           == "g"
    @test oligoname(2, 1+0)           == "aa"
    @test oligoname(2, 1+1<<2)        == "at"
    @test oligoname(2, 1+2<<2)        == "ac"
    @test oligoname(2, 1+3<<2)        == "ag"
    @test oligoname(3, 1+0)           == "aaa"
    @test oligoname(3, 1+1<<4)        == "aat"
    @test oligoname(3, 1+2<<4)        == "aac"
    @test oligoname(3, 1+3<<4)        == "aag"
    @test oligoname(2, 1+1)           == "ta"
    @test oligoname(2, 1+1+1<<2)      == "tt"
    @test oligoname(2, 1+1+2<<2)      == "tc"
    @test oligoname(2, 1+1+3<<2)      == "tg"
    @test oligoname(3, 1+2+1<<2)      == "cta"
    @test oligoname(3, 1+2+1<<2+1<<4) == "ctt"
    @test oligoname(3, 1+2+1<<2+2<<4) == "ctc"
    @test oligoname(3, 1+2+1<<2+3<<4) == "ctg"


    @test o"ctg"  == oligoid("ctg")
    @test or"ctg" == oligoid(BatchedOligo, "ctg")
    @test ob"ctg" == min(oligoid(BatchedOligo, "ctg"), oligoid(BatchedOligo, "cag"))

    onames  = sort(vcat((string(i, j, k) for i = "atgc", j = "atgc", k = "atgc")...))
    @test all(oligoname(3, oligoid(i)) == i for i in onames)
    @test Set(oligoid(i) for i in onames) == Set(1:64)

    for ol1 = 1:64, ol2 = 1:64
        n1 = oligoname(3, ol1)
        n2 = oligoname(3, ol2)
        if n1[2:end] == n2[1:end-1]
            @test overlapcount(3, ol1, ol2) == 2
        elseif n1[end] == n2[1]
            @test overlapcount(3, ol1, ol2) == 1
        else
            @test overlapcount(3, ol1, ol2) == 0
        end
    end

    @test (collect(overlaps(goright, 2, 3, oligoid("aaa")))
           == [oligoid(string("aa", i)) for i in "atcg"])
    @test (collect(overlaps(goleft,  2, 3, oligoid("aaa")))
           == [oligoid(string(i, "aa")) for i in "atcg"])

    pots = vcat([string(i...) for i in product("atgc", "atcg")]...)
    fcn(x, y)  = collect(overlaps(x, 1, 3, oligoid(y)))
    @test fcn(goright, "aaa") == sort!((x->oligoid(string("a", x))).(pots))
    @test fcn(goleft,  "aaa") == sort!((x->oligoid(string(x, "a"))).(pots))
    @test fcn(goright, "tta") == sort!((x->oligoid(string("a", x))).(pots))
    @test fcn(goleft,  "att") == sort!((x->oligoid(string(x, "a"))).(pots))
    @test fcn(goright, "aat") == sort!((x->oligoid(string("t", x))).(pots))
    @test fcn(goleft,  "taa") == sort!((x->oligoid(string(x, "t"))).(pots))

    getop(x) = x == 'a' ? 't' : x == 't' ? 'a' : x == 'c' ? 'g' : 'c'
    for i = "atcg", j = "atcg", k = "atcg"
        val = oligoid(string(i,j,k))
        rvv = oligoname(3, rc(3, val))
        op  = string(getop(k), getop(j), getop(i))
        @test op == rvv
    end
end

function test_neighbouringpeaks()
    cnf = OligoNode(2., 2, 3, goright)
    xp = [Float32[] for i = 1:64]
    xp[oligoid("atc")] = collect(0:5:20)*1.
    xp[oligoid("tca")] = [3.9, 4.1, 7.9, 8.1]
    xp[oligoid("tct")] = collect(0:5:20)*1.
    xp[oligoid("tcc")] = [0, 20]
    xp[oligoid("tgc")] = collect(1:5:20)*1.

    truth = [(oligoid("tca"), 2:3), (oligoid("tct"), 2:2),
             (oligoid("tcc"), 2:1), (oligoid("tcg"), 1:0)]
    @test truth == collect(Neighbour(cnf, xp, 2, (oligoid("atc"), 2), 5.f0))
end

function test_oligonode_estimatedposition()
    cnf   = OligoNode(2., 2, 3, goright)
    xp    = [Float32[] for i = 1:64]
    xp[oligoid("aaa")] = collect(1:10)*10

    @test estimatedposition(cnf, 1f0, xp, Peak[(oligoid("aaa"), i) for i = 1:1]) == 10f0
    @test estimatedposition(cnf, 1f0, xp, Peak[(oligoid("aaa"), i) for i = 1:6]) == 60f0
    @test estimatedposition(cnf, 1f0, xp, oligoid("taa"), 1f0, (oligoid("aaa"), 2)) == 20f0
    @test estimatedposition(cnf, 1f0, xp, oligoid("tta"), 1f0, (oligoid("aaa"), 2)) == 20f0

    xp[oligoid("aaa")] = collect(1:10)
    @test estimatedposition(cnf, .5f0, xp, Peak[(oligoid("aaa"), i) for i = 1:1]) == 1f0
    @test estimatedposition(cnf, .5f0, xp, Peak[(oligoid("aaa"), i) for i = 1:6]) == 6f0
    @test estimatedposition(cnf, .5f0, xp, oligoid("taa"), 1f0, (oligoid("aaa"), 2)) == 2f0
    @test estimatedposition(cnf, .5f0, xp, oligoid("tta"), 1f0, (oligoid("aaa"), 2)) == 2.5f0
end

function test_sequencestats()
    seq  = "atcgatcgatcgatcg"
    xp   = [Float32[] for i = 1:64]
    pks  = Peak[]
    used = zeros(Int, 64)
    for i = 1: length(seq)-2
        oid = oligoid(seq[i:i+2])
        push!(xp[oid], convert(Float32,i-1))
        push!(pks, (oid, length(xp[oid])))
        used[oid] += 1
    end
    used[pks[5][1]] -= 1
    pks = vcat(pks[1:4], pks[6:end])
    push!(xp[oligoid("tcg")] , convert(Float32, length(seq)+5))

    xp[oligoid("cga")] .= (xp[oligoid("cga")] .- 1.f0)./.5f0
    xp[oligoid("gat")] .= (xp[oligoid("gat")] .+ 1.f0)./2f0
    stats = SequenceStats(OligoNode(8f0, 1, 3, goright), xp, pks)
    @test stats.used == used

    und = zeros(Int, 64)
    und[oligoid("atc")] = 1
    @test stats.undetected == und

    fp = zeros(Int, 64)
    fp[oligoid("tcg")] = 1
    fp[oligoid("atc")] = 1
    @test stats.falsepositives == fp

    res = zeros(Float32, 64)
    res[oligoid("cga")] = 6.15385
    res[oligoid("gat")] = 2.69231
    @test stats.residuals² ≈ res atol=1e-5
end

@testset "oligo"                       begin test_oligo() end
@testset "oligonode estimatedposition" begin test_oligonode_estimatedposition() end
@testset "neighbouring peaks"          begin test_neighbouringpeaks() end
@testset "sequence stats"              begin test_sequencestats() end
