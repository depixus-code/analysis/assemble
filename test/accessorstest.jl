module TestAccessors
    using Assemble.Accessor
    using Test

    @accessors struct C1
        attr1::Int
        attr2::Int
    end

    @accessors struct C2
        attr1::Float32
        attr3::Int
    end

    abstract type A end
    abstract type B <: A end

    attr1(a::A, b::Int) = 1 # should not hamper getter creation

    @accessors struct C3 <: A
        attr1::Int32
        attr4::Int
    end

    @accessors struct C4 <: B
        attr1::Int32
        attr4::Int
    end

    @accessors struct C5{T <: Number, K} <: B
        attr1::T
        attr5::Int
    end

    abstract type D{T} end
    @accessors struct C6{T} <: D{T}
        attr1::T
        attr6::Int
    end

    function test()
        for i = 1:5, j = 1:5
            @test attr1(C1(i, j)) == i
            @test attr2(C1(i, j)) == j

            @test attr1(C2(i, j)) == convert(Float32, i)
            @test attr3(C2(i, j)) == j

            @test attr1(C3(i, j)) == convert(Int32, i)
            @test attr4(C3(i, j)) == j

            @test attr1(C4(i, j)) == convert(Int32, i)
            @test attr4(C4(i, j)) == j

            @test attr1(C5{Int, Int}(i, j)) == i
            @test attr5(C5{Int, Int}(i, j)) == j
            @test attr1(C5{Float64, Int}(i, j)) == convert(Float64, i)
            @test attr5(C5{Float64, Int}(i, j)) == convert(Float64, j)

            @test attr1(C6{Int}(i, j)) == i
            @test attr6(C6{Int}(i, j)) == j
            @test attr1(C6{Float64}(i, j)) == convert(Float64, i)
            @test attr6(C6{Float64}(i, j)) == convert(Float64, j)
        end
    end
end

module TestDefaults
    using Assemble.Accessor
    using Test

    @defaults struct C0
        attr1::Int    = 1000
        attr2::Int    = 200*2
    end

    @defaults struct C00{T}
        attr1::Int    = 1000
        attr2::Int    = 200*2
    end

    @defaults struct C1
        attr1::Int    = 100
        attr2::Int    = 100
    end

    @defaults struct C2
        attr1::Float32 = 100
        attr3::Int     = 100
    end

    abstract type A end
    abstract type B <: A end

    attr1(a::A, b::Int) = 1 # should not hamper getter creation
    @defaults struct C3 <: A
        attr1::Int32 = 100
        attr4::Int   = 100
    end

    @defaults struct C4 <: B
        attr1::Int32 = 100
        attr4::Int   = 100
    end

    @defaults struct C5{T <: Number, K} <: B
        attr1::T     = 100
        attr5::Int   = 100
    end

    abstract type D{T} end
    @defaults struct C6{T} <: D{T}
        attr1::T    = 100
        attr6::Int  = 100
    end

    function test()
        @test attr1(C0) == 1000
        @test attr2(C0) == 400
        @test attr1(C00{Int}) == 1000
        @test attr2(C00{Int}) == 400
        for i = 1:5, j = 1:5
            @test attr1(C1(i, j)) == i
            @test attr2(C1(i, j)) == j

            @test attr1(C2(i, j)) == convert(Float32, i)
            @test attr3(C2(i, j)) == j

            @test attr1(C3(i, j)) == convert(Int32, i)
            @test attr4(C3(i, j)) == j

            @test attr1(C4(i, j)) == convert(Int32, i)
            @test attr4(C4(i, j)) == j

            @test attr1(C5{Int,Int}(i, j)) == i
            @test attr5(C5{Int,Int}(i, j)) == j
            @test attr1(C5{Float64,Int}(i, j)) == convert(Float64, i)
            @test attr5(C5{Float64,Int}(i, j)) == convert(Float64, j)

            @test attr1(C6{Int}(i, j)) == i
            @test attr6(C6{Int}(i, j)) == j
            @test attr1(C6{Float64}(i, j)) == convert(Float64, i)
            @test attr6(C6{Float64}(i, j)) == convert(Float64, j)
        end

        @test attr1(C1())   == convert(C1.types[1], 100)
        @test attr2(C1())   == convert(C1.types[2], 100)
        @test attr1(C1(10)) == convert(C1.types[1], 10)
        @test attr2(C1(10)) == convert(C1.types[2], 100)

        @test attr1(C2())   == convert(C2.types[1], 100)
        @test attr3(C2())   == convert(C2.types[2], 100)
        @test attr1(C2(10)) == convert(C2.types[1], 10)
        @test attr3(C2(10)) == convert(C2.types[2], 100)

        @test attr1(C3())   == convert(C3.types[1], 100)
        @test attr4(C3())   == convert(C3.types[2], 100)
        @test attr1(C3(10)) == convert(C3.types[1], 10)
        @test attr4(C3(10)) == convert(C3.types[2], 100)

        @test attr1(C4())   == convert(C4.types[1], 100)
        @test attr4(C4())   == convert(C4.types[2], 100)
        @test attr1(C4(10)) == convert(C4.types[1], 10)
        @test attr4(C4(10)) == convert(C4.types[2], 100)

        @test attr1(C5{Int,Int}())   == convert(C5{Int,Int}.types[1], 100)
        @test attr5(C5{Int,Int}())   == convert(C5{Int,Int}.types[2], 100)
        @test attr1(C5{Int,Int}(10)) == convert(C5{Int,Int}.types[1], 10)
        @test attr5(C5{Int,Int}(10)) == convert(C5{Int,Int}.types[2], 100)
        @test attr1(C5{Float32,Int}())   == convert(C5{Float32,Int}.types[1], 100)
        @test attr5(C5{Float32,Int}())   == convert(C5{Float32,Int}.types[2], 100)
        @test attr1(C5{Float32,Int}(10)) == convert(C5{Float32,Int}.types[1], 10)
        @test attr5(C5{Float32,Int}(10)) == convert(C5{Float32,Int}.types[2], 100)

        @test attr1(C6{Int}())   == convert(C6{Int}.types[1], 100)
        @test attr6(C6{Int}())   == convert(C6{Int}.types[2], 100)
        @test attr1(C6{Int}(10)) == convert(C6{Int}.types[1], 10)
        @test attr6(C6{Int}(10)) == convert(C6{Int}.types[2], 100)
        @test attr1(C6{Float32}())   == convert(C6{Float32}.types[1], 100)
        @test attr6(C6{Float32}())   == convert(C6{Float32}.types[2], 100)
        @test attr1(C6{Float32}(10)) == convert(C6{Float32}.types[1], 10)
        @test attr6(C6{Float32}(10)) == convert(C6{Float32}.types[2], 100)
    end
end

@testset "accessors" begin TestAccessors.test() end
@testset "defaults"  begin TestDefaults.test() end
