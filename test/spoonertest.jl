using Random
using Assemble.Oligo:        Peak, sequence, oligoid
using Assemble.PeakPaths:     PeekPath, spoonerable, spooners, symmetry, bestspoonerism,
                             andreasinsi, DeltaScorePath, insertremaining
using Assemble.RandomSequence: randexperiment

function _test_iterator(seq)
    xp    = [Float32[] for _ = 1:64]
    peaks = Peak[]
    for i = 1:length(seq)-2
        id = oligoid(seq[i:i+2])
        push!(xp[id], i*1f0)
        push!(peaks, (id, length(xp[id])))
    end

    itr = PeekPath(7., 2, 3, :right, 1., 6, xp, peaks, [])
    (itr, peaks)
end

function test_internalshuffle()
    function _test(seq, inds, outs)
        itr, peaks = _test_iterator(seq)
        @test spoonerable(itr, peaks) == inds
        out = spooners(itr, peaks, inds[1][1], inds[1][2])
        @test Set([sequence(itr, i) for i in out]) == Set(outs)
    end

    _test("ggcctaacaactcattccgg",  [(6,13)],           ["aacaactca", "aactcaaca"])
    _test("aaagctcttgtccaaa",      [(4,11)],           ["gctcttgtc", "gcttgtctc"])
    _test("gggaccaacccactggg",     [(4, 12)],
          ["accaacccac", "accacccaac", "acccaaccac", "acccaccaac"])
end

function test_asymmetry()
    itr, peaks = _test_iterator("ttttttt")
    @test symmetry(itr, peaks) == 0.
    tmp        = peaks[1]
    peaks[1]   = peaks[end]
    peaks[end] = tmp
    @test symmetry(itr, peaks) == 1.6f0

    tmp          = peaks[2]
    peaks[2]     = peaks[end-1]
    peaks[end-1] = tmp
    @test symmetry(itr, peaks) == 2f0
end

function test_best()
    function _test(seq)
        itr, peaks = _test_iterator(seq)
        for i = spoonerable(itr, peaks)
            for j = spooners(itr, peaks, i[1], i[2])
                tmp              = copy(peaks)
                tmp[i[1]:i[2]-1] = j
                bestspoonerism(itr, tmp)
            end
        end
    end

    _test("ggcctaacaactcattccgg")
    _test("aaagctcttgtccaaa")
    _test("gggaccaacccactggg")
end

function test_rand()
    seq, _, xp  = randexperiment(Random.MersenneTwister(0), 100; sigma = 3.f0)
    best        = DeltaScorePath(10.f0, 1, 3, :right, .33f0, 4, xp, [], [])
    lst         = andreasinsi(best)
    out         = bestspoonerism(best, lst)
end

function test_insertremaining()
    itr, peaks = _test_iterator("ggcctaaactcattccgg")
    out        = insertremaining(itr, [peaks[1:5]; peaks[7:end]])
    @test out == peaks

    itr, peaks = _test_iterator("ggcctaaacaactcattccgg")
    out        = insertremaining(itr, [peaks[1:5]; peaks[10:end]])
    @test out == peaks
    @time insertremaining(itr, [peaks[1:5]; peaks[10:end]])
end

@testset "insertremaining" begin test_insertremaining() end
@testset "internalshuffle" begin test_internalshuffle() end
@testset "asymmetry"       begin test_asymmetry() end
@testset "rand"            begin test_rand() end
@testset "best"            begin test_best() end
