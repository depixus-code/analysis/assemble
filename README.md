# Installation

clone the repository in path <packagepath> of your choice

then install all dependencies within julia 
```
julia
using Pkg
Pkg.add(PackageSpec(url=<packagepath>))
```
Specify to julia that the package is still under development using:
```
] develop <packagepath>
```

and run test

```
] test Assemble # not assemble
```

Note that the gitlab repository is "assemble" (lowercase) but Julia's convention
for naming package is to use CamelCase (ie. "Assemble").
